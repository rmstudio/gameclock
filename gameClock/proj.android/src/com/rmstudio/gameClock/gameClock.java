/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.rmstudio.gameClock;

import org.cocos2dx.lib.Cocos2dxActivity;
import android.view.WindowManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class gameClock extends Cocos2dxActivity {
	
	public static String batteryStatus;
	
	public static String batteryLevel;
	
	static Cocos2dxActivity activity;

	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		activity = this;
		
        // Keep screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);  
	}
	
    static {
         System.loadLibrary("cocos2dcpp");
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        
        IntentFilter filter = new IntentFilter();
        
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(mBroadcastReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        
        unregisterReceiver(mBroadcastReceiver);
    }

    
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                int status = intent.getIntExtra("status", 0);
                int health = intent.getIntExtra("health", 0);
                boolean present = intent.getBooleanExtra("present", false);
                int level = intent.getIntExtra("level", 0);
                int scale = intent.getIntExtra("scale", 0);
                int icon_small = intent.getIntExtra("icon-small", 0);
                int plugged = intent.getIntExtra("plugged", 0);
                int voltage = intent.getIntExtra("voltage", 0);
                int temperature = intent.getIntExtra("temperature", 0);
                String technology = intent.getStringExtra("technology");
                
                batteryLevel = String.valueOf(level);
                
                String statusString = "";
                
                switch (status) {
                case BatteryManager.BATTERY_STATUS_UNKNOWN:
                	batteryStatus = "unknown";
                    break;
                case BatteryManager.BATTERY_STATUS_CHARGING:
                	batteryStatus = "charging";
                    break;
                case BatteryManager.BATTERY_STATUS_DISCHARGING:
                	batteryStatus = "discharging";
                    break;
                case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                	batteryStatus = "not charging";
                    break;
                case BatteryManager.BATTERY_STATUS_FULL:
                	batteryStatus = "full";
                    break;
                }
                
                String healthString = "";
                
                switch (health) {
                case BatteryManager.BATTERY_HEALTH_UNKNOWN:
                	healthString = "unknown";
                    break;
                case BatteryManager.BATTERY_HEALTH_GOOD:
                	healthString = "good";
                    break;
                case BatteryManager.BATTERY_HEALTH_OVERHEAT:
                	healthString = "overheat";
                    break;
                case BatteryManager.BATTERY_HEALTH_DEAD:
                	healthString = "dead";
                    break;
                case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
                	healthString = "voltage";
                    break;
                case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
                	healthString = "unspecified failure";
                    break;
                }
                
                String acString = "";
                
                switch (plugged) {
                case BatteryManager.BATTERY_PLUGGED_AC:
                    acString = "plugged ac";
                    break;
                case BatteryManager.BATTERY_PLUGGED_USB:
                    acString = "plugged usb";
                    break;
                }
                                
                Log.v("status", statusString);
                Log.v("health", healthString);
                Log.v("present", String.valueOf(present));
                Log.v("level", String.valueOf(level));
                Log.v("scale", String.valueOf(scale));
                Log.v("icon_small", String.valueOf(icon_small));
                Log.v("plugged", acString);
                Log.v("voltage", String.valueOf(voltage));
                Log.v("temperature", String.valueOf(temperature));
                Log.v("technology", technology);
            }
        }
    };
    
    public static String getBatteryStatus() {
        // 言語情報を返す
        return batteryStatus;
    }
    
    public static String getBatteryLevel() {
        // 言語情報を返す
        return batteryLevel;
    }
}
