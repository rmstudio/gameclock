#define COCOS2D_DEBUG 1

#include "Setting/KiremakeSettingScene.h"
#include "Setting/ByoYomiSettingScene.h"
#include "Setting/FisherSettingScene.h"
#include "Setting/CanadaSettingScene.h"
#include "Setting/XianqqiSettingScene.h"
#include "RuleSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

enum BUTTON {
    BUTTON_TIME1   = 0,
    BUTTON_TIME2  = 1,
    BUTTON_COLOR1  = 2,
    BUTTON_COLOR2  = 3,
    BUTTON_RULE  = 3,
};

enum RULE {
    RULE_KIREMAKE   = 0,
    RULE_BYOYOMI  = 1,
    RULE_FISHER  = 2,
    RULE_CANADA  = 3,
    RULE_XIANQQI  = 4,
};

enum PLAYER {
	PLAYER_SENTE = 0,
	PLAYER_GOTE = 1,
};

Scene* RuleSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    RuleSettingScene *layer = RuleSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool RuleSettingScene::init()
{
    
    m_settingBgColorStart = Color4B(237, 238, 217, 255);
    m_settingBgColorEnd = Color4B(237, 238, 217, 255);
    m_fontColor = Color3B(98, 98, 98);

    if ( !LayerGradient::initWithColor( m_settingBgColorStart, m_settingBgColorStart ) )
    {
        return false;
    }
    
    m_winSize = Director::getInstance()->getWinSize();

    m_rule = UserDefault::getInstance()->getIntegerForKey("rule", 0);

    LayerGradient *settingTitleLayer = LayerGradient::create(Color4B(196, 229, 103, 255), Color4B(64, 132, 31, 255));
    settingTitleLayer->setContentSize( Size(m_winSize.width, 120) );
    settingTitleLayer->setPosition(Vec2(0, m_winSize.height-120));

    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(RuleSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width+100, m_backButtonItem->getContentSize().height+100));
    settingTitleLayer->addChild(m_backButton, 100);

    this->addChild( settingTitleLayer , 160);

    Label *settingTitle = Label::createWithSystemFont("ルール設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    settingTitleLayer->addChild(settingTitle, 1);

    // ルール
    m_ruleSettingButtonItem = Sprite::create("res/setting_menu.png");
    m_ruleSettingButtonItem->setPosition( Vec2(m_winSize.width/2, m_winSize.height-250) );
    m_ruleSettingButtonItem->setAnchorPoint(Vec2(0.5f, 0.5f));

    Label *menu1 = Label::createWithSystemFont("切れ負け", "Thonburi", 48);
    Label *menu2 = Label::createWithSystemFont("秒読み", "Thonburi", 48);
    Label *menu3 = Label::createWithSystemFont("フィッシャー", "Thonburi", 48);
    Label *menu4 = Label::createWithSystemFont("カナダ式", "Thonburi", 48);
    Label *menu5 = Label::createWithSystemFont("シャンチー国際", "Thonburi", 48);

    menu1->setPosition(Vec2(m_ruleSettingButtonItem->getContentSize().width/2, 50));
    menu2->setPosition(Vec2(m_ruleSettingButtonItem->getContentSize().width/2, 50));
    menu3->setPosition(Vec2(m_ruleSettingButtonItem->getContentSize().width/2, 50));
    menu4->setPosition(Vec2(m_ruleSettingButtonItem->getContentSize().width/2, 50));
    menu5->setPosition(Vec2(m_ruleSettingButtonItem->getContentSize().width/2, 50));

    menu1->setColor(m_fontColor);
    menu2->setColor(m_fontColor);
    menu3->setColor(m_fontColor);
    menu4->setColor(m_fontColor);
    menu5->setColor(m_fontColor);

    m_slider_arrow_left = Sprite::create("res/slider_arrow_left.png");
    m_slider_arrow_right = Sprite::create("res/slider_arrow_right.png");

    m_slider_arrow_left->setPosition(Vec2(30, 50));
    m_slider_arrow_right->setPosition(Vec2(m_ruleSettingButtonItem->getContentSize().width-30, 50));

	m_pageView = ui::PageView::create();
	m_pageView->setContentSize(Size( m_winSize.width-40, 100 ));
	m_pageView->setPosition(Point::ZERO);
	m_pageView->addEventListener(CC_CALLBACK_2(RuleSettingScene::pageEvent, this));

    Layer *ruleLayer1 = Layer::create();
    ruleLayer1->setAnchorPoint( Point::ANCHOR_MIDDLE );
    ruleLayer1->setPosition(Vec2(0, 0));
    ruleLayer1->setContentSize( Size( m_winSize.width-40, 100 ) );
    ruleLayer1->addChild(menu1, 100);

    Layer *ruleLayer2 = Layer::create();
    ruleLayer2->setAnchorPoint( Point::ANCHOR_MIDDLE );
    ruleLayer2->setPosition(Vec2(0, 0));
    ruleLayer2->setContentSize( Size( m_winSize.width-40, 100 ) );
    ruleLayer2->addChild(menu2, 100);

    Layer *ruleLayer3 = Layer::create();
    ruleLayer3->setAnchorPoint( Point::ANCHOR_MIDDLE );
    ruleLayer3->setPosition(Vec2(0, 0));
    ruleLayer3->setContentSize( Size( m_winSize.width-40, 100 ) );
    ruleLayer3->addChild(menu3, 100);

    Layer *ruleLayer4 = Layer::create();
    ruleLayer4->setAnchorPoint( Point::ANCHOR_MIDDLE );
    ruleLayer4->setPosition(Vec2(0, 0));
    ruleLayer4->setContentSize( Size( m_winSize.width-40, 100 ) );
    ruleLayer4->addChild(menu4, 100);

    Layer *ruleLayer5 = Layer::create();
    ruleLayer5->setAnchorPoint( Point::ANCHOR_MIDDLE );
    ruleLayer5->setPosition(Vec2(0, 0));
    ruleLayer5->setContentSize( Size( m_winSize.width-40, 100 ) );
    ruleLayer5->addChild(menu5, 100);

    Vector<Layer*> layers;
    layers.pushBack(ruleLayer1);
    layers.pushBack(ruleLayer2);
    layers.pushBack(ruleLayer3);
    layers.pushBack(ruleLayer4);
    layers.pushBack(ruleLayer5);

    for(auto layer : layers) {
    	auto layout = ui::Layout::create();
    	layout->setContentSize(m_pageView->getContentSize());
    	layout->addChild(layer);
    	m_pageView->addPage(layout);
    }

    m_pageView->scrollToPage(m_rule);
    m_ruleSettingButtonItem->addChild(m_pageView, 0);
	this->addChild(m_ruleSettingButtonItem, 1);

	m_ruleContentsButtonItem = Sprite::create("res/rule_contents.png");
	m_ruleContentsButtonItem->setPosition( Vec2(m_winSize.width/2, m_winSize.height-350) );
	m_ruleContentsButtonItem->setAnchorPoint(Vec2(0.5f, 1.0f));
    this->addChild(m_ruleContentsButtonItem, 1);

    // ルール説明
    m_explanation = Label::createWithSystemFont("持ち時間が切れたら終了", "Thonburi", 48, Size::ZERO, TextHAlignment::LEFT, TextVAlignment::TOP);
    m_explanation->setColor(m_fontColor);
    m_explanation->setWidth(600);
    m_ruleContentsButtonItem->addChild(m_explanation,0);

    setExplanation();

    m_ruleSettingButtonItem->addChild(m_slider_arrow_left, 0);
    m_ruleSettingButtonItem->addChild(m_slider_arrow_right, 0);

	m_slider_arrow_left->setVisible(false);
	m_slider_arrow_right->setVisible(true);

    return true;
}

// button action
void RuleSettingScene::backButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

	Scene *nextScene;

	if(m_rule == RULE_KIREMAKE) {
		nextScene = TransitionSlideInL::create(0.1f, KiremakeSettingScene::scene());
	} else if(m_rule == RULE_BYOYOMI) {
		nextScene = TransitionSlideInL::create(0.1f, ByoYomiSettingScene::scene());
	} else if(m_rule == RULE_FISHER) {
		nextScene = TransitionSlideInL::create(0.1f, FisherSettingScene::scene());
	} else if(m_rule == RULE_CANADA) {
		nextScene = TransitionSlideInL::create(0.1f, CanadaSettingScene::scene());
	} else if(m_rule == RULE_XIANQQI) {
		nextScene = TransitionSlideInL::create(0.1f, XianqqiSettingScene::scene());
	} else {
		nextScene = TransitionSlideInL::create(0.1f, KiremakeSettingScene::scene());
	}

    Director::getInstance()->replaceScene(nextScene);
}

void RuleSettingScene::pageEvent(Ref *pSender, ui::PageView::EventType type)
{
	int page = m_pageView->getCurPageIndex();
	auto fadein = FadeIn::create(0.5);
	auto fadeout = FadeTo::create(0.5, 0.25);

	Sequence *seq = Sequence::create(fadein, fadeout, NULL);
	auto blink1 = RepeatForever::create(seq);
	auto blink2 = blink1->clone();

	m_slider_arrow_left->stopAction(blink1);
	m_slider_arrow_right->stopAction(blink2);

	switch(type)
	{
		case ui::PageView::EventType::TURNING:
		{
			if(page == 0) {						// 切れ負け
				UserDefault::getInstance()->setIntegerForKey("rule", 0);
				m_rule = 0;

				m_slider_arrow_left->setVisible(false);
				m_slider_arrow_right->setVisible(true);

				m_slider_arrow_right->runAction(blink2);
			} else if(page == 1) {				// 秒読み
				UserDefault::getInstance()->setIntegerForKey("rule", 1);
				m_rule = 1;

				m_slider_arrow_left->setVisible(true);
				m_slider_arrow_right->setVisible(true);

				m_slider_arrow_left->runAction(blink1);
				m_slider_arrow_right->runAction(blink2);
			} else if(page == 2) {				// フィッシャー
				UserDefault::getInstance()->setIntegerForKey("rule", 2);
				m_rule = 2;

				m_slider_arrow_left->setVisible(true);
				m_slider_arrow_right->setVisible(true);

				m_slider_arrow_left->runAction(blink1);
				m_slider_arrow_right->runAction(blink2);
			} else if(page == 3) {				// カナダ式
				UserDefault::getInstance()->setIntegerForKey("rule", 3);
				m_rule = 3;

				m_slider_arrow_left->setVisible(true);
				m_slider_arrow_right->setVisible(true);

				m_slider_arrow_left->runAction(blink1);
				m_slider_arrow_right->runAction(blink2);
			} else if(page == 4) {				// シャンチー国際
				UserDefault::getInstance()->setIntegerForKey("rule", 4);
				m_rule = 4;

				m_slider_arrow_left->setVisible(true);
				m_slider_arrow_right->setVisible(false);

				m_slider_arrow_right->runAction(blink1);
			}
		}
			break;
		default:
			break;
	}

	setExplanation();
}

void RuleSettingScene::setExplanation() {

	if(m_rule == RULE_KIREMAKE) {
		m_explanation->setString("持ち時間がなくなった時点でゲームが終了します。");
	} else if(m_rule == RULE_BYOYOMI) {
		m_explanation->setString("持ち時間がなくなった後、設定した秒数内で指す秒読みになります。\n秒読みの回数は設定できます。");
	} else if(m_rule == RULE_FISHER) {
		m_explanation->setString("持ち時間が切れたら終了します。\n一手ごとに設定した加算時間が追加されます。");
	} else if(m_rule == RULE_CANADA) {
		m_explanation->setString("持ち時間がなくなった後、超過時間に入り、設定した時間内に設定した手数で指します。");
	} else if(m_rule == RULE_XIANQQI) {
		m_explanation->setString("持ち時間を設定した手数で指し、それがなくなると、設定した時間内に設定した手数で指す考慮時間に入ります。考慮時間の回数は設定できます。\n考慮時間がなくなると設定した秒数内で指す秒読みになります。");
	}
    m_explanation->setPosition(Vec2(30, m_ruleContentsButtonItem->getContentSize().height-50));
    m_explanation->setAnchorPoint(Vec2(0.0f, 1.0f));
    m_explanation->setColor(Color3B(68, 68, 68));
}
