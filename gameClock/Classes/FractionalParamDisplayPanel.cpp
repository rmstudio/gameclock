
#include "FractionalParamDisplayPanel.h"

using namespace cocos2d;

void FractionalParamDisplayPanel::setValue(int currentValue, int maxValue) {
	Size winSize = Director::getInstance()->getVisibleSize();

	char valueStr[15];
	sprintf(valueStr, "%02d/%02d", currentValue, maxValue);

	if(m_timeLabel) {
		m_backGroundLayer->removeChild(m_timeLabel, true);
	}

    // 時間
	m_timeLabel = Label::createWithSystemFont(valueStr, "HiraKakuProN-W6", 32);
	m_timeLabel->setColor(Color3B(98, 98, 98));
	m_timeLabel->setPosition(Vec2(m_backGroundLayer->getContentSize().width/2,
    		(m_backGroundLayer->getContentSize().height-m_headlineLayer->getContentSize().height)/2));

    m_backGroundLayer->addChild(m_timeLabel, 170);
}

void FractionalParamDisplayPanel::setUnit(const std::string& unit) {
	Size winSize = Director::getInstance()->getVisibleSize();

    // 単位
	m_unitLabel = Label::createWithSystemFont(unit, "HiraKakuProN-W6", 12);
	m_unitLabel->setColor(Color3B(98, 98, 98));
	m_unitLabel->setPosition(Vec2(m_backGroundLayer->getContentSize().width - 10, 15));

    m_backGroundLayer->addChild(m_unitLabel, 170);
}
