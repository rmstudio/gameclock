#include "MenuPanel.h"

#define kItemTag 0

MenuPanel* MenuPanel::create(MenuItem* item, ...)
{
    va_list args;
    va_start(args,item);

    MenuPanel *pRet = MenuPanel::createWithItems(item, args);

    va_end(args);

    return pRet;
}

MenuPanel* MenuPanel::createWithArray(Vector<MenuItem*> pArrayOfItems)
{
    MenuPanel *pRet = new MenuPanel();
    if (pRet && pRet->initWithArray(pArrayOfItems))
    {
        pRet->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(pRet);
    }

    return pRet;
}

MenuPanel* MenuPanel::createWithItems(MenuItem* item, va_list args)
{
	Vector<MenuItem*> items;
    if( item )
    {
    	items.pushBack(item);
        MenuItem *i = va_arg(args, MenuItem*);
        while(i)
        {
        	items.pushBack(i);
            i = va_arg(args, MenuItem*);
        }
    }

    return MenuPanel::createWithArray(items);
}

MenuPanel* MenuPanel::create()
{
	return MenuPanel::create(NULL, NULL);
}

