/*
 * ScrollMenu.h
 *
 *  Created on: 2015/11/22
 *      Author: ryuji_muraoka
 */

#ifndef __SCROLLMENU_H__
#define __SCROLLMENU_H__

#include "cocos2d.h"

namespace cocos2d {

class ScrollMenu : public Menu {
public:
	ScrollMenu();
	virtual ~ScrollMenu(){};

    // オブジェクト作成用function
	static ScrollMenu* create(MenuItem* item, ...) CC_REQUIRES_NULL_TERMINATION;

    static ScrollMenu* createWithArray(const Vector<MenuItem*>& arrayOfItems);

    static ScrollMenu* createWithItems(MenuItem* item, va_list args);

    // オブジェクト作成時実行処理
    bool initWithArray(const Vector<MenuItem*>& arrayOfItems);

    // オーバーライドするタッチ開始イベント
    bool onTouchBegan(Touch* touch, Event* event);

    // オーバーライドするスクロール中イベント
    void onTouchMoved(Touch* touch, Event* event);

    // オーバーライドするタッチ終了イベント
    void onTouchEnded(Touch* touch, Event* event);

protected:
    // コンストラクタ
    //ScrollMenu();

private:
    // 現在スクロール中かを判断するフラグ
    bool _inMoving;
    Vec2 _touchStartPoint;
};

} /* namespace cocos2d */
#endif /* __SCROLLMENU_H__ */
