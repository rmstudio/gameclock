
#ifndef FRACTIONAL_PARAM_DISPLAYPANEL_H_
#define FRACTIONAL_PARAM_DISPLAYPANEL_H_

#include "cocos2d.h"
#include "ParamDisplayPanel.h"

namespace cocos2d {

class FractionalParamDisplayPanel : public ParamDisplayPanel {
public:
	CREATE_FUNC(FractionalParamDisplayPanel);

	void setValue(int currentValue, int maxValue);
	void setUnit(const std::string& unit);
};

}
#endif
