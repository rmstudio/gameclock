#ifndef __GameClockScene_SCENE_H__
#define __GameClockScene_SCENE_H__

#include "cocos2d.h"
#include "DialogLayer.h"
#include "ParamDisplayPanel.h"
#include "FractionalParamDisplayPanel.h"

using namespace cocos2d;

class GameClockScene : public cocos2d::LayerGradient {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(GameClockScene);
    
    Size m_winSize;
    
    cocos2d::Label* m_senteTimeLabel;
    cocos2d::Label* m_goteTimeLabel;
    cocos2d::Label* m_senteMoveLabel;
    cocos2d::Label* m_goteMoveLabel;

    cocos2d::Label* m_senteFisherAdditionalTimeLabel;
    cocos2d::Label* m_goteFisherAdditionalTimeLabel;
    cocos2d::Label* m_senteCanadaKiteiTimeLabel;
    cocos2d::Label* m_goteCanadaKiteiTimeLabel;
    cocos2d::Label* m_senteCanadaKiteiTekazuLabel;
    cocos2d::Label* m_goteCanadaKiteiTekazuLabel;
    cocos2d::Label* m_senteXianqqiKiteiTekazuLabel;
    cocos2d::Label* m_goteXianqqiKiteiTekazuLabel;
    cocos2d::Label* m_senteXianqqiMotiJikanLabel;
    cocos2d::Label* m_goteXianqqiMotiJikanLabel;
    cocos2d::Label* m_senteXianqqiKiteiCntLabel;
    cocos2d::Label* m_goteXianqqiKiteiCntLabel;
    cocos2d::Label* m_senteXianqqiByoYomiTimeLabel;
    cocos2d::Label* m_goteXianqqiByoYomiTimeLabel;
    
    ParamDisplayPanel *m_senteByoYomiPanel;
    ParamDisplayPanel *m_goteByoYomiPanel;
    FractionalParamDisplayPanel *m_senteByoYomiKouryoCntPanel;
    FractionalParamDisplayPanel *m_goteByoYomiKouryoCntPanel;
    ParamDisplayPanel *m_senteByoYomiKouryoTimePanel;
    ParamDisplayPanel *m_goteByoYomiKouryoTimePanel;

    ParamDisplayPanel *m_senteFisherAdditionalTimePanel;
    ParamDisplayPanel *m_goteFisherAdditionalTimePanel;

    ParamDisplayPanel *m_senteCanadaKiteiTimePanel;
    ParamDisplayPanel *m_goteCanadaKiteiTimePanel;
    FractionalParamDisplayPanel *m_senteCanadaKiteiTekazuPanel;
    FractionalParamDisplayPanel *m_goteCanadaKiteiTekazuPanel;

    FractionalParamDisplayPanel *m_senteXianqqiKiteiTekazuPanel;
    FractionalParamDisplayPanel *m_goteXianqqiKiteiTekazuPanel;
    ParamDisplayPanel *m_senteXianqqiMotiJikanPanel;
    ParamDisplayPanel *m_goteXianqqiMotiJikanPanel;
    FractionalParamDisplayPanel *m_senteXianqqiKiteiTekazu2Panel;
    FractionalParamDisplayPanel *m_goteXianqqiKiteiTekazu2Panel;
    FractionalParamDisplayPanel *m_senteXianqqiKiteiCountPanel;
    FractionalParamDisplayPanel *m_goteXianqqiKiteiCountPanel;
    ParamDisplayPanel *m_senteXianqqiByoYomiTimePanel;
    ParamDisplayPanel *m_goteXianqqiByoYomiTimePanel;

    // selector callback
    void menuCloseCallback(Ref* pSender);
    void setting1ButtonDidPushed(Ref* pSender);
    void setting2ButtonDidPushed(Ref* pSender);
    void pauseButtonDidPushed(Ref* pSender);
    void resetButtonDidPushed(Ref* pSender);
    void resumeButtonDidPushed(Ref* pSender);
    
    int m_turn;
    bool m_start;

    int m_gameTime;
    char m_gameTimeStr[5];

    int m_hour1;
    int m_minite1;
    int m_second1;
    int m_hour2;
    int m_minite2;
    int m_second2;

    int m_move;
    int m_move1;
    int m_move2;

    int m_rule;											// ルール
    int m_byo_yomi_time1;								// 秒読み時間（プレイヤー１）
    int m_byo_yomi_time2;								// 秒読み時間（プレイヤー２）
    int m_byo_yomi_kouryo_cnt1;							// 秒読み考慮回数（プレイヤー１）
    int m_byo_yomi_kouryo_cnt2;							// 秒読み考慮回数（プレイヤー２）
    int m_byo_yomi_kouryo_cnt_max1;						// 秒読み考慮回数最大（プレイヤー１）
    int m_byo_yomi_kouryo_cnt_max2;						// 秒読み考慮回数最大（プレイヤー２）
    int m_fisher_additional_time1;						// フィッシャー加算時間（プレイヤー１）
    int m_fisher_additional_time2;						// フィッシャー加算時間（プレイヤー２）
    int m_canada_kitei_time1;							// カナダ式規定時間（プレイヤー１）
    int m_canada_kitei_time2;							// カナダ式規定時間（プレイヤー２）
    int m_canada_kitei_tekazu_current1;					// カナダ式規定手数（現在値）（プレイヤー１）
    int m_canada_kitei_tekazu_current2;					// カナダ式規定手数（現在値）（プレイヤー２）
    int m_canada_kitei_tekazu1;							// カナダ式規定手数（プレイヤー１）
    int m_canada_kitei_tekazu2;							// カナダ式規定手数（プレイヤー２）
    int m_xianqqi_kitei_tekazu1_1;						// シャンチー国際 規定手数１（プレイヤー１）
    int m_xianqqi_kitei_tekazu1_2;						// シャンチー国際 規定手数１（プレイヤー２）
    int m_xianqqi_kitei_tekazu_current1_1;				// シャンチー国際 規定手数１（現在値）（プレイヤー１）
    int m_xianqqi_kitei_tekazu_current1_2;				// シャンチー国際 規定手数１（現在値）（プレイヤー２）
    int m_xianqqi_moti_jikan2_1;						// シャンチー国際 持ち時間２（プレイヤー１）
    int m_xianqqi_moti_jikan2_2;						// シャンチー国際 持ち時間２（プレイヤー２）
    int m_xianqqi_kitei_tekazu2_1;						// シャンチー国際 規定手数２（プレイヤー１）
    int m_xianqqi_kitei_tekazu2_2;						// シャンチー国際 規定手数２（プレイヤー２）
    int m_xianqqi_kitei_tekazu_current2_1;				// シャンチー国際 規定手数２（現在値）（プレイヤー１）
    int m_xianqqi_kitei_tekazu_current2_2;				// シャンチー国際 規定手数２（現在値）（プレイヤー２）
    int m_xianqqi_kitei_cnt1;							// シャンチー国際 規定回数（プレイヤー１）
    int m_xianqqi_kitei_cnt2;							// シャンチー国際 規定回数（プレイヤー２）
    int m_xianqqi_kitei_cnt_current1;					// シャンチー国際 規定回数（現在値）（プレイヤー１）
    int m_xianqqi_kitei_cnt_current2;					// シャンチー国際 規定回数（現在値）（プレイヤー２）
    int m_xianqqi_byo_yomi_time1;						// シャンチー国際 秒読み時間（プレイヤー１）
    int m_xianqqi_byo_yomi_time2;						// シャンチー国際 秒読み時間（プレイヤー２）

    int m_secondNow;
    
    int m_color1;
    int m_color2;

    int m_xianqqi_time_mode1;				// シャンチー国際 時間モード（プレイヤー１）
    int m_xianqqi_time_mode2;				// シャンチー国際 時間モード（プレイヤー２）

    Color3B m_grayFont;

    Color4B m_startColorRed;				// 赤
    Color4B m_endColorRed;
    Color4B m_startColorBlue;				// 青
    Color4B m_endColorBlue;
    Color4B m_startColorGreen;				// 緑
    Color4B m_endColorGreen;
    Color4B m_startColorOrange;				// オレンジ（フォント：グレー）
    Color4B m_endColorOrange;

    Color4B m_startColorDeepPink;			// ディープピンク
    Color4B m_endColorDeepPink;
    Color4B m_startColorLightSeaGreen;		// ライトシーグリーン
    Color4B m_endColorLightSeaGreen;
    Color4B m_startColorOliveGreen;			// ダークオリーブグリーン
    Color4B m_endColorOliveGreen;
    Color4B m_startColorYellow;				// 黄
    Color4B m_endColorYellow;

    Color4B m_startColorHotPink;			// ホットピンク
    Color4B m_endColorHotPink;
    Color4B m_startColorSkyBlue;			// スカイブルー（フォント：グレー）
    Color4B m_endColorSkyBlue;
    Color4B m_startColorPaleGreen;			// パールグリーン
    Color4B m_endColorPaleGreen;
    Color4B m_startColorKhaki;				// カーキ
    Color4B m_endColorKhaki;

    Color4B m_startColorMediumViolet;		// ミディアムバイオレット
    Color4B m_endColorMediumViolet;
    Color4B m_startColorPurple;				// パープル
    Color4B m_endColorPurple;
    Color4B m_startColorDarkGreen;			// 深緑
    Color4B m_endColorDarkGreen;
    Color4B m_startColorBrown;				// 茶色
    Color4B m_endColorBrown;

    Color4B m_startColorMaroon;				// マルーン
    Color4B m_endColorMaroon;
    Color4B m_startColorNavy;				// ネイビー
    Color4B m_endColorNavy;
    Color4B m_startColorSilver;				// シルバー
    Color4B m_endColorSilver;
    Color4B m_startColorBlack;				// ブラック
    Color4B m_endColorBlack;

    Color4B m_colorTurqoise;				// ターコイズ
    Color4B m_colorEmerald;					// エメラルド
    Color4B m_colorPeterRiver;				// ピーター・リバー
    Color4B m_colorAmethyst;				// アメジスト
    Color4B m_colorWetAspalt;				// ウェット・アスファルト
    Color4B m_colorGreenSea;				// グリーン・シー
    Color4B m_colorNephritis;				// ネフリシス
    Color4B m_colorBelizeHole;				// ベリーズホール
    Color4B m_colorWisteria;				// ウィステリア
    Color4B m_colorMidnightBlue;			// ミッドナイト・ブルー
    Color4B m_colorSunFlower;				// サンフラワー
    Color4B m_colorCarrot;					// キャロット
    Color4B m_colorAlizarin;				// アリザリン
    Color4B m_colorClouds;					// クラウド
    Color4B m_colorConcrete;				// コンクリート
    Color4B m_colorOrange;					// オレンジ
    Color4B m_colorPumpkin;					// パンプキン
    Color4B m_colorPomegranate;				// ポームグラネート
    Color4B m_colorSilver;					// シルバー
    Color4B m_colorAsbestos;				// アスベストス

    char m_senteTimeStr[100];
    char m_goteTimeStr[100];
    char m_senteMoveStr[100];
    char m_goteMoveStr[100];

    bool m_settingMode1;
    bool m_settingMode2;
    bool m_byoYomiMode1;					// 秒読みモードフラグ（プレイヤー１）
    bool m_byoYomiMode2;					// 秒読みモードフラグ（プレイヤー２）
    bool m_canadaOverTimeMode1;				// カナダ式 超過時間フラグ（プレイヤー１）
    bool m_canadaOverTimeMode2;				// カナダ式 超過時間フラグ（プレイヤー２）
    
    MenuItemImage *m_pauseButtonItem;

	LayerGradient* m_senteArea;
	LayerGradient* m_goteArea;
    LayerColor *m_settingLayer1;
    LayerColor *m_settingLayer2;
    DialogLayer *m_pauseDialogLayer;
    LayerColor *m_pauseLayerbg;
    LayerColor *m_goteMaskLayer;
    LayerColor *m_senteMaskLayer;
    
    Label *m_tapRequreText;
    Label *m_tapLabelJibun;
    Label *m_tapLabelAite;

    Menu* m_pauseButton;

    EventListenerTouchOneByOne *m_listener;
    EventDispatcher *m_eventDispatcher;

    void updateTimer(float delta);
    void endTimer();
    
    void loadSaveParams();
    
    void didTap();
    void didLongPress();
    
private:
    void setBackGroundColor();
    void showByoYomiTimeLabel();
    void showFisherTimeLabel();
    void showCanadaTimeLabel();
    void showXianqqiTimeLabel();
    void updateFisherAdditionalTimeLabel(int turn);
    bool onTouchBegan(cocos2d::Touch *pTouch, cocos2d::Event *pEvent);
    void onTouchMoved(cocos2d::Touch *pTouch, cocos2d::Event *pEvent);
    void onTouchEnded(cocos2d::Touch *pTouch, cocos2d::Event *pEvent);
    void updateClockTime(int turn);
    void endJudge(int turn);
    void gameOver(int loserTurn);
};


#endif // __GameClockScene_SCENE_H__
