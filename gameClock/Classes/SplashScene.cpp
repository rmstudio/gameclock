#define COCOS2D_DEBUG 1

#include "SplashScene.h"
#include "Setting/KiremakeSettingScene.h"
#include "Setting/ByoYomiSettingScene.h"
#include "Setting/FisherSettingScene.h"
#include "Setting/CanadaSettingScene.h"
#include "Setting/XianqqiSettingScene.h"
#include "GameClockScene.h"

enum RULE {
    RULE_KIREMAKE   = 0,
    RULE_BYOYOMI  = 1,
    RULE_FISHER  = 2,
    RULE_CANADA  = 3,
    RULE_XIANQQI  = 4,
};

using namespace cocos2d;
using namespace cocos2d::extension;

Scene* SplashScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    SplashScene *layer = SplashScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SplashScene::init()
{
    
    Color4B startColor = Color4B(196, 229, 103, 255);
    Color4B endColor = Color4B(64, 132, 31, 255);

    if ( !LayerGradient::initWithColor( startColor, endColor ) )
    {
        return false;
    }
    
    m_winSize = Director::getInstance()->getWinSize();

    Sprite *app_logo_large = Sprite::create("res/splash_logo.png");
    app_logo_large->setPosition( Vec2(m_winSize.width/2, m_winSize.height*2/3) );
    app_logo_large->setContentSize( Size(m_winSize.width*2/3, m_winSize.width*2/3) );
    this->addChild(app_logo_large,1);

    this->schedule(schedule_selector(SplashScene::nextScene), 3);

    return true;
}

void SplashScene::nextScene(float delta)
{
	int rule = UserDefault::getInstance()->getIntegerForKey("rule", 0);

	Scene *nextScene;

	if(rule == RULE_KIREMAKE) {
		nextScene = CCTransitionFade::create(1.0f, KiremakeSettingScene::scene());
	} else if(rule == RULE_BYOYOMI) {
		nextScene = CCTransitionFade::create(1.0f, ByoYomiSettingScene::scene());
	} else if(rule == RULE_FISHER) {
		nextScene = CCTransitionFade::create(1.0f, FisherSettingScene::scene());
	} else if(rule == RULE_CANADA) {
		nextScene = CCTransitionFade::create(1.0f, CanadaSettingScene::scene());
	} else if(rule == RULE_XIANQQI) {
		nextScene = CCTransitionFade::create(1.0f, XianqqiSettingScene::scene());
	}

    Director::getInstance()->replaceScene(nextScene);
}
