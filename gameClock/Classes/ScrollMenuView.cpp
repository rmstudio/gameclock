#include "ScrollMenuView.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;

ScrollMenuView::ScrollMenuView() : ScrollView() {

}

ScrollMenuView* ScrollMenuView::create() {
    ScrollMenuView* widget = new (std::nothrow) ScrollMenuView();
    if (widget && widget->init())
    {
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

ScrollMenuView* ScrollMenuView::create(ScrollMenu *menu) {
    ScrollMenuView* widget = new (std::nothrow) ScrollMenuView();
    if (widget && widget->init(menu))
    {
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

bool ScrollMenuView::init() {
    if(!ScrollView::init()) {
        return false;
    }

    return true;
}

bool ScrollMenuView::init(ScrollMenu *menu) {
    if(!ScrollView::init()) {
        return false;
    }

    this->setInnerContainerSize(menu->getContentSize());
    this->addChild(menu, 1, "menu");

    return true;
}

bool ScrollMenuView::onTouchBegan(Touch *touch, Event *unusedEvent)
{
    this->_touchStartPoint = touch->getLocationInView();
    return ScrollView::onTouchBegan(touch, unusedEvent);
}

void ScrollMenuView::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unusedEvent) {
    if(5.0f < touch->getLocationInView().distance(this->_touchStartPoint)) {
        ScrollView::onTouchMoved(touch, unusedEvent);
    }
}
