#define COCOS2D_DEBUG 1

#include "Setting/KiremakeSettingScene.h"
#include "Setting/ByoYomiSettingScene.h"
#include "Setting/FisherSettingScene.h"
#include "Setting/CanadaSettingScene.h"
#include "Setting/XianqqiSettingScene.h"
#include "ColorSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

enum BUTTON {
    BUTTON_TIME1   = 0,
    BUTTON_TIME2  = 1,
    BUTTON_COLOR1  = 2,
    BUTTON_COLOR2  = 3,
    BUTTON_RULE  = 3,
};
enum RULE {
    RULE_KIREMAKE   = 0,
    RULE_BYOYOMI  = 1,
    RULE_FISHER  = 2,
    RULE_CANADA  = 3,
    RULE_XIANQQI  = 4,
};

enum PLAYER {
	PLAYER_SENTE = 0,
	PLAYER_GOTE = 1,
};

Scene* ColorSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    ColorSettingScene *layer = ColorSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool ColorSettingScene::init()
{
    
    Color4B startColor = Color4B(237, 238, 217, 255);
    Color4B endColor = Color4B(237, 238, 217, 255);
    m_grayFont = Color3B(149, 165, 166);
    
    if ( !LayerGradient::initWithColor( startColor, endColor ) )
    {
        return false;
    }
    
    m_winSize = Director::getInstance()->getWinSize();

    m_colorTurqoise = Color4B(26, 188, 156, 255);
    m_colorGreenSea = Color4B(22, 160, 133, 255);
    m_colorSunFlower = Color4B(241, 196, 15, 255);
    m_colorOrange = Color4B(243, 156, 18, 255);
    m_colorEmerald = Color4B(46, 204, 113, 255);
    m_colorNephritis = Color4B(39, 174, 96, 255);
    m_colorCarrot = Color4B(230, 126, 34, 255);
    m_colorPumpkin = Color4B(211, 84, 0, 255);
    m_colorPeterRiver = Color4B(52, 152, 219, 255);
    m_colorBelizeHole = Color4B(41, 128, 185, 255);
    m_colorAlizarin = Color4B(231, 76, 60, 255);
    m_colorPomegranate = Color4B(192, 57, 43, 255);
    m_colorAmethyst = Color4B(155, 89, 182, 255);
    m_colorWisteria = Color4B(142, 68, 173, 255);
    m_colorClouds = Color4B(251, 252, 252, 255);
    m_colorSilver = Color4B(189, 195, 199, 255);
    m_colorWetAspalt = Color4B(52, 73, 94, 255);
    m_colorMidnightBlue = Color4B(44, 62, 80, 255);
    m_colorConcrete = Color4B(149, 165, 166, 255);
    m_colorAsbestos = Color4B(127, 140, 141, 255);

    // 背景色
    m_color1 = UserDefault::getInstance()->getIntegerForKey("color1", 0);
    m_color2 = UserDefault::getInstance()->getIntegerForKey("color2", 1);

    // ルール
    m_rule = UserDefault::getInstance()->getIntegerForKey("rule", 0);

    int player = UserDefault::getInstance()->getIntegerForKey("player" , 0);

    // タイトル
    LayerGradient *settingTitleLayer = LayerGradient::create(Color4B(196, 229, 103, 255), Color4B(64, 132, 31, 255));
    settingTitleLayer->setContentSize( Size(m_winSize.width, 120) );
    settingTitleLayer->setPosition(Point(0, m_winSize.height-120));

    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(ColorSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Point(50, settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width+100, m_backButtonItem->getContentSize().height));
    settingTitleLayer->addChild(m_backButton, 100);

    this->addChild( settingTitleLayer , 160);

    // タイトルラベル
    Label *settingTitle;
    if(player == 0) {
    	settingTitle = Label::createWithSystemFont("背景色設定(先手)", "HiraKakuProN-W6", 64);
    } else if(player == 1) {
    	settingTitle = Label::createWithSystemFont("背景色設定(後手)", "HiraKakuProN-W6", 64);
    } else {
    	settingTitle = Label::createWithSystemFont("背景色設定", "HiraKakuProN-W6", 64);
    }
    settingTitle->setPosition( Point(m_winSize.width / 2, 60) );
    settingTitleLayer->addChild(settingTitle, 1);

    m_color1Item = MenuItemImage::create(
												 "res/color_peter_river.png",
												 "res/color_peter_river.png",
												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
    m_color1Item->setContentSize(Size(150, 150));
    m_color1Item->setAnchorPoint(Point(0.0f, 0.0f));
    m_color1Item->setTag(1);
    m_color1Item->setPosition(Point(30, m_winSize.height-300));

    m_color2Item = MenuItemImage::create(
												 "res/color_belize_hole.png",
												 "res/color_belize_hole.png",
												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
    m_color2Item->setContentSize(Size(150, 150));
    m_color2Item->setAnchorPoint(Point(0.0f, 0.0f));
    m_color2Item->setTag(2);
    m_color2Item->setPosition(Point(200, m_winSize.height-300));

    m_color3Item = MenuItemImage::create(
												 "res/color_alizarin.png",
												 "res/color_alizarin.png",
												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
    m_color3Item->setContentSize(Size(150, 150));
    m_color3Item->setAnchorPoint(Point(0.0f, 0.0f));
    m_color3Item->setTag(3);
    m_color3Item->setPosition(Point(370, m_winSize.height-300));

    m_color4Item = MenuItemImage::create(
												 "res/color_pomegranete.png",
												 "res/color_pomegranete.png",
												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
    m_color4Item->setContentSize(Size(150, 150));
    m_color4Item->setAnchorPoint(Point(0.0f, 0.0f));
    m_color4Item->setTag(4);
    m_color4Item->setPosition(Point(540, m_winSize.height-300));

    m_color5Item = MenuItemImage::create(
												 "res/color_emerald.png",
												 "res/color_emerald.png",
												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
    m_color5Item->setContentSize(Size(150, 150));
    m_color5Item->setAnchorPoint(Point(0.0f, 0.0f));
    m_color5Item->setTag(5);
    m_color5Item->setPosition(Point(30, m_winSize.height-470));

    m_color6Item = MenuItemImage::create(
 												 "res/color_nephritis.png",
 												 "res/color_nephritis.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color6Item->setContentSize(Size(150, 150));
	m_color6Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color6Item->setTag(6);
	m_color6Item->setPosition(Point(200, m_winSize.height-470));

    m_color7Item = MenuItemImage::create(
 												 "res/color_carrot.png",
 												 "res/color_carrot.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color7Item->setContentSize(Size(150, 150));
	m_color7Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color7Item->setTag(7);
	m_color7Item->setPosition(Point(370, m_winSize.height-470));

    m_color8Item = MenuItemImage::create(
 												 "res/color_pumpkin.png",
 												 "res/color_pumpkin.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color8Item->setContentSize(Size(150, 150));
	m_color8Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color8Item->setTag(8);
	m_color8Item->setPosition(Point(540, m_winSize.height-470));

    m_color9Item = MenuItemImage::create(
 												 "res/color_turquoise.png",
 												 "res/color_turquoise.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color9Item->setContentSize(Size(150, 150));
	m_color9Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color9Item->setTag(9);
	m_color9Item->setPosition(Point(30, m_winSize.height-640));

    m_color10Item = MenuItemImage::create(
 												 "res/color_green_sea.png",
 												 "res/color_green_sea.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color10Item->setContentSize(Size(150, 150));
	m_color10Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color10Item->setTag(10);
	m_color10Item->setPosition(Point(200, m_winSize.height-640));

    m_color11Item = MenuItemImage::create(
 												 "res/color_sun_flower.png",
 												 "res/color_sun_flower.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color11Item->setContentSize(Size(150, 150));
	m_color11Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color11Item->setTag(11);
	m_color11Item->setPosition(Point(370, m_winSize.height-640));

    m_color12Item = MenuItemImage::create(
 												 "res/color_orange.png",
 												 "res/color_orange.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color12Item->setContentSize(Size(150, 150));
	m_color12Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color12Item->setTag(12);
	m_color12Item->setPosition(Point(540, m_winSize.height-640));

    m_color13Item = MenuItemImage::create(
 												 "res/color_amethyst.png",
 												 "res/color_amethyst.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color13Item->setContentSize(Size(150, 150));
	m_color13Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color13Item->setTag(13);
	m_color13Item->setPosition(Point(30, m_winSize.height-810));

    m_color14Item = MenuItemImage::create(
 												 "res/color_wisteria.png",
 												 "res/color_wisteria.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color14Item->setContentSize(Size(150, 150));
	m_color14Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color14Item->setTag(14);
	m_color14Item->setPosition(Point(200, m_winSize.height-810));

    m_color15Item = MenuItemImage::create(
 												 "res/color_cloud.png",
 												 "res/color_cloud.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color15Item->setContentSize(Size(150, 150));
	m_color15Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color15Item->setTag(15);
	m_color15Item->setPosition(Point(370, m_winSize.height-810));

    m_color16Item = MenuItemImage::create(
 												 "res/color_silver.png",
 												 "res/color_silver.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color16Item->setContentSize(Size(150, 150));
	m_color16Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color16Item->setTag(16);
	m_color16Item->setPosition(Point(540, m_winSize.height-810));

    m_color17Item = MenuItemImage::create(
 												 "res/color_wet_asphalt.png",
 												 "res/color_wet_asphalt.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color17Item->setContentSize(Size(150, 150));
	m_color17Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color17Item->setTag(17);
	m_color17Item->setPosition(Point(30, m_winSize.height-980));

    m_color18Item = MenuItemImage::create(
 												 "res/color_midnight_blue.png",
 												 "res/color_midnight_blue.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color18Item->setContentSize(Size(150, 150));
	m_color18Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color18Item->setTag(18);
	m_color18Item->setPosition(Point(200, m_winSize.height-980));

    m_color19Item = MenuItemImage::create(
 												 "res/color_concrete.png",
 												 "res/color_concrete.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color19Item->setContentSize(Size(150, 150));
	m_color19Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color19Item->setTag(19);
	m_color19Item->setPosition(Point(370, m_winSize.height-980));

    m_color20Item = MenuItemImage::create(
 												 "res/color_asbestos.png",
 												 "res/color_asbestos.png",
 												 CC_CALLBACK_1(ColorSettingScene::colorSelected, this));
	m_color20Item->setContentSize(Size(150, 150));
	m_color20Item->setAnchorPoint(Point(0.0f, 0.0f));
	m_color20Item->setTag(20);
	m_color20Item->setPosition(Point(540, m_winSize.height-980));

    m_color1ItemButton = Menu::create(m_color1Item, NULL);
    m_color1ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color1ItemButton, 100);

    m_color2ItemButton = Menu::create(m_color2Item, NULL);
    m_color2ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color2ItemButton, 100);

    m_color3ItemButton = Menu::create(m_color3Item, NULL);
    m_color3ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color3ItemButton, 100);

    m_color4ItemButton = Menu::create(m_color4Item, NULL);
    m_color4ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color4ItemButton, 100);

    m_color5ItemButton = Menu::create(m_color5Item, NULL);
    m_color5ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color5ItemButton, 100);

    m_color6ItemButton = Menu::create(m_color6Item, NULL);
    m_color6ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color6ItemButton, 100);

    m_color7ItemButton = Menu::create(m_color7Item, NULL);
    m_color7ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color7ItemButton, 100);

    m_color8ItemButton = Menu::create(m_color8Item, NULL);
    m_color8ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color8ItemButton, 100);

    m_color9ItemButton = Menu::create(m_color9Item, NULL);
    m_color9ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color9ItemButton, 100);

    m_color10ItemButton = Menu::create(m_color10Item, NULL);
    m_color10ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color10ItemButton, 100);

    m_color11ItemButton = Menu::create(m_color11Item, NULL);
    m_color11ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color11ItemButton, 100);

    m_color12ItemButton = Menu::create(m_color12Item, NULL);
    m_color12ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color12ItemButton, 100);

    m_color13ItemButton = Menu::create(m_color13Item, NULL);
    m_color13ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color13ItemButton, 100);

    m_color14ItemButton = Menu::create(m_color14Item, NULL);
    m_color14ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color14ItemButton, 100);

    m_color15ItemButton = Menu::create(m_color15Item, NULL);
    m_color15ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color15ItemButton, 100);

    m_color16ItemButton = Menu::create(m_color16Item, NULL);
    m_color16ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color16ItemButton, 100);

    m_color17ItemButton = Menu::create(m_color17Item, NULL);
    m_color17ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color17ItemButton, 100);

    m_color18ItemButton = Menu::create(m_color18Item, NULL);
    m_color18ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color18ItemButton, 100);

    m_color19ItemButton = Menu::create(m_color19Item, NULL);
    m_color19ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color19ItemButton, 100);

    m_color20ItemButton = Menu::create(m_color20Item, NULL);
    m_color20ItemButton->setPosition( Point::ZERO );
    this->addChild(m_color20ItemButton, 100);

	////////////////////////////////////////////////

	m_selectLabel1_1 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_1->setPosition(Point(75, 75));
	m_color1Item->addChild(m_selectLabel1_1, 110);

	m_selectLabel1_2 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_2->setPosition(Point(75, 75));
	m_color2Item->addChild(m_selectLabel1_2, 110);

	m_selectLabel1_3 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_3->setPosition(Point(75, 75));
	m_color3Item->addChild(m_selectLabel1_3, 110);

	m_selectLabel1_4 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_4->setPosition(Point(75, 75));
	m_color4Item->addChild(m_selectLabel1_4, 110);

	m_selectLabel1_5 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_5->setPosition(Point(75, 75));
	m_color5Item->addChild(m_selectLabel1_5, 110);

	m_selectLabel1_6 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_6->setPosition(Point(75, 75));
	m_color6Item->addChild(m_selectLabel1_6, 110);

	m_selectLabel1_7 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_7->setPosition(Point(75, 75));
	m_color7Item->addChild(m_selectLabel1_7, 110);

	m_selectLabel1_8 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_8->setPosition(Point(75, 75));
	m_color8Item->addChild(m_selectLabel1_8, 110);

	m_selectLabel1_9 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_9->setPosition(Point(75, 75));
	m_color9Item->addChild(m_selectLabel1_9, 110);

	m_selectLabel1_10 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_10->setPosition(Point(75, 75));
	m_color10Item->addChild(m_selectLabel1_10, 110);

	m_selectLabel1_11 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_11->setPosition(Point(75, 75));
	m_color11Item->addChild(m_selectLabel1_11, 110);

	m_selectLabel1_12 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_12->setPosition(Point(75, 75));
	m_color12Item->addChild(m_selectLabel1_12, 110);

	m_selectLabel1_13 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_13->setPosition(Point(75, 75));
	m_color13Item->addChild(m_selectLabel1_13, 110);

	m_selectLabel1_14 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_14->setPosition(Point(75, 75));
	m_color14Item->addChild(m_selectLabel1_14, 110);

	m_selectLabel1_15 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_15->setPosition(Point(75, 75));
	m_selectLabel1_15->setColor(m_grayFont);
	m_color15Item->addChild(m_selectLabel1_15, 110);

    m_selectLabel1_16 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
    m_selectLabel1_16->setPosition(Point(75, 75));
    m_color16Item->addChild(m_selectLabel1_16, 110);

	m_selectLabel1_17 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_17->setPosition(Point(75, 75));
	m_color17Item->addChild(m_selectLabel1_17, 110);

	m_selectLabel1_18 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_18->setPosition(Point(75, 75));
	m_color18Item->addChild(m_selectLabel1_18, 110);

	m_selectLabel1_19 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_19->setPosition(Point(75, 75));
	m_color19Item->addChild(m_selectLabel1_19, 110);

	m_selectLabel1_20 = Label::createWithSystemFont("先手", "HiraKakuProN-W6", 48);
	m_selectLabel1_20->setPosition(Point(75, 75));
	m_color20Item->addChild(m_selectLabel1_20, 110);

	////////////////////////////////////////////////

	m_selectLabel2_1 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_1->setPosition(Point(75, 75));
	m_color1Item->addChild(m_selectLabel2_1, 110);

	m_selectLabel2_2 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_2->setPosition(Point(75, 75));
	m_color2Item->addChild(m_selectLabel2_2, 110);

	m_selectLabel2_3 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_3->setPosition(Point(75, 75));
	m_color3Item->addChild(m_selectLabel2_3, 110);

	m_selectLabel2_4 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_4->setPosition(Point(75, 75));
	m_color4Item->addChild(m_selectLabel2_4, 110);

	m_selectLabel2_5 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_5->setPosition(Point(75, 75));
	m_color5Item->addChild(m_selectLabel2_5, 110);

	m_selectLabel2_6 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_6->setPosition(Point(75, 75));
	m_color6Item->addChild(m_selectLabel2_6, 110);

	m_selectLabel2_7 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_7->setPosition(Point(75, 75));
	m_color7Item->addChild(m_selectLabel2_7, 110);

	m_selectLabel2_8 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_8->setPosition(Point(75, 75));
	m_color8Item->addChild(m_selectLabel2_8, 110);

	m_selectLabel2_9 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_9->setPosition(Point(75, 75));
	m_color9Item->addChild(m_selectLabel2_9, 110);

	m_selectLabel2_10 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_10->setPosition(Point(75, 75));
	m_color10Item->addChild(m_selectLabel2_10, 110);

	m_selectLabel2_11 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_11->setPosition(Point(75, 75));
	m_color11Item->addChild(m_selectLabel2_11, 110);

	m_selectLabel2_12 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_12->setPosition(Point(75, 75));
	m_color12Item->addChild(m_selectLabel2_12, 110);

	m_selectLabel2_13 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_13->setPosition(Point(75, 75));
	m_color13Item->addChild(m_selectLabel2_13, 110);

	m_selectLabel2_14 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_14->setPosition(Point(75, 75));
	m_color14Item->addChild(m_selectLabel2_14, 110);

	m_selectLabel2_15 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_15->setPosition(Point(75, 75));
	m_selectLabel2_15->setColor(m_grayFont);
	m_color15Item->addChild(m_selectLabel2_15, 110);

    m_selectLabel2_16 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
    m_selectLabel2_16->setPosition(Point(75, 75));
    m_color16Item->addChild(m_selectLabel2_16, 110);

	m_selectLabel2_17 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_17->setPosition(Point(75, 75));
	m_color17Item->addChild(m_selectLabel2_17, 110);

	m_selectLabel2_18 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_18->setPosition(Point(75, 75));
	m_color18Item->addChild(m_selectLabel2_18, 110);

	m_selectLabel2_19 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_19->setPosition(Point(75, 75));
	m_color19Item->addChild(m_selectLabel2_19, 110);

	m_selectLabel2_20 = Label::createWithSystemFont("後手", "HiraKakuProN-W6", 48);
	m_selectLabel2_20->setPosition(Point(75, 75));
	m_color20Item->addChild(m_selectLabel2_20, 110);

	m_selectLabel1_1->setVisible(false);
	m_selectLabel1_2->setVisible(false);
	m_selectLabel1_3->setVisible(false);
	m_selectLabel1_4->setVisible(false);
	m_selectLabel1_5->setVisible(false);
	m_selectLabel1_6->setVisible(false);
	m_selectLabel1_7->setVisible(false);
	m_selectLabel1_8->setVisible(false);
	m_selectLabel1_9->setVisible(false);
	m_selectLabel1_10->setVisible(false);
	m_selectLabel1_11->setVisible(false);
	m_selectLabel1_12->setVisible(false);
	m_selectLabel1_13->setVisible(false);
	m_selectLabel1_14->setVisible(false);
	m_selectLabel1_15->setVisible(false);
	m_selectLabel1_16->setVisible(false);
	m_selectLabel1_17->setVisible(false);
	m_selectLabel1_18->setVisible(false);
	m_selectLabel1_19->setVisible(false);
	m_selectLabel1_20->setVisible(false);

	m_selectLabel2_1->setVisible(false);
	m_selectLabel2_2->setVisible(false);
	m_selectLabel2_3->setVisible(false);
	m_selectLabel2_4->setVisible(false);
	m_selectLabel2_5->setVisible(false);
	m_selectLabel2_6->setVisible(false);
	m_selectLabel2_7->setVisible(false);
	m_selectLabel2_8->setVisible(false);
	m_selectLabel2_9->setVisible(false);
	m_selectLabel2_10->setVisible(false);
	m_selectLabel2_11->setVisible(false);
	m_selectLabel2_12->setVisible(false);
	m_selectLabel2_13->setVisible(false);
	m_selectLabel2_14->setVisible(false);
	m_selectLabel2_15->setVisible(false);
	m_selectLabel2_16->setVisible(false);
	m_selectLabel2_17->setVisible(false);
	m_selectLabel2_18->setVisible(false);
	m_selectLabel2_19->setVisible(false);
	m_selectLabel2_20->setVisible(false);

	if(m_color1 == 0) {
		m_selectLabel1_1->setVisible(true);
	} else if(m_color1 == 1) {
		m_selectLabel1_2->setVisible(true);
	} else if(m_color1 == 2) {
		m_selectLabel1_3->setVisible(true);
	} else if(m_color1 == 3) {
		m_selectLabel1_4->setVisible(true);
	} else if(m_color1 == 4) {
		m_selectLabel1_5->setVisible(true);
	} else if(m_color1 == 5) {
		m_selectLabel1_6->setVisible(true);
	} else if(m_color1 == 6) {
		m_selectLabel1_7->setVisible(true);
	} else if(m_color1 == 7) {
		m_selectLabel1_8->setVisible(true);
	} else if(m_color1 == 8) {
		m_selectLabel1_9->setVisible(true);
	} else if(m_color1 == 9) {
		m_selectLabel1_10->setVisible(true);
	} else if(m_color1 == 10) {
		m_selectLabel1_11->setVisible(true);
	} else if(m_color1 == 11) {
		m_selectLabel1_12->setVisible(true);
	} else if(m_color1 == 12) {
		m_selectLabel1_13->setVisible(true);
	} else if(m_color1 == 13) {
		m_selectLabel1_14->setVisible(true);
	} else if(m_color1 == 14) {
		m_selectLabel1_15->setVisible(true);
	} else if(m_color1 == 15) {
		m_selectLabel1_16->setVisible(true);
	} else if(m_color1 == 16) {
		m_selectLabel1_17->setVisible(true);
	} else if(m_color1 == 17) {
		m_selectLabel1_18->setVisible(true);
	} else if(m_color1 == 18) {
		m_selectLabel1_19->setVisible(true);
	} else if(m_color1 == 19) {
		m_selectLabel1_20->setVisible(true);
	}

	if(m_color2 == 0) {
		m_selectLabel2_1->setVisible(true);
	} else if(m_color2 == 1) {
		m_selectLabel2_2->setVisible(true);
	} else if(m_color2 == 2) {
		m_selectLabel2_3->setVisible(true);
	} else if(m_color2 == 3) {
		m_selectLabel2_4->setVisible(true);
	} else if(m_color2 == 4) {
		m_selectLabel2_5->setVisible(true);
	} else if(m_color2 == 5) {
		m_selectLabel2_6->setVisible(true);
	} else if(m_color2 == 6) {
		m_selectLabel2_7->setVisible(true);
	} else if(m_color2 == 7) {
		m_selectLabel2_8->setVisible(true);
	} else if(m_color2 == 8) {
		m_selectLabel2_9->setVisible(true);
	} else if(m_color2 == 9) {
    	m_selectLabel2_10->setVisible(true);
	} else if(m_color2 == 10) {
		m_selectLabel2_11->setVisible(true);
	} else if(m_color2 == 11) {
		m_selectLabel2_12->setVisible(true);
	} else if(m_color2 == 12) {
		m_selectLabel2_13->setVisible(true);
	} else if(m_color2 == 13) {
		m_selectLabel2_14->setVisible(true);
	} else if(m_color2 == 14) {
		m_selectLabel2_15->setVisible(true);
	} else if(m_color2 == 15) {
		m_selectLabel2_16->setVisible(true);
	} else if(m_color2 == 16) {
		m_selectLabel2_17->setVisible(true);
	} else if(m_color2 == 17) {
		m_selectLabel2_18->setVisible(true);
	} else if(m_color2 == 18) {
		m_selectLabel2_19->setVisible(true);
	} else if(m_color2 == 19) {
		m_selectLabel2_20->setVisible(true);
	}

    return true;
}

void ColorSettingScene::colorSelected(Ref *pSender) {

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");

    MenuItem* pMenuItem = (MenuItem *)(pSender);
    int tag = (int)pMenuItem->getTag();

	char tagStr[5];
	sprintf(tagStr, "%d h", tag);

	int player = UserDefault::getInstance()->getIntegerForKey("player" , 0);

    // 背景色
    m_color1 = UserDefault::getInstance()->getIntegerForKey("color1", 0);
    m_color2 = UserDefault::getInstance()->getIntegerForKey("color2", 1);

	log("color:%d", tag);

	if(player == 0) {
		if(m_color2 == tag-1) {
			return;
		}
		m_selectLabel1_1->setVisible(false);
		m_selectLabel1_2->setVisible(false);
		m_selectLabel1_3->setVisible(false);
		m_selectLabel1_4->setVisible(false);
		m_selectLabel1_5->setVisible(false);
		m_selectLabel1_6->setVisible(false);
		m_selectLabel1_7->setVisible(false);
		m_selectLabel1_8->setVisible(false);
		m_selectLabel1_9->setVisible(false);
		m_selectLabel1_10->setVisible(false);
		m_selectLabel1_11->setVisible(false);
		m_selectLabel1_12->setVisible(false);
		m_selectLabel1_13->setVisible(false);
		m_selectLabel1_14->setVisible(false);
		m_selectLabel1_15->setVisible(false);
		m_selectLabel1_16->setVisible(false);
		m_selectLabel1_17->setVisible(false);
		m_selectLabel1_18->setVisible(false);
		m_selectLabel1_19->setVisible(false);
		m_selectLabel1_20->setVisible(false);
	} else {
		if(m_color1 == tag-1) {
			return;
		}
		m_selectLabel2_1->setVisible(false);
		m_selectLabel2_2->setVisible(false);
		m_selectLabel2_3->setVisible(false);
		m_selectLabel2_4->setVisible(false);
		m_selectLabel2_5->setVisible(false);
		m_selectLabel2_6->setVisible(false);
		m_selectLabel2_7->setVisible(false);
		m_selectLabel2_8->setVisible(false);
		m_selectLabel2_9->setVisible(false);
		m_selectLabel2_10->setVisible(false);
		m_selectLabel2_11->setVisible(false);
		m_selectLabel2_12->setVisible(false);
		m_selectLabel2_13->setVisible(false);
		m_selectLabel2_14->setVisible(false);
		m_selectLabel2_15->setVisible(false);
		m_selectLabel2_16->setVisible(false);
		m_selectLabel2_17->setVisible(false);
		m_selectLabel2_18->setVisible(false);
		m_selectLabel2_19->setVisible(false);
		m_selectLabel2_20->setVisible(false);
	}

	if(tag == 1) {
		if(player == 0) {
			m_selectLabel1_1->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 0);
		} else {
			m_selectLabel2_1->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 0);
		}
	} else if(tag == 2) {
		if(player == 0) {
			m_selectLabel1_2->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 1);
		} else {
			m_selectLabel2_2->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 1);
		}
	} else if(tag == 3) {
		if(player == 0) {
			m_selectLabel1_3->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 2);
		} else {
			m_selectLabel2_3->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 2);
		}
	} else if(tag == 4) {
		if(player == 0) {
			m_selectLabel1_4->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 3);
		} else {
			m_selectLabel2_4->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 3);
		}
	} else if(tag == 5) {
		if(player == 0) {
			m_selectLabel1_5->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 4);
		} else {
			m_selectLabel2_5->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 4);
		}
	} else if(tag == 6) {
		if(player == 0) {
			m_selectLabel1_6->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 5);
		} else {
			m_selectLabel2_6->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 5);
		}
	} else if(tag == 7) {
		if(player == 0) {
			m_selectLabel1_7->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 6);
		} else {
			m_selectLabel2_7->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 6);
		}
	} else if(tag == 8) {
		if(player == 0) {
			m_selectLabel1_8->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 7);
		} else {
			m_selectLabel2_8->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 7);
		}
	} else if(tag == 9) {
		if(player == 0) {
			m_selectLabel1_9->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 8);
		} else {
			m_selectLabel2_9->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 8);
		}
	} else if(tag == 10) {
		if(player == 0) {
			m_selectLabel1_10->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 9);
		} else {
			m_selectLabel2_10->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 9);
		}
	} else if(tag == 11) {
		if(player == 0) {
			m_selectLabel1_11->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 10);
		} else {
			m_selectLabel2_11->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 10);
		}
	} else if(tag == 12) {
		if(player == 0) {
			m_selectLabel1_12->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 11);
		} else {
			m_selectLabel2_12->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 11);
		}
	} else if(tag == 13) {
		if(player == 0) {
			m_selectLabel1_13->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 12);
		} else {
			m_selectLabel2_13->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 12);
		}
	} else if(tag == 14) {
		if(player == 0) {
			m_selectLabel1_14->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 13);
		} else {
			m_selectLabel2_14->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 13);
		}
	} else if(tag == 15) {
		if(player == 0) {
			m_selectLabel1_15->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 14);
		} else {
			m_selectLabel2_15->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 14);
		}
	} else if(tag == 16) {
		if(player == 0) {
			m_selectLabel1_16->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 15);
		} else {
			m_selectLabel2_16->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 15);
		}
	} else if(tag == 17) {
		if(player == 0) {
			m_selectLabel1_17->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 16);
		} else {
			m_selectLabel2_17->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 16);
		}
	} else if(tag == 18) {
		if(player == 0) {
			m_selectLabel1_18->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 17);
		} else {
			m_selectLabel2_18->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 17);
		}
	} else if(tag == 19) {
		if(player == 0) {
			m_selectLabel1_19->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 18);
		} else {
			m_selectLabel2_19->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 18);
		}
	} else if(tag == 20) {
		if(player == 0) {
			m_selectLabel1_20->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color1", 19);
		} else {
			m_selectLabel2_20->setVisible(true);
			UserDefault::getInstance()->setIntegerForKey("color2", 19);
		}
	}

}

// button action
void ColorSettingScene::backButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    Scene *nextScene;
    if(m_rule == RULE_KIREMAKE) {
    	nextScene = TransitionSlideInL::create(0.1f, KiremakeSettingScene::scene());
    } else if(m_rule == RULE_BYOYOMI) {
        nextScene = TransitionSlideInL::create(0.1f, ByoYomiSettingScene::scene());
    } else if(m_rule == RULE_FISHER) {
        nextScene = TransitionSlideInL::create(0.1f, FisherSettingScene::scene());
    } else if(m_rule == RULE_CANADA) {
        nextScene = TransitionSlideInL::create(0.1f, CanadaSettingScene::scene());
    } else if(m_rule == RULE_XIANQQI) {
        nextScene = TransitionSlideInL::create(0.1f, XianqqiSettingScene::scene());
    } else {
    	nextScene = TransitionSlideInL::create(0.1f, KiremakeSettingScene::scene());
    }

    Director::getInstance()->replaceScene(nextScene);
}
