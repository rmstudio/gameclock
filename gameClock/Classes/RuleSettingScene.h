
#include "cocos2d.h"
#include "DialogLayer.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class RuleSettingScene : public cocos2d::LayerGradient {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(RuleSettingScene);
    
    Size m_winSize;

    int m_rule;

    Sprite *m_ruleSettingButtonItem;
    Sprite *m_ruleContentsButtonItem;

    MenuItemImage *m_settingBtnItem;
    MenuItemImage *m_backButtonItem;

    Menu *m_settingButton;
    Menu *m_backButton;

    // 設定画面背景色
    Color4B m_settingBgColorStart;
    Color4B m_settingBgColorEnd;

    // フォントカラー
    Color3B m_fontColor;

    cocos2d::Label *m_explanation;					// ルール説明

    Sprite *m_slider_arrow_left;
    Sprite *m_slider_arrow_right;

    ui::PageView* m_pageView;

    void backButtonDidPushed(Ref* pSender);

    void pageEvent(Ref *pSender, ui::PageView::EventType type);

    void setExplanation();

};


