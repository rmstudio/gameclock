#include "ScrollMenu.h"

USING_NS_CC;

ScrollMenu::ScrollMenu() {
	this->_inMoving = false;
}

ScrollMenu * ScrollMenu::create(MenuItem* item, ...)
{
    va_list args;
    va_start(args,item);

    ScrollMenu *ret = ScrollMenu::createWithItems(item, args);

    va_end(args);

    return ret;
}

ScrollMenu* ScrollMenu::createWithArray(const Vector<MenuItem*>& arrayOfItems)
{
    auto ret = new (std::nothrow) ScrollMenu();
    if (ret && ret->initWithArray(arrayOfItems))
    {
        ret->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(ret);
    }

    return ret;
}

ScrollMenu* ScrollMenu::createWithItems(MenuItem* item, va_list args)
{
    Vector<MenuItem*> items;
    if( item )
    {
        items.pushBack(item);
        MenuItem *i = va_arg(args, MenuItem*);
        while(i)
        {
            items.pushBack(i);
            i = va_arg(args, MenuItem*);
        }
    }

    return ScrollMenu::createWithArray(items);
}

bool ScrollMenu::initWithArray(const Vector<MenuItem*>& arrayOfItems)
{
    if (Layer::init())
    {
        _enabled = true;
        // menu in the center of the screen
        Size s = Director::getInstance()->getWinSize();

        this->ignoreAnchorPointForPosition(true);
        setAnchorPoint(Vec2(0.5f, 0.5f));
        this->setContentSize(s);

        setPosition(s.width/2, s.height/2);

        int z=0;

        for (auto& item : arrayOfItems)
        {
            this->addChild(item, z);
            z++;
        }

        _selectedItem = nullptr;
        _state = Menu::State::WAITING;

        setCascadeColorEnabled(true);
        setCascadeOpacityEnabled(true);


        auto touchListener = EventListenerTouchOneByOne::create();

//        touchListener->setSwallowTouches(true);
        touchListener->setSwallowTouches(false);

        touchListener->onTouchBegan = CC_CALLBACK_2(Menu::onTouchBegan, this);
        touchListener->onTouchMoved = CC_CALLBACK_2(Menu::onTouchMoved, this);
        touchListener->onTouchEnded = CC_CALLBACK_2(Menu::onTouchEnded, this);
        touchListener->onTouchCancelled = CC_CALLBACK_2(Menu::onTouchCancelled, this);

        _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

        return true;
    }
    return false;
}

void ScrollMenu::onTouchMoved(Touch* touch, Event* event) {
    if(5.0f < touch->getLocationInView().distance(this->_touchStartPoint)) {
    	this->_inMoving = true;
    	return Menu::onTouchMoved(touch, event);
    }
}

bool ScrollMenu::onTouchBegan(Touch* touch, Event* event) {
	this->_inMoving = false;
	this->_touchStartPoint = touch->getLocationInView();
    return Menu::onTouchBegan(touch, event);
}

void ScrollMenu::onTouchEnded(Touch *touch, Event *event) {
    if(this->_inMoving) {
        Menu::onTouchCancelled(touch, event);
    } else {
        Menu::onTouchEnded(touch, event);
    }
}
