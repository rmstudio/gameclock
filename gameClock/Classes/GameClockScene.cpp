#include "GameClockScene.h"
#include "Setting/KiremakeSettingScene.h"
#include "Setting/ByoYomiSettingScene.h"
#include "Setting/FisherSettingScene.h"
#include "Setting/CanadaSettingScene.h"
#include "Setting/XianqqiSettingScene.h"
#include "ParamDisplayPanel.h"
#include "cocos-ext.h"
#include "SimpleAudioEngine.h"

enum TURN {
    TURN_SENTE   = 0,
    TURN_GOTE  = 1,
};

enum RULE {
    RULE_KIREMAKE   = 0,
    RULE_BYOYOMI  = 1,
    RULE_FISHER  = 2,
    RULE_CANADA  = 3,
    RULE_XIANQQI  = 4,
};

enum XIANQQI_TIME_MODE {
    XIANQQI_MOCHIJIKAN_MODE1   = 0,
    XIANQQI_MOCHIJIKAN_MODE2  = 1,
    XIANQQI_MOCHIJIKAN_MODE3  = 2,
    XIANQQI_BYOYOMI_MODE  = 3,
};

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

Scene* GameClockScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    GameClockScene *layer = GameClockScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameClockScene::init()
{
    
    Color4B startColor = Color4B(64, 132, 31, 255);
    Color4B endColor  = Color4B(64, 132, 31, 255);

    m_grayFont = Color3B(149, 165, 166);
    
    if ( !LayerGradient::initWithColor( startColor, endColor ) )
    {
        return false;
    }
    
    m_winSize = Director::getInstance()->getWinSize();
    
    m_start = false;
    m_byoYomiMode1 = false;						// 秒読みモードフラグ（プレイヤー１）
    m_byoYomiMode2 = false;						// 秒読みモードフラグ（プレイヤー２）
    m_canadaOverTimeMode1 = false;				// カナダ式 超過時間フラグ（プレイヤー１）
    m_canadaOverTimeMode2 = false;				// カナダ式 超過時間フラグ（プレイヤー２）
    m_move = 0;									// 手数
    m_move1 = 1;								// 手数（プレイヤー１）
    m_move2 = 1;								// 手数（プレイヤー２）

    m_gameTime = 2;
    sprintf(m_gameTimeStr, "%d", m_gameTime);
    
    loadSaveParams();

    // サウンド
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0f);
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/tap.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/pause.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/change_scene.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/gameover.mp3");

    // ルール
    m_rule = UserDefault::getInstance()->getIntegerForKey("rule", 0);

    // 持ち時間取得
    m_hour1 = UserDefault::getInstance()->getIntegerForKey("hour1");
    m_minite1 = UserDefault::getInstance()->getIntegerForKey("minite1");
    m_second1 = 0;
    m_hour2 = UserDefault::getInstance()->getIntegerForKey("hour2");
    m_minite2 = UserDefault::getInstance()->getIntegerForKey("minite2");
    m_second2 = 0;
    
    m_turn = TURN_SENTE;
    m_settingMode1 = false;
    m_settingMode2 = false;
    m_canadaOverTimeMode1 = false;
    m_canadaOverTimeMode2 = false;
    
    // 背景色
    setBackGroundColor();

    if(m_hour1 > 0) {
    	sprintf(m_senteTimeStr, "%02d:%02d:%02d", m_hour1, m_minite1, m_second1);
    } else {
    	sprintf(m_senteTimeStr, "%02d:%02d", m_minite1, m_second1);
    }

    if(m_hour2 > 0) {
    	sprintf(m_goteTimeStr, "%02d:%02d:%02d", m_hour1, m_minite1, m_second1);
    } else {
    	sprintf(m_goteTimeStr, "%02d:%02d", m_minite2, m_second2);
    }

	// 時間（自分）
    if(m_hour1 > 0) {
		m_senteTimeLabel = Label::createWithSystemFont(m_senteTimeStr, "HiraKakuProN-W6", 170);
		m_senteTimeLabel->setPosition( Point(m_senteArea->getContentSize().width / 2, m_senteArea->getContentSize().height / 2) );
		m_senteArea->addChild(m_senteTimeLabel, 1);
    } else {
		m_senteTimeLabel = Label::createWithSystemFont(m_senteTimeStr, "HiraKakuProN-W6", 250);
		m_senteTimeLabel->setPosition( Point(m_senteArea->getContentSize().width / 2, m_senteArea->getContentSize().height / 2) );
		m_senteArea->addChild(m_senteTimeLabel, 1);
    }

    if(m_color1 == 14) {
    	m_senteTimeLabel->setColor(m_grayFont);
    }

    if(m_rule == RULE_BYOYOMI) {
    	showByoYomiTimeLabel();
    } else if(m_rule == RULE_FISHER) {
    	showFisherTimeLabel();
    } else if(m_rule == RULE_CANADA) {
    	showCanadaTimeLabel();
    } else if(m_rule == RULE_XIANQQI) {
    	showXianqqiTimeLabel();
    }

    if(m_hour2 > 0) {
		m_goteTimeLabel = Label::createWithSystemFont(m_goteTimeStr, "HiraKakuProN-W6", 170);
		m_goteTimeLabel->setPosition( Point(m_goteArea->getContentSize().width / 2, m_goteArea->getContentSize().height / 2) );
		m_goteArea->addChild(m_goteTimeLabel, 1);
    } else {
		m_goteTimeLabel = Label::createWithSystemFont(m_goteTimeStr, "HiraKakuProN-W6", 250);
		m_goteTimeLabel->setPosition( Point(m_goteArea->getContentSize().width / 2, m_goteArea->getContentSize().height / 2) );
		m_goteArea->addChild(m_goteTimeLabel, 1);
    }

    if(m_color2 == 14) {
    	m_goteTimeLabel->setColor(m_grayFont);
    }

    RotateBy* rotateTimeLabel = RotateBy::create(0.0f, 180);
    m_goteTimeLabel->runAction(rotateTimeLabel);

    // 手数（プレイヤー１）
    sprintf(m_senteMoveStr, "%d 手", m_move1);
    m_senteMoveLabel = Label::createWithSystemFont(m_senteMoveStr, "HiraKakuProN-W6", 60);
    m_senteMoveLabel->setPosition( Point(m_senteArea->getContentSize().width - 80, m_senteArea->getContentSize().height - 60) );
    m_senteArea->addChild(m_senteMoveLabel, 1);

    if(m_color1 == 14) {
    	m_senteMoveLabel->setColor(m_grayFont);
    }

	// 手数（プレイヤー２）
    sprintf(m_goteMoveStr, "%d 手", m_move2);
    m_goteMoveLabel = Label::createWithSystemFont(m_goteMoveStr, "HiraKakuProN-W6", 60);
    m_goteMoveLabel->setPosition( Point(80, 60) );
    m_goteArea->addChild(m_goteMoveLabel, 1);

    if(m_color2 == 14) {
    	m_goteMoveLabel->setColor(m_grayFont);
    }

	RotateBy* rotateMoveCountLabel = RotateBy::create(0.0f, 180);
	m_goteMoveLabel->runAction(rotateMoveCountLabel);

    m_senteMaskLayer = LayerColor::create(Color4B(0, 0, 0, 50));
    m_senteMaskLayer->setContentSize(Size(m_senteArea->getContentSize().width, m_senteArea->getContentSize().height));
    m_senteMaskLayer->setPosition(Point(0, 0));
    m_senteMaskLayer->setTag(130);
    this->addChild( m_senteMaskLayer , 110);

    m_goteMaskLayer = LayerColor::create(Color4B(0, 0, 0, 50));
    m_goteMaskLayer->setContentSize(Size(m_goteArea->getContentSize().width, m_goteArea->getContentSize().height));
    m_goteMaskLayer->setPosition(Point(0, m_winSize.height - m_goteArea->getContentSize().height));
    m_goteMaskLayer->setTag(130);
    this->addChild( m_goteMaskLayer , 110);

    m_senteMaskLayer->setVisible(false);
    m_goteMaskLayer->setVisible(true);

    // グレーアウト
    m_pauseLayerbg = LayerColor::create(Color4B(0, 0, 0, 100));
    m_pauseLayerbg->setContentSize(Size(m_winSize.width, m_winSize.height));
    m_pauseLayerbg->setPosition(Point(0, 0));
    m_pauseLayerbg->setTag(120);
    this->addChild( m_pauseLayerbg , 300);

    // スタート時説明
    m_tapRequreText = Label::createWithSystemFont("画面をタップすると\nゲームを開始します。", "HiraKakuProN-W6", 48);
    m_tapRequreText->setPosition( Point(m_winSize.width / 2, m_winSize.height / 2) );
    m_tapRequreText->setColor(Color3B(255, 255, 255));
    m_pauseLayerbg->addChild(m_tapRequreText, 350);

    // ポーズボタン    
    m_pauseButtonItem = MenuItemImage::create(
												  "res/pause_button.png",
												  "res/pause_button.png",
												  CC_CALLBACK_1(GameClockScene::pauseButtonDidPushed, this));
    m_pauseButtonItem->setPosition( Point(m_winSize.width/2, m_winSize.height/2) );
    m_pauseButton = Menu::create(m_pauseButtonItem, NULL);
    m_pauseButton->setPosition( Point::ZERO );
    m_pauseButton->setEnabled(false);
    this->addChild(m_pauseButton, 200);

    m_eventDispatcher = Director::getInstance()->getEventDispatcher();
    m_listener = EventListenerTouchOneByOne::create();

    m_listener->onTouchBegan = CC_CALLBACK_2(GameClockScene::onTouchBegan, this);
    m_listener->onTouchMoved = CC_CALLBACK_2(GameClockScene::onTouchMoved, this);
    m_listener->onTouchEnded = CC_CALLBACK_2(GameClockScene::onTouchEnded, this);

    m_eventDispatcher->addEventListenerWithSceneGraphPriority(m_listener, this);

    return true;
}

void GameClockScene::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void GameClockScene::loadSaveParams() {

    // 持ち時間
    m_hour1 = UserDefault::getInstance()->getIntegerForKey("hour1" , 0);
    m_hour2 = UserDefault::getInstance()->getIntegerForKey("hour2", 0);
    m_minite1 = UserDefault::getInstance()->getIntegerForKey("minite1" , 30);
    m_minite2 = UserDefault::getInstance()->getIntegerForKey("minite2", 30);

    // 背景色
    m_color1 = UserDefault::getInstance()->getIntegerForKey("color1", 0);
    m_color2 = UserDefault::getInstance()->getIntegerForKey("color2", 1);

    // ルール
    m_rule = UserDefault::getInstance()->getIntegerForKey("rule", 0);

    // 秒読み時間
    m_byo_yomi_time1 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_time1", 30);
    m_byo_yomi_time2 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_time2", 30);

    // 秒読み考慮回数
    m_byo_yomi_kouryo_cnt1 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_cnt1", 3);
    m_byo_yomi_kouryo_cnt2 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_cnt2", 3);

    // 最大秒読み考慮回数
    m_byo_yomi_kouryo_cnt_max1 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_cnt1", 3);
    m_byo_yomi_kouryo_cnt_max2 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_cnt2", 3);

    // フィッシャー加算時間
    m_fisher_additional_time1 = UserDefault::getInstance()->getIntegerForKey("fisher_additional_time1", 10);
    m_fisher_additional_time2 = UserDefault::getInstance()->getIntegerForKey("fisher_additional_time2", 10);

    // カナダ式時間
    m_canada_kitei_time1 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_time1", 30);
    m_canada_kitei_time2 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_time2", 30);

    // カナダ式規定手数（現在値）
    m_canada_kitei_tekazu_current1 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_tekazu1", 10);
    m_canada_kitei_tekazu_current2 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_tekazu2", 10);

    // カナダ式規定手数
    m_canada_kitei_tekazu1 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_tekazu1", 10);
    m_canada_kitei_tekazu2 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_tekazu2", 10);

    // シャンチー国際 規定手数１
    m_xianqqi_kitei_tekazu1_1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu1_1", 15);
    m_xianqqi_kitei_tekazu1_2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu1_2", 15);

    // シャンチー国際 規定手数１（現在値）
    m_xianqqi_kitei_tekazu_current1_1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu1_1", 15);
    m_xianqqi_kitei_tekazu_current1_2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu1_2", 15);

    // シャンチー国際 持ち時間２
    m_xianqqi_moti_jikan2_1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_moti_jikan2_1", 10);
    m_xianqqi_moti_jikan2_2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_moti_jikan2_2", 10);

    // シャンチー国際 規定手数２
    m_xianqqi_kitei_tekazu2_1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu2_1", 5);
    m_xianqqi_kitei_tekazu2_2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu2_2", 5);

    // シャンチー国際 規定手数２（現在値）
    m_xianqqi_kitei_tekazu_current2_1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu2_1", 5);
    m_xianqqi_kitei_tekazu_current2_2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu2_2", 5);

    // シャンチー国際 規定回数
    m_xianqqi_kitei_cnt1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_cnt1", 5);
    m_xianqqi_kitei_cnt2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_cnt2", 5);

    // シャンチー国際 規定回数（現在値）
    m_xianqqi_kitei_cnt_current1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_cnt1", 5);
    m_xianqqi_kitei_cnt_current2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_cnt2", 5);

    // シャンチー国際 秒読み
    m_xianqqi_byo_yomi_time1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_byo_yomi_time1");
    m_xianqqi_byo_yomi_time2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_byo_yomi_time2");
}

void GameClockScene::updateTimer(float delta)
{
        
    if(m_turn == TURN_SENTE) {

    	m_senteTimeLabel->setOpacity(255);
    	m_goteTimeLabel->setOpacity(150);

    	if(m_second1 == 0) {				// 秒が0のとき
    		if(m_minite1 == 0) {			// 秒が0かつ分が0のとき
    			if(m_hour1 == 0) {			// 秒が0かつ分が0かつ時間が0のとき
    				endJudge(TURN_SENTE);
    			} else {					// 秒が0かつ分が0かつ時間が0でないとき
    				m_hour1--;
    				m_minite1 = 59;
    				m_second1 = 59;
    			}
    		} else {						// 分が0でないとき
    			if(m_hour1 == 0) {			// 秒が0かつ分が0でないかつ時間が0のとき
    				m_minite1--;
    				m_second1 = 59;
    			} else {					// 秒が0かつ分が0でないかつ時間が0でないとき
    				m_minite1--;
    				m_second1 = 59;
    			}
    		}
    	} else {							// 秒が0でないとき
    		m_second1--;
    		if(m_hour1 == 0 && m_minite1 == 0 && m_second1 == 0) {
    			endJudge(TURN_SENTE);
    		}
    	}
        
    	updateClockTime(TURN_SENTE);

    } else {

    	m_senteTimeLabel->setOpacity(150);
    	m_goteTimeLabel->setOpacity(255);

    	if(m_second2 == 0) {				// 秒が0のとき
    		if(m_minite2 == 0) {			// 秒が0かつ分が0のとき
    			if(m_hour2 == 0) {			// 秒が0かつ分が0かつ時間が0のとき
    				// 時・分・秒が0のとき終了判定
    				endJudge(TURN_GOTE);
    			} else {					// 秒が0かつ分が0かつ時間が0でないとき
    				m_hour2--;
    				m_minite2 = 59;
    				m_second2 = 59;
    			}
    		} else {						// 分が0でないとき
    			if(m_hour2 == 0) {			// 秒が0かつ分が0でないかつ時間が0のとき
    				m_minite2--;
    				m_second2 = 59;
    			} else {					// 秒が0かつ分が0かつ時間が0でないとき
    				m_minite2 = 59;
    				m_second2 = 59;
    			}
    		}
    	} else {							// 秒が0でないとき
    		m_second2--;
    		if(m_hour2 == 0 && m_minite2 == 0 && m_second2 == 0) {
    			// 時・分・秒が0のとき終了判定
    			endJudge(TURN_GOTE);
    		}
    	}

    	updateClockTime(TURN_GOTE);
        
    }

}

void GameClockScene::endTimer()
{
    this->unschedule(schedule_selector(GameClockScene::updateTimer));
}

bool GameClockScene::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    log("Touched!");
    Point touchPoint = Vec2(pTouch->getLocationInView().x, pTouch->getLocationInView().y);

    if(m_start == false) {
    	log("start!");
    	m_start = true;
		m_turn = TURN_SENTE;
		m_pauseButton->setEnabled(true);
		this->removeChild(m_tapRequreText , true);
		this->removeChild(m_pauseLayerbg , true);
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");
		this->schedule(schedule_selector(GameClockScene::updateTimer), 1.0);
    } else {

    	if(m_turn == TURN_SENTE) {
    		if(touchPoint.y <= m_winSize.height / 2 + 160) {
    			return false;
    		}
    	} else {
    		if(touchPoint.y >= m_winSize.height / 2 - 160) {
    			return false;
    		}
    	}

    	log("change turn!");

        this->pause();

        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/tap.mp3");

        m_move++;

        if(m_turn == TURN_SENTE) {
            m_senteMaskLayer->setVisible(true);
            m_goteMaskLayer->setVisible(false);
            m_turn = TURN_GOTE;

            m_move2++;
            sprintf(m_goteMoveStr, "%d 手", m_move2 - 1);
            m_goteMoveLabel->setString(m_goteMoveStr);

            if(m_rule == RULE_BYOYOMI) {
            	if(m_byoYomiMode1) {
            		m_second1 = m_byo_yomi_time1;
            		updateClockTime(TURN_SENTE);
            	}
            } else if(m_rule == RULE_FISHER) {
            	updateFisherAdditionalTimeLabel(TURN_SENTE);
            } else if(m_rule == RULE_CANADA) {
            	if(m_canadaOverTimeMode1) {
            		m_canada_kitei_tekazu_current1--;
            		if(m_canada_kitei_tekazu_current1 > 0) {
                		m_senteCanadaKiteiTekazuPanel->setValue(m_canada_kitei_tekazu_current1, m_canada_kitei_tekazu1);
            		} else {
                		m_minite1 = m_minite1 + m_canada_kitei_time1;
                		if(m_minite1 >= 60) {
                			m_hour1 = m_hour1 + 1;
                			m_minite1 = m_minite1 - 60;
                		}
                		updateClockTime(TURN_SENTE);

                		m_senteCanadaKiteiTekazuPanel->setValue(m_canada_kitei_tekazu1, m_canada_kitei_tekazu1);
                		m_canada_kitei_tekazu_current1 = m_canada_kitei_tekazu1;
            		}
            	}
            } else if(m_rule == RULE_XIANQQI) {
            	if(m_xianqqi_time_mode1 == XIANQQI_MOCHIJIKAN_MODE1) {

            		m_xianqqi_kitei_tekazu_current1_1--;

            		if(m_xianqqi_kitei_tekazu_current1_1 > 0) {
            			m_senteXianqqiKiteiTekazuPanel->setValue(m_xianqqi_kitei_tekazu_current1_1, m_xianqqi_kitei_tekazu1_1);
            		} else {
                		// 持ち時間１内に規定手数を着手した場合、持ち時間２が追加され、持ち時間２モードに移る
                		m_minite1 = m_minite1 + m_xianqqi_moti_jikan2_1;
                		if(m_minite1 >= 60) {
                			m_hour1++;
                			m_minite1 = m_minite1 - 60;
                		}
                		updateClockTime(TURN_SENTE);
                		m_xianqqi_time_mode1 = XIANQQI_MOCHIJIKAN_MODE2;
            		}
            	} else if(m_xianqqi_time_mode1 == XIANQQI_MOCHIJIKAN_MODE2) {

            		m_xianqqi_kitei_tekazu_current2_1--;

            		if(m_xianqqi_kitei_tekazu_current2_1 > 0) {
            			m_senteXianqqiKiteiTekazu2Panel->setValue(m_xianqqi_kitei_tekazu_current2_1, m_xianqqi_kitei_tekazu2_1);
            		} else {
            			// 持ち時間２が追加
            			m_minite1 =  m_minite1 + m_xianqqi_moti_jikan2_1;
                		if(m_minite1 >= 60) {
                			m_hour1++;
                			m_minite1 = m_minite1 - 60;
                		}
                		updateClockTime(TURN_SENTE);
            			m_xianqqi_kitei_cnt_current1--;				// 規定回数を減らす
            			m_senteXianqqiKiteiCountPanel->setValue(m_xianqqi_kitei_cnt_current1, m_xianqqi_kitei_cnt1);
            			if(m_xianqqi_kitei_cnt_current1 == 0) {		// 規定回数が0になったら
            				m_xianqqi_time_mode1 = XIANQQI_MOCHIJIKAN_MODE3;	// 持ち時間3モードにする
            			} else {
            				// 規定手数２を最大値に戻す
                			m_xianqqi_kitei_tekazu_current2_1 = m_xianqqi_kitei_tekazu2_1;
                			m_senteXianqqiKiteiTekazu2Panel->setValue(m_xianqqi_kitei_tekazu_current2_1, m_xianqqi_kitei_tekazu2_1);
            			}
            		}
            	} else if(m_xianqqi_time_mode1 == XIANQQI_BYOYOMI_MODE) {
        			// 秒読み時間が追加
        			m_second1 =  m_xianqqi_byo_yomi_time1;
            		updateClockTime(TURN_SENTE);
            	}
            }
        } else {
            m_senteMaskLayer->setVisible(false);
            m_goteMaskLayer->setVisible(true);
            m_turn = TURN_SENTE;
            m_move1++;
            sprintf(m_senteMoveStr, "%d 手", m_move1);
            m_senteMoveLabel->setString(m_senteMoveStr);

            if(m_rule == RULE_BYOYOMI) {
            	if(m_byoYomiMode2) {
            		m_second2 = m_byo_yomi_time2;
            		updateClockTime(TURN_GOTE);
            	}
            } else if(m_rule == RULE_FISHER) {
            	updateFisherAdditionalTimeLabel(TURN_GOTE);
            } else if(m_rule == RULE_CANADA) {
            	if(m_canadaOverTimeMode2) {
            		m_canada_kitei_tekazu_current2--;
            		if(m_canada_kitei_tekazu_current2 > 0) {
            			m_goteCanadaKiteiTekazuPanel->setValue(m_canada_kitei_tekazu_current2, m_canada_kitei_tekazu2);
            		} else {
						m_minite2 = m_canada_kitei_time2;
                		if(m_minite2 >= 60) {
                			m_hour2 = m_hour2 + 1;
                			m_minite2 = m_minite2 - 60;
                		}
						updateClockTime(TURN_GOTE);

                		m_goteCanadaKiteiTekazuPanel->setValue(m_canada_kitei_tekazu2, m_canada_kitei_tekazu2);
						m_canada_kitei_tekazu_current2 = m_canada_kitei_tekazu2;
            		}
            	}
            } else if(m_rule == RULE_XIANQQI) {
            	if(m_xianqqi_time_mode2 == XIANQQI_MOCHIJIKAN_MODE1) {

            		m_xianqqi_kitei_tekazu_current1_2--;

            		if(m_xianqqi_kitei_tekazu_current1_2 > 0) {
            			m_goteXianqqiKiteiTekazuPanel->setValue(m_xianqqi_kitei_tekazu_current1_2, m_xianqqi_kitei_tekazu1_2);
            		} else {
                		// 持ち時間１内に規定手数を着手した場合、持ち時間２が追加され、持ち時間２モードに移る
                		m_minite2 = m_minite2 + m_xianqqi_moti_jikan2_2;
                		if(m_minite2 >= 60) {
                			m_hour2++;
                			m_minite2 = m_minite2 - 60;
                		}
                		updateClockTime(TURN_GOTE);
                		m_xianqqi_time_mode2 = XIANQQI_MOCHIJIKAN_MODE2;
            		}
            	} else if(m_xianqqi_time_mode2 == XIANQQI_MOCHIJIKAN_MODE2) {

            		m_xianqqi_kitei_tekazu_current2_2--;

            		if(m_xianqqi_kitei_tekazu_current2_2 > 0) {
            			m_goteXianqqiKiteiTekazu2Panel->setValue(m_xianqqi_kitei_tekazu_current2_2, m_xianqqi_kitei_tekazu2_2);
            		} else {
            			// 持ち時間２が追加
            			m_minite2 =  m_minite2 + m_xianqqi_moti_jikan2_2;
                		if(m_minite2 >= 60) {
                			m_hour2++;
                			m_minite2 = m_minite2 - 60;
                		}
                		updateClockTime(TURN_GOTE);
            			m_xianqqi_kitei_cnt_current2--;										// 規定回数を減らす
            			m_goteXianqqiKiteiCountPanel->setValue(m_xianqqi_kitei_cnt_current2, m_xianqqi_kitei_cnt2);
            			if(m_xianqqi_kitei_cnt_current2 == 0) {					// 規定回数が0になったら
            				m_xianqqi_time_mode2 = XIANQQI_MOCHIJIKAN_MODE3;	// 持ち時間３モードにする
            			} else {
            				// 規定手数２を最大値に戻す
                			m_xianqqi_kitei_tekazu_current2_2 = m_xianqqi_kitei_tekazu2_2;
                			m_goteXianqqiKiteiTekazu2Panel->setValue(m_xianqqi_kitei_tekazu_current2_2, m_xianqqi_kitei_tekazu2_2);
            			}
            		}
            	} else if(m_xianqqi_time_mode2 == XIANQQI_BYOYOMI_MODE) {
        			// 秒読み時間が追加
        			m_second2 =  m_xianqqi_byo_yomi_time2;
            		updateClockTime(TURN_GOTE);
            	}
            }
        }

        this->resume();
    }

    return true;
}

void GameClockScene::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}

void GameClockScene::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}

// ポーズボタン
void GameClockScene::pauseButtonDidPushed(Ref* pSender)
{
    this->pause();

    if(!m_start) {
    	return;
    }

    m_eventDispatcher->pauseEventListenersForTarget(this, true);

    m_pauseButton->setEnabled(false);

    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/pause.mp3");

    // グレーアウト
    m_pauseLayerbg = LayerColor::create(Color4B(0, 0, 0, 100));
    m_pauseLayerbg->setContentSize(Size(m_winSize.width, m_winSize.height));
    m_pauseLayerbg->setPosition(Point(0, 0));
    m_pauseLayerbg->setTag(120);
    this->addChild( m_pauseLayerbg , 300);

    // ポーズダイアログ
    m_pauseDialogLayer = DialogLayer::create();
    m_pauseDialogLayer->setPosition(Point(m_winSize.width/2-300, m_winSize.height/2-175));
    m_pauseDialogLayer->setContentSize( Size(600,350) );
    m_pauseDialogLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
    m_pauseDialogLayer->setTag(110);
    m_pauseDialogLayer->setTitle("ゲーム中断", new __String("pause"));
    m_pauseDialogLayer->setScale(0.5f);
    this->addChild(m_pauseDialogLayer, 400);

    ActionInterval* scaleAction = ScaleTo::create(0.2f, 1.0f);
    ActionInterval* easeAction = EaseElasticOut::create(scaleAction, 1.0f);
    m_pauseDialogLayer->runAction(easeAction);

    // リセットボタン
    MenuItemImage *resetButtonItem = MenuItemImage::create(
                                                               "res/dialog_reset_button.png",
                                                               "res/dialog_reset_button.png",
                                                               CC_CALLBACK_1(GameClockScene::resetButtonDidPushed, this));
    resetButtonItem->setPosition( Vec2(180, 125) );
    Menu* resetButton = Menu::create(resetButtonItem, NULL);
    resetButton->setPosition( Point::ZERO );
    m_pauseDialogLayer->addChild(resetButton, 110);

    // レジュームボタン
    MenuItemImage *resumeButtonItem = MenuItemImage::create(
                                                               "res/dialog_resume_button.png",
                                                               "res/dialog_resume_button.png",
                                                               CC_CALLBACK_1(GameClockScene::resumeButtonDidPushed, this));
    resumeButtonItem->setPosition( Vec2(m_pauseDialogLayer->getContentSize().width-180, 125) );
    Menu* resumeButton = Menu::create(resumeButtonItem, NULL);
    resumeButton->setPosition( Point::ZERO );
    m_pauseDialogLayer->addChild(resumeButton, 110);
}

// リセットボタン押下
void GameClockScene::resetButtonDidPushed(Ref* pSender)
{

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    Scene *nextScene;
    if(m_rule == RULE_KIREMAKE) {
    	nextScene = TransitionSlideInL::create(0.1f, KiremakeSettingScene::scene());
    } else if(m_rule == RULE_BYOYOMI) {
        nextScene = TransitionSlideInL::create(0.1f, ByoYomiSettingScene::scene());
    } else if(m_rule == RULE_FISHER) {
        nextScene = TransitionSlideInL::create(0.1f, FisherSettingScene::scene());
    } else if(m_rule == RULE_CANADA) {
        nextScene = TransitionSlideInL::create(0.1f, CanadaSettingScene::scene());
    } else if(m_rule == RULE_XIANQQI) {
        nextScene = TransitionSlideInL::create(0.1f, XianqqiSettingScene::scene());
    } else {
    	nextScene = TransitionSlideInL::create(0.1f, KiremakeSettingScene::scene());
    }
    Director::getInstance()->replaceScene(nextScene);
}

// レジュームボタン押下
void GameClockScene::resumeButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    this->resume();

    m_eventDispatcher->resumeEventListenersForTarget(this, true);

    m_pauseButton->setEnabled(true);
    this->removeChild(m_pauseDialogLayer , true);
    this->removeChild(m_pauseLayerbg , true);
}

void GameClockScene::setBackGroundColor() {

	m_color1 = UserDefault::getInstance()->getIntegerForKey("color1", 0);
	m_color2 = UserDefault::getInstance()->getIntegerForKey("color2", 1);

    m_colorTurqoise = Color4B(26, 188, 156, 255);
    m_colorGreenSea = Color4B(22, 160, 133, 255);
    m_colorSunFlower = Color4B(241, 196, 15, 255);
    m_colorOrange = Color4B(243, 156, 18, 255);
    m_colorEmerald = Color4B(46, 204, 113, 255);
    m_colorNephritis = Color4B(39, 174, 96, 255);
    m_colorCarrot = Color4B(230, 126, 34, 255);
    m_colorPumpkin = Color4B(211, 84, 0, 255);
    m_colorPeterRiver = Color4B(52, 152, 219, 255);
    m_colorBelizeHole = Color4B(41, 128, 185, 255);
    m_colorAlizarin = Color4B(231, 76, 60, 255);
    m_colorPomegranate = Color4B(192, 57, 43, 255);
    m_colorAmethyst = Color4B(155, 89, 182, 255);
    m_colorWisteria = Color4B(142, 68, 173, 255);
    m_colorClouds = Color4B(251, 252, 252, 255);
    m_colorSilver = Color4B(189, 195, 199, 255);
    m_colorWetAspalt = Color4B(52, 73, 94, 255);
    m_colorMidnightBlue = Color4B(44, 62, 80, 255);
    m_colorConcrete = Color4B(149, 165, 166, 255);
    m_colorAsbestos = Color4B(127, 140, 141, 255);

    if(m_color1 == 0) {
    	m_senteArea = LayerGradient::create(m_colorPeterRiver, m_colorPeterRiver);
    } else if(m_color1 == 1) {
    	m_senteArea = LayerGradient::create(m_colorBelizeHole, m_colorBelizeHole);
    } else if(m_color1 == 2) {
    	m_senteArea = LayerGradient::create(m_colorAlizarin, m_colorAlizarin);
    } else if(m_color1 == 3) {
    	m_senteArea = LayerGradient::create(m_colorPomegranate, m_colorPomegranate);
    } else if(m_color1 == 4) {
    	m_senteArea = LayerGradient::create(m_colorEmerald, m_colorEmerald);
    } else if(m_color1 == 5) {
    	m_senteArea = LayerGradient::create(m_colorNephritis, m_colorNephritis);
    } else if(m_color1 == 6) {
    	m_senteArea = LayerGradient::create(m_colorCarrot, m_colorCarrot);
    } else if(m_color1 == 7) {
    	m_senteArea = LayerGradient::create(m_colorPumpkin, m_colorPumpkin);
    } else if(m_color1 == 8) {
    	m_senteArea = LayerGradient::create(m_colorTurqoise, m_colorTurqoise);
    } else if(m_color1 == 9) {
    	m_senteArea = LayerGradient::create(m_colorGreenSea, m_colorGreenSea);
    } else if(m_color1 == 10) {
    	m_senteArea = LayerGradient::create(m_colorSunFlower, m_colorSunFlower);
    } else if(m_color1 == 11) {
    	m_senteArea = LayerGradient::create(m_colorOrange, m_colorOrange);
    } else if(m_color1 == 12) {
    	m_senteArea = LayerGradient::create(m_colorAmethyst, m_colorAmethyst);
    } else if(m_color1 == 13) {
    	m_senteArea = LayerGradient::create(m_colorWisteria, m_colorWisteria);
    } else if(m_color1 == 14) {
    	m_senteArea = LayerGradient::create(m_colorClouds, m_colorClouds);
    } else if(m_color1 == 15) {
    	m_senteArea = LayerGradient::create(m_colorSilver, m_colorSilver);
    } else if(m_color1 == 16) {
    	m_senteArea = LayerGradient::create(m_colorWetAspalt, m_colorWetAspalt);
    } else if(m_color1 == 17) {
    	m_senteArea = LayerGradient::create(m_colorMidnightBlue, m_colorMidnightBlue);
    } else if(m_color1 == 18) {
    	m_senteArea = LayerGradient::create(m_colorConcrete, m_colorConcrete);
    } else if(m_color1 == 19) {
    	m_senteArea = LayerGradient::create(m_colorAsbestos, m_colorAsbestos);
    }

    if(m_color2 == 0) {
    	m_goteArea = LayerGradient::create(m_colorPeterRiver, m_colorPeterRiver);
    } else if(m_color2 == 1) {
    	m_goteArea = LayerGradient::create(m_colorBelizeHole, m_colorBelizeHole);
    } else if(m_color2 == 2) {
    	m_goteArea = LayerGradient::create(m_colorAlizarin, m_colorAlizarin);
    } else if(m_color2 == 3) {
    	m_goteArea = LayerGradient::create(m_colorPomegranate, m_colorPomegranate);
    } else if(m_color2 == 4) {
    	m_goteArea = LayerGradient::create(m_colorEmerald, m_colorEmerald);
    } else if(m_color2 == 5) {
    	m_goteArea = LayerGradient::create(m_colorNephritis, m_colorNephritis);
    } else if(m_color2 == 6) {
    	m_goteArea = LayerGradient::create(m_colorCarrot, m_colorCarrot);
    } else if(m_color2 == 7) {
    	m_goteArea = LayerGradient::create(m_colorPumpkin, m_colorPumpkin);
    } else if(m_color2 == 8) {
    	m_goteArea = LayerGradient::create(m_colorTurqoise, m_colorTurqoise);
    } else if(m_color2 == 9) {
    	m_goteArea = LayerGradient::create(m_colorGreenSea, m_colorGreenSea);
    } else if(m_color2 == 10) {
    	m_goteArea = LayerGradient::create(m_colorSunFlower, m_colorSunFlower);
    } else if(m_color2 == 11) {
    	m_goteArea = LayerGradient::create(m_colorOrange, m_colorOrange);
    } else if(m_color2 == 12) {
    	m_goteArea = LayerGradient::create(m_colorAmethyst, m_colorAmethyst);
    } else if(m_color2 == 13) {
    	m_goteArea = LayerGradient::create(m_colorWisteria, m_colorWisteria);
    } else if(m_color2 == 14) {
    	m_goteArea = LayerGradient::create(m_colorClouds, m_colorClouds);
    } else if(m_color2 == 15) {
    	m_goteArea = LayerGradient::create(m_colorSilver, m_colorSilver);
    } else if(m_color2 == 16) {
    	m_goteArea = LayerGradient::create(m_colorWetAspalt, m_colorWetAspalt);
    } else if(m_color2 == 17) {
    	m_goteArea = LayerGradient::create(m_colorMidnightBlue, m_colorMidnightBlue);
    } else if(m_color2 == 18) {
    	m_goteArea = LayerGradient::create(m_colorConcrete, m_colorConcrete);
    } else if(m_color2 == 19) {
    	m_goteArea = LayerGradient::create(m_colorAsbestos, m_colorAsbestos);
    }

    // 背景色(先手)
    m_senteArea->setContentSize(Size(m_winSize.width, m_winSize.height / 2 - 160));
    m_senteArea->setPosition(Point(0, 0));
    this->addChild(m_senteArea);

    // 背景色(後手)
    m_goteArea->setContentSize(Size(m_winSize.width, m_winSize.height / 2 - 160));
    m_goteArea->setPosition(Point(0, m_winSize.height / 2 + 160));
    this->addChild(m_goteArea);

}

void GameClockScene::showByoYomiTimeLabel() {

	// 秒読み（プレイヤー１）
	m_senteByoYomiPanel = ParamDisplayPanel::create();
	m_senteByoYomiPanel->setTitle("秒読み");
	m_senteByoYomiPanel->setValue(m_byo_yomi_time1);
	m_senteByoYomiPanel->setUnit("秒");
	m_senteByoYomiPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_senteByoYomiPanel->setPosition(Point(m_winSize.width/2 - m_senteByoYomiPanel->getLayerWidth() - 10, m_senteArea->getContentSize().height + 10));
	this->addChild(m_senteByoYomiPanel, 10);

	// 秒読み（プレイヤー２）
	m_goteByoYomiPanel = ParamDisplayPanel::create();
	m_goteByoYomiPanel->setTitle("秒読み");
	m_goteByoYomiPanel->setValue(m_byo_yomi_time2);
	m_goteByoYomiPanel->setUnit("秒");
	m_goteByoYomiPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_goteByoYomiPanel->setPosition(Point(m_winSize.width/2 + m_goteByoYomiPanel->getLayerWidth() + 10, m_winSize.height - m_goteArea->getContentSize().height - 10));
	this->addChild(m_goteByoYomiPanel, 10);

    RotateBy* rotateByoYomiPanel = RotateBy::create(0.0f, 180);
    m_goteByoYomiPanel->runAction(rotateByoYomiPanel);

	// 考慮回数（プレイヤー１）
    m_senteByoYomiKouryoCntPanel = FractionalParamDisplayPanel::create();
    m_senteByoYomiKouryoCntPanel->setTitle("考慮回数");
    m_senteByoYomiKouryoCntPanel->setValue(m_byo_yomi_kouryo_cnt1, m_byo_yomi_kouryo_cnt_max1);
    m_senteByoYomiKouryoCntPanel->setUnit("回");
    m_senteByoYomiKouryoCntPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    m_senteByoYomiKouryoCntPanel->setPosition(Point(m_winSize.width/2 + 10, m_senteArea->getContentSize().height + 10));
    this->addChild(m_senteByoYomiKouryoCntPanel, 10);

	// 考慮回数（プレイヤー２）
	m_goteByoYomiKouryoCntPanel = FractionalParamDisplayPanel::create();
	m_goteByoYomiKouryoCntPanel->setTitle("考慮回数");
	m_goteByoYomiKouryoCntPanel->setValue(m_byo_yomi_kouryo_cnt2, m_byo_yomi_kouryo_cnt_max2);
	m_goteByoYomiKouryoCntPanel->setUnit("回");
	m_goteByoYomiKouryoCntPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_goteByoYomiKouryoCntPanel->setPosition(Point(m_winSize.width/2 - 10, m_winSize.height - m_goteArea->getContentSize().height - 10));
	this->addChild(m_goteByoYomiKouryoCntPanel, 10);

    RotateBy* rotateByoYomiKouryoCntPanel = RotateBy::create(0.0f, 180);
    m_goteByoYomiKouryoCntPanel->runAction(rotateByoYomiKouryoCntPanel);
}

void GameClockScene::showFisherTimeLabel() {

	// 加算時間（プレイヤー１）
	m_senteFisherAdditionalTimePanel = ParamDisplayPanel::create();
	m_senteFisherAdditionalTimePanel->setTitle("加算時間");
	m_senteFisherAdditionalTimePanel->setValue(m_fisher_additional_time1);
	m_senteFisherAdditionalTimePanel->setUnit("秒");
	m_senteFisherAdditionalTimePanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_senteFisherAdditionalTimePanel->setPosition(Point(m_winSize.width/2-m_senteFisherAdditionalTimePanel->getLayerWidth()/2, m_senteArea->getContentSize().height + 10));
	this->addChild(m_senteFisherAdditionalTimePanel, 10);

	// 加算時間（プレイヤー２）
	m_goteFisherAdditionalTimePanel = ParamDisplayPanel::create();
	m_goteFisherAdditionalTimePanel->setTitle("加算時間");
	m_goteFisherAdditionalTimePanel->setValue(m_fisher_additional_time2);
	m_goteFisherAdditionalTimePanel->setUnit("秒");
	m_goteFisherAdditionalTimePanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_goteFisherAdditionalTimePanel->setPosition(Point(m_winSize.width/2+m_goteFisherAdditionalTimePanel->getLayerWidth()/2, m_winSize.height - m_goteArea->getContentSize().height - 10));
	this->addChild(m_goteFisherAdditionalTimePanel, 10);

    RotateBy* rotateFisherAdditionalTimePanel = RotateBy::create(0.0f, 180);
    m_goteFisherAdditionalTimePanel->runAction(rotateFisherAdditionalTimePanel);
}

void GameClockScene::showCanadaTimeLabel() {

	// 規定時間（プレイヤー１）
	m_senteCanadaKiteiTimePanel = ParamDisplayPanel::create();
	m_senteCanadaKiteiTimePanel->setTitle("規定時間");
	m_senteCanadaKiteiTimePanel->setValue(m_canada_kitei_time1);
	m_senteCanadaKiteiTimePanel->setUnit("分");
	m_senteCanadaKiteiTimePanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_senteCanadaKiteiTimePanel->setPosition(Point(m_winSize.width/2 - m_senteCanadaKiteiTimePanel->getLayerWidth() - 10, m_senteArea->getContentSize().height + 10));
	this->addChild(m_senteCanadaKiteiTimePanel, 10);

	// 規定時間（プレイヤー２）
	m_goteCanadaKiteiTimePanel = ParamDisplayPanel::create();
	m_goteCanadaKiteiTimePanel->setTitle("規定時間");
	m_goteCanadaKiteiTimePanel->setValue(m_canada_kitei_time2);
	m_goteCanadaKiteiTimePanel->setUnit("分");
	m_goteCanadaKiteiTimePanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_goteCanadaKiteiTimePanel->setPosition(Point(m_winSize.width/2 + m_goteCanadaKiteiTimePanel->getLayerWidth() + 10, m_winSize.height - m_goteArea->getContentSize().height - 10));
	this->addChild(m_goteCanadaKiteiTimePanel, 10);

    RotateBy* rotateCanadaKiteiTimePanel = RotateBy::create(0.0f, 180);
    m_goteCanadaKiteiTimePanel->runAction(rotateCanadaKiteiTimePanel);

	// 規定手数（プレイヤー１）
	m_senteCanadaKiteiTekazuPanel = FractionalParamDisplayPanel::create();
	m_senteCanadaKiteiTekazuPanel->setTitle("規定手数");
	m_senteCanadaKiteiTekazuPanel->setValue(m_canada_kitei_tekazu_current1, m_canada_kitei_tekazu1);
	m_senteCanadaKiteiTekazuPanel->setUnit("手");
	m_senteCanadaKiteiTekazuPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_senteCanadaKiteiTekazuPanel->setPosition(Point(m_winSize.width/2 + 10, m_senteArea->getContentSize().height + 10));
	this->addChild(m_senteCanadaKiteiTekazuPanel, 10);

	// 規定手数（プレイヤー２）
	m_goteCanadaKiteiTekazuPanel = FractionalParamDisplayPanel::create();
	m_goteCanadaKiteiTekazuPanel->setTitle("規定手数");
	m_goteCanadaKiteiTekazuPanel->setValue(m_canada_kitei_tekazu_current2, m_canada_kitei_tekazu2);
	m_goteCanadaKiteiTekazuPanel->setUnit("手");
	m_goteCanadaKiteiTekazuPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_goteCanadaKiteiTekazuPanel->setPosition(Point(m_winSize.width/2 - 10, m_winSize.height - m_goteArea->getContentSize().height - 10));
	this->addChild(m_goteCanadaKiteiTekazuPanel, 10);

    RotateBy* rotateCanadaKiteiTekazuPanel = RotateBy::create(0.0f, 180);
    m_goteCanadaKiteiTekazuPanel->runAction(rotateCanadaKiteiTekazuPanel);
}

void GameClockScene::showXianqqiTimeLabel() {

	// 規定手数（プレイヤー１）
	m_senteXianqqiKiteiTekazuPanel = FractionalParamDisplayPanel::create();
	m_senteXianqqiKiteiTekazuPanel->setTitle("規定手数１");
	m_senteXianqqiKiteiTekazuPanel->setValue(m_xianqqi_kitei_tekazu_current1_1, m_xianqqi_kitei_tekazu1_1);
	m_senteXianqqiKiteiTekazuPanel->setUnit("手");
	m_senteXianqqiKiteiTekazuPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_senteXianqqiKiteiTekazuPanel->setPosition(Point(m_winSize.width/2 - m_senteXianqqiKiteiTekazuPanel->getLayerWidth()*5/2 - 20, m_senteArea->getContentSize().height + 10));
	this->addChild(m_senteXianqqiKiteiTekazuPanel, 10);

	// 規定手数（プレイヤー２）
	m_goteXianqqiKiteiTekazuPanel = FractionalParamDisplayPanel::create();
	m_goteXianqqiKiteiTekazuPanel->setTitle("規定手数１");
	m_goteXianqqiKiteiTekazuPanel->setValue(m_xianqqi_kitei_tekazu_current1_2, m_xianqqi_kitei_tekazu1_2);
	m_goteXianqqiKiteiTekazuPanel->setUnit("手");
	m_goteXianqqiKiteiTekazuPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_goteXianqqiKiteiTekazuPanel->setPosition(Point(m_winSize.width/2 + m_goteXianqqiKiteiTekazuPanel->getLayerWidth()*5/2 + 20, m_winSize.height - m_goteArea->getContentSize().height - 10));
	this->addChild(m_goteXianqqiKiteiTekazuPanel, 10);

    RotateBy *rotateXianqqiKiteiTekazuPanelLabel = RotateBy::create(0.0f, 180);
    m_goteXianqqiKiteiTekazuPanel->runAction(rotateXianqqiKiteiTekazuPanelLabel);

    // 持ち時間（プレイヤー１）
	m_senteXianqqiMotiJikanPanel = ParamDisplayPanel::create();
	m_senteXianqqiMotiJikanPanel->setTitle("持ち時間２");
	m_senteXianqqiMotiJikanPanel->setValue(m_xianqqi_moti_jikan2_1);
	m_senteXianqqiMotiJikanPanel->setUnit("分");
	m_senteXianqqiMotiJikanPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_senteXianqqiMotiJikanPanel->setPosition(Point(m_winSize.width/2 - m_senteXianqqiMotiJikanPanel->getLayerWidth()*3/2 - 10, m_senteArea->getContentSize().height + 10));
	this->addChild(m_senteXianqqiMotiJikanPanel, 10);

    // 持ち時間（プレイヤー２）
	m_goteXianqqiMotiJikanPanel = ParamDisplayPanel::create();
	m_goteXianqqiMotiJikanPanel->setTitle("持ち時間２");
	m_goteXianqqiMotiJikanPanel->setValue(m_xianqqi_moti_jikan2_2);
	m_goteXianqqiMotiJikanPanel->setUnit("分");
	m_goteXianqqiMotiJikanPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_goteXianqqiMotiJikanPanel->setPosition(Point(m_winSize.width/2 + m_senteXianqqiMotiJikanPanel->getLayerWidth()*3/2 + 10, m_winSize.height - m_goteArea->getContentSize().height - 10));
	this->addChild(m_goteXianqqiMotiJikanPanel, 10);

    RotateBy *rotateXianqqiMotiJikanPanel = RotateBy::create(0.0f, 180);
    m_goteXianqqiMotiJikanPanel->runAction(rotateXianqqiMotiJikanPanel);

    // 規定手数２（プレイヤー１）
    m_senteXianqqiKiteiTekazu2Panel = FractionalParamDisplayPanel::create();
    m_senteXianqqiKiteiTekazu2Panel->setTitle("規定手数２");
    m_senteXianqqiKiteiTekazu2Panel->setValue(m_xianqqi_kitei_tekazu_current2_1, m_xianqqi_kitei_tekazu2_1);
    m_senteXianqqiKiteiTekazu2Panel->setUnit("手");
    m_senteXianqqiKiteiTekazu2Panel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    m_senteXianqqiKiteiTekazu2Panel->setPosition(Point(m_winSize.width/2 - m_senteXianqqiMotiJikanPanel->getLayerWidth()/2, m_senteArea->getContentSize().height + 10));
	this->addChild(m_senteXianqqiKiteiTekazu2Panel, 10);

    // 規定手数２（プレイヤー２）
    m_goteXianqqiKiteiTekazu2Panel = FractionalParamDisplayPanel::create();
    m_goteXianqqiKiteiTekazu2Panel->setTitle("規定手数２");
    m_goteXianqqiKiteiTekazu2Panel->setValue(m_xianqqi_kitei_tekazu_current2_2, m_xianqqi_kitei_tekazu2_2);
    m_goteXianqqiKiteiTekazu2Panel->setUnit("手");
    m_goteXianqqiKiteiTekazu2Panel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    m_goteXianqqiKiteiTekazu2Panel->setPosition(Point(m_winSize.width/2 + m_senteXianqqiMotiJikanPanel->getLayerWidth()/2, m_winSize.height - m_goteArea->getContentSize().height - 10));
	this->addChild(m_goteXianqqiKiteiTekazu2Panel, 10);

    RotateBy *rotateXianqqiKiteiTekazu2Panel = RotateBy::create(0.0f, 180);
    m_goteXianqqiKiteiTekazu2Panel->runAction(rotateXianqqiKiteiTekazu2Panel);

    // 規定回数（プレイヤー１）
	m_senteXianqqiKiteiCountPanel = FractionalParamDisplayPanel::create();
	m_senteXianqqiKiteiCountPanel->setTitle("規定回数");
	m_senteXianqqiKiteiCountPanel->setValue(m_xianqqi_kitei_cnt_current1, m_xianqqi_kitei_cnt1);
	m_senteXianqqiKiteiCountPanel->setUnit("回");
	m_senteXianqqiKiteiCountPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_senteXianqqiKiteiCountPanel->setPosition(Point(m_winSize.width/2 +  m_senteXianqqiMotiJikanPanel->getLayerWidth()/2 + 10, m_senteArea->getContentSize().height + 10));
	this->addChild(m_senteXianqqiKiteiCountPanel, 10);

    // 規定回数（プレイヤー２）
	m_goteXianqqiKiteiCountPanel = FractionalParamDisplayPanel::create();
	m_goteXianqqiKiteiCountPanel->setTitle("規定回数");
	m_goteXianqqiKiteiCountPanel->setValue(m_xianqqi_kitei_cnt_current2, m_xianqqi_kitei_cnt2);
	m_goteXianqqiKiteiCountPanel->setUnit("回");
	m_goteXianqqiKiteiCountPanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_goteXianqqiKiteiCountPanel->setPosition(Point(m_winSize.width/2 -  m_senteXianqqiMotiJikanPanel->getLayerWidth()/2 - 10, m_winSize.height - m_goteArea->getContentSize().height - 10));
	this->addChild(m_goteXianqqiKiteiCountPanel, 10);

    RotateBy *rotateXianqqiKiteiCountPanel = RotateBy::create(0.0f, 180);
    m_goteXianqqiKiteiCountPanel->runAction(rotateXianqqiKiteiCountPanel);

    // 秒読み（プレイヤー１）
	m_senteXianqqiByoYomiTimePanel = ParamDisplayPanel::create();
	m_senteXianqqiByoYomiTimePanel->setTitle("秒読み");
	m_senteXianqqiByoYomiTimePanel->setValue(m_xianqqi_byo_yomi_time1);
	m_senteXianqqiByoYomiTimePanel->setUnit("秒");
	m_senteXianqqiByoYomiTimePanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_senteXianqqiByoYomiTimePanel->setPosition(Point(m_winSize.width/2 +  m_senteXianqqiMotiJikanPanel->getLayerWidth()*3/2 + 20, m_senteArea->getContentSize().height + 10));
	this->addChild(m_senteXianqqiByoYomiTimePanel, 10);

    // 秒読み（プレイヤー２）
	m_goteXianqqiByoYomiTimePanel = ParamDisplayPanel::create();
	m_goteXianqqiByoYomiTimePanel->setTitle("秒読み");
	m_goteXianqqiByoYomiTimePanel->setValue(m_xianqqi_byo_yomi_time2);
	m_goteXianqqiByoYomiTimePanel->setUnit("秒");
	m_goteXianqqiByoYomiTimePanel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	m_goteXianqqiByoYomiTimePanel->setPosition(Point(m_winSize.width/2 -  m_senteXianqqiMotiJikanPanel->getLayerWidth()*3/2 - 20, m_winSize.height - m_goteArea->getContentSize().height - 10));
	this->addChild(m_goteXianqqiByoYomiTimePanel, 10);

    RotateBy *rotateByoYomiTimePanel = RotateBy::create(0.0f, 180);
    m_goteXianqqiByoYomiTimePanel->runAction(rotateByoYomiTimePanel);
}

void GameClockScene::updateFisherAdditionalTimeLabel(int turn) {
	if(turn == TURN_SENTE) {
		m_second1 = m_second1 + m_fisher_additional_time1;

		if(m_second1 >= 60) {
			m_second1 = m_second1 - 60;
			m_minite1++;
			if(m_minite1 >= 60) {
				m_minite1 = m_minite1 - 60;
				m_hour1++;
			}
		}
		updateClockTime(TURN_SENTE);
	} else {
		m_second2 = m_second2 + m_fisher_additional_time2;

		if(m_second2 >= 60) {
			m_second2 = m_second2 - 60;
			m_minite2++;
			if(m_minite1 >= 60) {
				m_minite2 = m_minite2 - 60;
				m_hour2++;
			}
		}
		updateClockTime(TURN_GOTE);
	}
}

void GameClockScene::updateClockTime(int turn) {

	if(turn == TURN_SENTE) {
        if(m_hour1 <= 0) {
        	m_senteTimeLabel->setSystemFontSize(250);
        	sprintf(m_senteTimeStr, "%02d:%02d", m_minite1, m_second1);
        } else {
        	m_senteTimeLabel->setSystemFontSize(170);
        	sprintf(m_senteTimeStr, "%02d:%02d:%02d", m_hour1, m_minite1, m_second1);
        }

        m_senteTimeLabel->setString(m_senteTimeStr);
	} else {
        if(m_hour2 <= 0) {
        	m_goteTimeLabel->setSystemFontSize(250);
        	sprintf(m_goteTimeStr, "%02d:%02d", m_minite2, m_second2);
        } else {
        	m_goteTimeLabel->setSystemFontSize(170);
        	sprintf(m_goteTimeStr, "%02d:%02d:%02d", m_hour2, m_minite2, m_second2);
        }

        m_goteTimeLabel->setString(m_goteTimeStr);
	}
}

void GameClockScene::endJudge(int turn) {
	if(turn == TURN_SENTE) {
		if(m_rule == RULE_KIREMAKE) {				// 切れ負け
			if(m_second1 == 0 && m_minite1 == 0 && m_hour1 == 0) {
				gameOver(turn);
			}
		} else if(m_rule == RULE_BYOYOMI) {			// 秒読み
			if(m_second1 == 0 && m_minite1 == 0 && m_hour1 == 0) {
				m_byoYomiMode1 = true;
				m_byo_yomi_kouryo_cnt1--;
				if(m_byo_yomi_kouryo_cnt1 == 0 || m_byo_yomi_time1 == 0) {
					// 持ち時間を考慮回数分使いきった場合は終了
					gameOver(turn);
				} else {
					// 持ち時間を追加
					m_second1 = m_byo_yomi_time1;
					m_senteByoYomiKouryoCntPanel->setValue(m_byo_yomi_kouryo_cnt1, m_byo_yomi_kouryo_cnt_max1);
				}
			}
		} else if(m_rule == RULE_FISHER) {			// フィッシャー
			if(m_second1 == 0 && m_minite1 == 0 && m_hour1 == 0) {
				gameOver(turn);
			}
		} else if(m_rule == RULE_CANADA) {			// カナダ式
			if(m_second1 == 0 && m_minite1 == 0 && m_hour1 == 0) {
				if(!m_canadaOverTimeMode1) {		// 超過時間のとき
					m_minite1 = m_canada_kitei_time1;
					m_canadaOverTimeMode1 = true;
				} else {							// 超過時間のとき
					if(m_canada_kitei_tekazu_current1 != 0) {
						// 規定手数に満たない場合は終了
						gameOver(turn);
					} else {
						if(m_canada_kitei_time1 != 0) {		// 規定手数を満たす場合
							// 規定時間を加算
							m_minite1 = m_canada_kitei_time1;
						} else {
							// 規定時間が0のとき終了
							gameOver(turn);
						}
					}
				}
			}
		} else if(m_rule == RULE_XIANQQI) {			// シャンチー国際
			if(m_second1 == 0 && m_minite1 == 0 && m_hour1 == 0) {
				if(m_xianqqi_time_mode1 == XIANQQI_MOCHIJIKAN_MODE1) {			// 持ち時間１のとき
					// 持ち時間モード１のとき持ち時間２を追加して、持ち時間モード２にする
					m_minite1 = m_xianqqi_moti_jikan2_1;
					m_xianqqi_time_mode1 = XIANQQI_MOCHIJIKAN_MODE2;
				} else if(m_xianqqi_time_mode1 == XIANQQI_MOCHIJIKAN_MODE2) {	// 持ち時間２のとき
					if(m_xianqqi_kitei_cnt_current1 != 0 && m_xianqqi_moti_jikan2_1 != 0) {
						// 持ち時間２を規定回数分使いきっていない時、持ち時間２を追加する
						m_minite1 = m_xianqqi_moti_jikan2_1;
						m_xianqqi_kitei_cnt_current1--;
						m_senteXianqqiKiteiCountPanel->setValue(m_xianqqi_kitei_cnt_current1, m_xianqqi_kitei_cnt1);
					} else {
						// 持ち時間２を規定回数分使いきった時、秒読みモードにする
						m_xianqqi_time_mode1 = XIANQQI_BYOYOMI_MODE;
						m_second1 = m_xianqqi_byo_yomi_time1;
					}
				} else if(m_xianqqi_time_mode1 == XIANQQI_MOCHIJIKAN_MODE3) {
					// 規定回数分の持ち時間を使いきっている（持ち時間モード３）のとき、秒読みモードにする
					m_xianqqi_time_mode1 = XIANQQI_BYOYOMI_MODE;
					m_second1 = m_xianqqi_byo_yomi_time1;
				} else if(m_xianqqi_time_mode1 == XIANQQI_BYOYOMI_MODE && m_second1 == 0) {
					gameOver(turn);
				}
			}
		} else {
			if(m_second1 == 0 && m_minite1 == 0 && m_hour1 == 0) {
				gameOver(turn);
			}
		}
	} else {
        if(m_rule == RULE_KIREMAKE) {		 		// 切れ負け
        	if(m_second2 == 0 && m_minite2 == 0 && m_hour2 == 0) {
        		gameOver(turn);
        	}
        } else if(m_rule == RULE_BYOYOMI) {			// 秒読み
        	if(m_second2 == 0 && m_minite2 == 0 && m_hour2 == 0) {
            	m_byoYomiMode2 = true;
				m_byo_yomi_kouryo_cnt2--;
            	if(m_byo_yomi_kouryo_cnt2 == 0 || m_byo_yomi_time2 == 0) {
    				gameOver(turn);
    			} else {
					// 持ち時間を追加
					m_second2 = m_byo_yomi_time2;
					m_goteByoYomiKouryoCntPanel->setValue(m_byo_yomi_kouryo_cnt2, m_byo_yomi_kouryo_cnt_max2);
    			}
        	}
        } else if(m_rule == RULE_FISHER) {			// フィッシャー
        	if(m_second2 == 0 && m_minite2 == 0 && m_hour2 == 0) {
        		gameOver(turn);
        	}
        } else if(m_rule == RULE_CANADA) {			// カナダ式
        	if(m_second2 == 0 && m_minite2 == 0 && m_hour2 == 0) {
        		if(!m_canadaOverTimeMode2) {
            		m_minite2 = m_canada_kitei_time2;
            		m_canadaOverTimeMode2 = true;
        		} else {
					if(m_canada_kitei_tekazu_current2 == 0 &&
							m_canadaOverTimeMode2) {
						// 規定手数に満たない場合は終了
						gameOver(turn);
        			} else {
						if(m_canada_kitei_time2 != 0) {		// 規定手数を満たす場合
							// 規定時間を加算
							m_minite2 = m_canada_kitei_time2;
						} else {
							// 規定時間が0のとき終了
							gameOver(turn);
						}
        			}
        		}
        	}
        } else if(m_rule == RULE_XIANQQI) {			// シャンチー国際
        	if(m_second2 == 0 && m_minite2 == 0 && m_hour2 == 0) {
        		if(m_xianqqi_time_mode2 == XIANQQI_MOCHIJIKAN_MODE1) {
        			// 持ち時間モード１のとき持ち時間２を追加して、持ち時間モード２にする
        			m_minite2 = m_xianqqi_moti_jikan2_2;
        			m_xianqqi_time_mode2 = XIANQQI_MOCHIJIKAN_MODE2;
        		} else if (m_xianqqi_time_mode2 == XIANQQI_MOCHIJIKAN_MODE2) {
        			if(m_xianqqi_kitei_cnt_current2 != 0 && m_xianqqi_moti_jikan2_2 != 0) {
        				// 持ち時間２を規定回数分使いきっていない時、持ち時間２を追加する
        				m_minite2 = m_xianqqi_moti_jikan2_2;
        				m_xianqqi_kitei_cnt_current2--;
        				m_goteXianqqiKiteiCountPanel->setValue(m_xianqqi_kitei_cnt_current2, m_xianqqi_kitei_cnt2);
        			} else {
        				// 持ち時間２を規定回数分使いきった時、秒読みモードにする
            			m_xianqqi_time_mode2 = XIANQQI_BYOYOMI_MODE;
            			m_second2 = m_xianqqi_byo_yomi_time2;
        			}
				} else if(m_xianqqi_time_mode2 == XIANQQI_MOCHIJIKAN_MODE3) {
					// 規定回数分の持ち時間を使いきっている（持ち時間モード３）のとき、秒読みモードにする
					m_xianqqi_time_mode1 = XIANQQI_BYOYOMI_MODE;
					m_second2 = m_xianqqi_byo_yomi_time2;
        		} else if(m_xianqqi_time_mode2 == XIANQQI_BYOYOMI_MODE && m_second2 == 0) {
        			gameOver(turn);
        		}
        	}
        } else {
        	if(m_second2 == 0 && m_minite2 == 0 && m_hour2 == 0) {
        		gameOver(turn);
        	}
        }
	}
}

void GameClockScene::gameOver(int loserTurn) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/gameover.mp3");

	if(loserTurn == TURN_GOTE) {
		MessageBox("先手の勝利", "ゲーム終了");
	} else {
		MessageBox("後手の勝利", "ゲーム終了");
	}

	this->unschedule(schedule_selector(GameClockScene::updateTimer));

	m_listener->setEnabled(false);

    this->removeChild(m_pauseButton, 200);

    MenuItemImage *resetButtonItem = MenuItemImage::create(
												  "res/game_reset_button.png",
												  "res/game_reset_button.png",
												  CC_CALLBACK_1(GameClockScene::resetButtonDidPushed, this));
    resetButtonItem->setPosition( Point(m_winSize.width/2, m_winSize.height/2) );

    Menu *resetButton = Menu::create(resetButtonItem, NULL);
    resetButton->setPosition( Point::ZERO );
    this->addChild(resetButton, 200);
}

