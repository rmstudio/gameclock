#ifndef __GameClockScene_SCENE_H__
#define __GameClockScene_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class SplashScene : public cocos2d::LayerGradient {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(SplashScene);
    
    void nextScene(float delta);

    Size m_winSize;

};


#endif
