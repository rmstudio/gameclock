
#include "BatteryAlertDialogLayer.h"

enum {
    //* priority used by the menu for the event handler
    kCCMenuHandlerPriority = -128,
};

// モーダルレイヤの優先順位
#define kModalLayerPriority kCCMenuHandlerPriority-1
//#define kModalLayerPriority -130
// モーダルレイア上のMenuの優先順位
#define kModalLayerMenuPriority kCCMenuHandlerPriority-2
//#define kModalLayerMenuPriority -140

using namespace cocos2d;

bool BatteryAlertDialogLayer::init()
{

    if ( !LayerGradient::initWithColor( Color4B(0, 0, 0, 0), Color4B(0, 0, 0, 0) ) )
    {
        return false;
    }
    
    log("init!");
    
    Size winSize = Director::getInstance()->getVisibleSize();

    log("DialogLayer %d", this->getTag());

    m_backGroundLayer = Sprite::create("res/battery_alert_dialog_base.png");
    m_backGroundLayer->setPosition(Vec2(0, 0));
    m_backGroundLayer->setAnchorPoint(Vec2(0.0f, 0.0f));
    this->addChild( m_backGroundLayer , 110);

    m_headlineLayer = Sprite::create("res/battery_alert_dialog_title.png");
    m_headlineLayer->setPosition(Vec2(0, m_backGroundLayer->getContentSize().height -
    		m_headlineLayer->getContentSize().height));
    m_headlineLayer->setAnchorPoint(Vec2(0.0f, 0.0f));
    this->addChild( m_headlineLayer , 160);

    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    dispatcher->setPriority(listener, kModalLayerPriority);

    listener->onTouchBegan = CC_CALLBACK_2(BatteryAlertDialogLayer::onTouchBegan, this);

    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}

void BatteryAlertDialogLayer::setTitle(const char* title, __String *dialogKind)
{
	Size winSize = Director::getInstance()->getVisibleSize();

    // タイトル
	Label *titleLabel = Label::createWithSystemFont(title, "HiraKakuProN-W6", 48);
    titleLabel->setColor(Color3B(255, 255, 255));
    titleLabel->setPosition(Vec2(m_headlineLayer->getContentSize().width/2,
    							m_headlineLayer->getContentSize().height/2));

    m_headlineLayer->addChild(titleLabel, 170);
}

bool BatteryAlertDialogLayer::onTouchBegan(Touch* touch, Event* event) {
    // can not touch on back layers
    log("touch!");
    return true;
}

void BatteryAlertDialogLayer::menuCloseCallback(Ref* pSender)
{
    log("menu pushed!");
    this->removeFromParentAndCleanup(true);
}
