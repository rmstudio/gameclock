
#ifndef PARAMDISPLAYPANEL_H_
#define PARAMDISPLAYPANEL_H_

#include "cocos2d.h"

enum SIZE {
    SIZE_NORMAL   = 0,
    SIZE_SMALL  = 1,
};

namespace cocos2d {

class ParamDisplayPanel : public cocos2d::Layer {
public:
	virtual bool init();
	CREATE_FUNC(ParamDisplayPanel);

    Sprite *m_backGroundLayer;
    Sprite *m_headlineLayer;
    Label *m_titleLabel;
    Label *m_timeLabel;
    Label *m_unitLabel;

    CC_SYNTHESIZE(float, m_layerWidth, LayerWidth);

    int m_time;

	void setTitle(const std::string& title);
	void setValue(int value);
	void setUnit(const std::string& unit);
	void setSize(int size);

private:
	void setTitlePosition();
};

}
#endif /* PARAMDISPLAYPANEL_H_ */
