/*
 * ByoYomiTimeSetting.h
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#ifndef _BYOYOMI_TIMESETTING_H_
#define _BYOYOMI_TIMESETTING_H_

#include "cocos2d.h"
#include "DialogLayer.h"
#include "TimeSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class ByoYomiTimeSettingScene : public TimeSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(ByoYomiTimeSettingScene);

    Size m_winSize;

    // 設定値
    int m_byo_yomi_time1;								// 秒読み時間（プレイヤー１）
    int m_byo_yomi_time2;								// 秒読み時間（プレイヤー２）
    int m_byoYomiTimeMaxLimit = 60;						// 秒読み時間最大値
    int m_byoYomiTimeMinLimit = 1;						// 秒読み時間最小値

    Sprite *m_byoYomiTimeSettingButtonItem;

    MenuItemImage *m_byoYomiMinusButtonItem;
    Menu *m_byoYomiMinusButton;

    MenuItemImage *m_byoYomiPlusButtonItem;
    Menu *m_byoYomiPlusButton;

    Label *m_byoYomiTimeDispLabel;
    Label *m_byoYomiTimeUnitLabel;

    // button action
    void backButtonDidPushed(Ref* pSender);
    void byoYomiTimePlusButtonDidPushed(Ref* pSender);
    void byoYomiTimeMinusButtonDidPushed(Ref* pSender);

private:
    int byoYomiTimeIncrement(int player);
    int byoYomiTimeDecrement(int player);
};


#endif
