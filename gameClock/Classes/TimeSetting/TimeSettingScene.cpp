#define COCOS2D_DEBUG 1

#include "Setting/KiremakeSettingScene.h"
#include "Setting/ByoYomiSettingScene.h"
#include "Setting/FisherSettingScene.h"
#include "Setting/CanadaSettingScene.h"
#include "Setting/XianqqiSettingScene.h"
#include "TimeSetting/TimeSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

enum BUTTON {
    BUTTON_TIME1   = 0,
    BUTTON_TIME2  = 1,
    BUTTON_COLOR1  = 2,
    BUTTON_COLOR2  = 3,
    BUTTON_RULE  = 3,
};

enum RULE {
    RULE_KIREMAKE   = 0,
    RULE_BYOYOMI  = 1,
    RULE_FISHER  = 2,
    RULE_CANADA  = 3,
    RULE_XIANQQI  = 4,
};

enum PLAYER {
	PLAYER_SENTE = 0,
	PLAYER_GOTE = 1,
};

Scene* TimeSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    TimeSettingScene *layer = TimeSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool TimeSettingScene::init()
{

	loadCommonSetting();

    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0f);
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/change_scene.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/select.mp3");

    Label *settingTitle = Label::createWithSystemFont("時間設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    settingTitle->setColor(m_titleFontColor);
    settingTitle->setAnchorPoint(Vec2(0.5f, 0.5f));
    m_settingTitleLayer->addChild(settingTitle, 1);

    // 戻るボタン
    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(TimeSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, m_settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width + 200,
    							m_backButtonItem->getContentSize().height));
    m_settingTitleLayer->addChild(m_backButton, 100);

    // 時間ラベル
	Label *timeLabel1 = Label::createWithSystemFont("時間", "HiraKakuProN-W6", 55);
	timeLabel1->setPosition(Vec2(50, m_settingTitleLayer->getPositionY()-80));
	timeLabel1->setColor(m_subjectFontColor);
	timeLabel1->setAnchorPoint(Vec2(0.0f, 0.5f));
	this->addChild(timeLabel1, 150);

    // 時間
    m_hourSettingButtonItem = Sprite::create("res/setting_menu_small.png");
    m_hourSettingButtonItem->setPosition( Vec2(40, timeLabel1->getPositionY()-90) );
    m_hourSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	// 時間表示ラベル
	int hour;
    if(m_player == 0) {
    	hour = m_hour1;
    } else {
    	hour = m_hour2;
    }
	char hourStr[5];
	sprintf(hourStr, "%d", hour);

	m_hourDispLabel = Label::createWithSystemFont(hourStr, "HiraKakuProN-W6", 48);
	m_hourDispLabel->setPosition(Vec2(m_hourSettingButtonItem->getPositionX(),
										m_hourSettingButtonItem->getContentSize().height/2));
	m_hourDispLabel->setColor(m_buttonFontColor);
	m_hourDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_hourSettingButtonItem->addChild(m_hourDispLabel, 150);

	// 時間単位
	m_hourUnitLabel = Label::createWithSystemFont("時間", "HiraKakuProN-W6", 48);
	m_hourUnitLabel->setPosition(Vec2(m_hourSettingButtonItem->getContentSize().width - 50,
			m_hourSettingButtonItem->getContentSize().height/2));
	m_hourUnitLabel->setColor(m_buttonFontColor);
	m_hourUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_hourSettingButtonItem->addChild(m_hourUnitLabel, 150);

	this->addChild(m_hourSettingButtonItem, 1);

	// 時間マイナス
	m_hourMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(TimeSettingScene::hourMinusButtonDidPushed, this));

	m_hourMinusButton = Menu::create(m_hourMinusButtonItem, NULL);
	m_hourMinusButton->setPosition( Vec2(m_winSize.width-200, timeLabel1->getPositionY()-90) );

    this->addChild( m_hourMinusButton , 200);

    // 時間プラス
	m_hourPlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(TimeSettingScene::hourPlusButtonDidPushed, this));

	m_hourPlusButton = Menu::create(m_hourPlusButtonItem, NULL);
	m_hourPlusButton->setPosition( Vec2(m_winSize.width-100, timeLabel1->getPositionY()-90) );

    this->addChild( m_hourPlusButton , 200);

    // 分ラベル
	Label *miniteLabel = Label::createWithSystemFont("分", "HiraKakuProN-W6", 55);
	miniteLabel->setPosition(Vec2(50, m_hourSettingButtonItem->getPositionY()-110));
	miniteLabel->setColor(m_subjectFontColor);
	miniteLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	this->addChild(miniteLabel, 150);

    // 分
    m_miniteSettingButtonItem = Sprite::create("res/setting_menu_small.png");
    m_miniteSettingButtonItem->setPosition( Vec2(40, miniteLabel->getPositionY()-90) );
    m_miniteSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	this->addChild(m_miniteSettingButtonItem, 1);

	// 分表示ラベル
	int minite;
    if(m_player == 0) {
    	minite = m_minite1;
    } else {
    	minite = m_minite2;
    }
	char miniteStr[5];
	sprintf(miniteStr, "%d", minite);

	m_miniteDispLabel = Label::createWithSystemFont(miniteStr, "HiraKakuProN-W6", 48);
	m_miniteDispLabel->setPosition(Vec2(m_miniteSettingButtonItem->getPositionX(),
			m_miniteSettingButtonItem->getContentSize().height/2));
	m_miniteDispLabel->setColor(m_buttonFontColor);
	m_miniteDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_miniteSettingButtonItem->addChild(m_miniteDispLabel, 150);

	// 分単位
	m_miniteUnitLabel = Label::createWithSystemFont("分", "HiraKakuProN-W6", 48);
	m_miniteUnitLabel->setPosition(Vec2(m_miniteSettingButtonItem->getContentSize().width - 50,
			m_miniteSettingButtonItem->getContentSize().height/2));
	m_miniteUnitLabel->setColor(m_buttonFontColor);
	m_miniteUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_miniteSettingButtonItem->addChild(m_miniteUnitLabel, 150);

	this->addChild(m_miniteSettingButtonItem, 1);

	// 分マイナス
	m_miniteMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(TimeSettingScene::miniteMinusButtonDidPushed, this));

	m_miniteMinusButton = Menu::create(m_miniteMinusButtonItem, NULL);
	m_miniteMinusButton->setPosition( Vec2(m_winSize.width-200, m_hourSettingButtonItem->getPositionY()-200) );

    this->addChild( m_miniteMinusButton , 200);

    // 分プラス
    m_minitePlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(TimeSettingScene::minitePlusButtonDidPushed, this));

    m_minitePlusButton = Menu::create(m_minitePlusButtonItem, NULL);
    m_minitePlusButton->setPosition( Vec2(m_winSize.width-100, m_hourSettingButtonItem->getPositionY()-200) );

    this->addChild( m_minitePlusButton , 200);

    return true;
}

void TimeSettingScene::loadCommonSetting()
{
	m_backgroundColorStart = Color4B(237, 238, 217, 255);
	m_backgroundColorEnd = Color4B(237, 238, 217, 255);

	LayerGradient::initWithColor( m_backgroundColorStart, m_backgroundColorEnd );

    m_titleFontColor = Color3B(255, 255, 255);
    m_subjectFontColor = Color3B(98, 98, 98);
    m_buttonFontColor = Color3B(98, 98, 98);

    m_winSize = Director::getInstance()->getWinSize();

    m_player = UserDefault::getInstance()->getIntegerForKey("player" , 0);

    m_rule = UserDefault::getInstance()->getIntegerForKey("rule" , 0);

    // 持ち時間（時間）
    m_hour1 = UserDefault::getInstance()->getIntegerForKey("hour1" , 0);
    m_hour2 = UserDefault::getInstance()->getIntegerForKey("hour2", 0);

    // 持ち時間（分）
    m_minite1 = UserDefault::getInstance()->getIntegerForKey("minite1");
    m_minite2 = UserDefault::getInstance()->getIntegerForKey("minite2");

    m_settingTitleLayer = LayerGradient::create(Color4B(196, 229, 103, 255), Color4B(64, 132, 31, 255));
    m_settingTitleLayer->setContentSize( Size(m_winSize.width, 120) );
    m_settingTitleLayer->setPosition(Vec2(0, m_winSize.height-120));

    this->addChild( m_settingTitleLayer , 160);
}

// button action
void TimeSettingScene::backButtonDidPushed(Ref* pSender)
{
    UserDefault::getInstance()->setIntegerForKey("hour1", m_hour1);
    UserDefault::getInstance()->setIntegerForKey("hour2", m_hour2);

    UserDefault::getInstance()->setIntegerForKey("minite1", m_minite1);
    UserDefault::getInstance()->setIntegerForKey("minite2", m_minite2);

    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    Scene *nextScene;
    if(m_rule == RULE_KIREMAKE) {
		nextScene = TransitionSlideInL::create(0.1f, KiremakeSettingScene::scene());
    } else if(m_rule == RULE_BYOYOMI) {
		nextScene = TransitionSlideInL::create(0.1f, ByoYomiSettingScene::scene());
    } else if(m_rule == RULE_FISHER) {
		nextScene = TransitionSlideInL::create(0.1f, FisherSettingScene::scene());
    } else if(m_rule == RULE_CANADA) {
		nextScene = TransitionSlideInL::create(0.1f, CanadaSettingScene::scene());
    } else if(m_rule == RULE_XIANQQI) {
		nextScene = TransitionSlideInL::create(0.1f, XianqqiSettingScene::scene());
    }
    Director::getInstance()->replaceScene(nextScene);
}

void TimeSettingScene::hourPlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char hourStr[5];
	sprintf(hourStr, "%d", hourIncrement(m_player));
	m_hourDispLabel->setString(hourStr);
}

void TimeSettingScene::hourMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char hourStr[5];
	sprintf(hourStr, "%d", hourDecrement(m_player));
	m_hourDispLabel->setString(hourStr);
}

void TimeSettingScene::minitePlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char miniteStr[5];
	sprintf(miniteStr, "%d", miniteIncrement(m_player));
	m_miniteDispLabel->setString(miniteStr);
}

void TimeSettingScene::miniteMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char miniteStr[5];
	sprintf(miniteStr, "%d", miniteDecrement(m_player));
	m_miniteDispLabel->setString(miniteStr);
}

int TimeSettingScene::hourIncrement(int player) {
	int hour;
    if(player == 0) {
    	if(m_hour1 < m_hourMaxLimit) {
    		m_hour1++;
    		hour = m_hour1;
    	} else {
    		m_hour1 = m_hourMinLimit;
    		hour = m_hour1;
    	}
    } else {
    	if(m_hour2 < m_hourMaxLimit) {
    		m_hour2++;
    		hour = m_hour2;
    	} else {
    		m_hour2 = m_hourMinLimit;
    		hour = m_hour2;
    	}
    }
    return hour;
}

int TimeSettingScene::hourDecrement(int player) {
	int hour;
    if(player == 0) {
    	if(m_hour1 > m_hourMinLimit) {
    		m_hour1--;
    		hour = m_hour1;
    	} else {
    		m_hour1 = m_hourMaxLimit;
    		hour = m_hour1;
    	}
    } else {
    	if(m_hour2 > m_hourMinLimit) {
    		m_hour2--;
    		hour = m_hour2;
    	} else {
    		m_hour2 = m_hourMaxLimit;
    		hour = m_hour2;
    	}
    }
    return hour;
}

int TimeSettingScene::miniteIncrement(int player) {
	int minite;
    if(player == 0) {
    	if(m_minite1 < m_miniteMaxLimit) {
    		m_minite1++;
    		minite = m_minite1;
    	} else {
    		m_minite1 = m_miniteMinLimit;
    		minite = m_minite1;
    	}
    } else {
    	if(m_minite2 < m_miniteMaxLimit) {
    		m_minite2++;
    		minite = m_minite2;
    	} else {
    		m_minite2 = m_miniteMinLimit;
    		minite = m_minite2;
    	}
    }
    return minite;
}

int TimeSettingScene::miniteDecrement(int player) {
	int minite;
    if(player == 0) {
    	if(m_minite1 > m_miniteMinLimit) {
    		m_minite1--;
    		minite = m_minite1;
    	} else {
    		m_minite1 = m_miniteMaxLimit;
    		minite = m_minite1;
    	}
    } else {
    	if(m_minite2 > m_miniteMinLimit) {
    		m_minite2--;
    		minite = m_minite2;
    	} else {
    		m_minite2 = m_miniteMaxLimit;
    		minite = m_minite2;
    	}
    }
    return minite;
}
