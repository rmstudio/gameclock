/*
 * ByoYomiTimeSetting.h
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#ifndef _BYOYOMIKOURYOCNTSETTING_H_
#define _BYOYOMIKOURYOCNTSETTING_H_

#include "cocos2d.h"
#include "DialogLayer.h"
#include "TimeSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class ByoYomiKouryoCntSettingScene : public TimeSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(ByoYomiKouryoCntSettingScene);

    Size m_winSize;

    // 設定値
    int m_byo_yomi_kouryo_cnt1;							// 秒読み考慮回数（プレイヤー１）
    int m_byo_yomi_kouryo_cnt2;							// 秒読み考慮回数（プレイヤー２）
    int m_byoYomiKouryoCntMaxLimit = 99;				// 秒読み考慮回数最大値
    int m_byoYomiKouryoCntMinLimit = 1;					// 秒読み考慮回数最小値

    Sprite *m_byoYomiKouryoCntSettingButtonItem;

    Menu *m_byoYomiKouryoCntSettingButton;

    Label *m_byoYomiKouryoCntDispLabel;
    Label *m_byoYomiKouryoCntUnitLabel;

    MenuItemImage *m_byoYomiKouryoCntMinusButtonItem;	// マイナスボタン
    Menu *m_byoYomiKouryoCntMinusButton;

    MenuItemImage *m_byoYomiKouryoCntPlusButtonItem;	// プラスボタン
    Menu *m_byoYomiKouryoCntPlusButton;

    MenuItemImage *m_backButtonItem;
    Menu *m_backButton;

    // button action
    void backButtonDidPushed(Ref* pSender);
    void byoYomiKouryoCntPlusButtonDidPushed(Ref* pSender);
    void byoYomiKouryoCntMinusButtonDidPushed(Ref* pSender);

    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

private:
	void backSettingScene();
    int byoYomiKouryoCntIncrement(int player);
    int byoYomiKouryoCntDecrement(int player);
};

#endif
