/*
 * ByoYomiTimeSetteing.cpp
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#define COCOS2D_DEBUG 1

#include "Setting/FisherSettingScene.h"
#include "TimeSetting/FisherAddTimeSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

Scene* FisherAddTimeSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();

    // 'layer' is an autorelease object
    FisherAddTimeSettingScene *layer = FisherAddTimeSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance


bool FisherAddTimeSettingScene::init()
{
	loadCommonSetting();

    m_winSize = Director::getInstance()->getWinSize();

    // フィッシャー加算時間
    m_fisher_additional_time1 = UserDefault::getInstance()->getIntegerForKey("fisher_additional_time1", 10);
    m_fisher_additional_time2 = UserDefault::getInstance()->getIntegerForKey("fisher_additional_time2", 10);

    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(FisherAddTimeSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, m_settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width + 200,
    							m_backButtonItem->getContentSize().height));
    m_settingTitleLayer->addChild(m_backButton, 100);

    this->addChild( m_settingTitleLayer , 160);

    Label *settingTitle = Label::createWithSystemFont("加算時間設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    m_settingTitleLayer->addChild(settingTitle, 1);

	// 加算時間表示ラベル
    m_fisherAddTimeSettingButtonItem = Sprite::create("res/setting_menu_small.png");
    m_fisherAddTimeSettingButtonItem->setPosition( Vec2(40, m_winSize.height-250) );
    m_fisherAddTimeSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	Label *fisherAddTimeLabel = Label::createWithSystemFont("加算時間", "HiraKakuProN-W6", 48);
	fisherAddTimeLabel->setPosition(Vec2(50, m_settingTitleLayer->getPositionY()-80));
	fisherAddTimeLabel->setColor(Color3B(68, 68, 68));
	fisherAddTimeLabel->setAnchorPoint(Vec2(0.0f, 0.5f));

	this->addChild(fisherAddTimeLabel, 1);

	// 加算時間表示パネル
	m_fisherAddTimeSettingButtonItem = Sprite::create("res/setting_menu_small.png");
	m_fisherAddTimeSettingButtonItem->setPosition( Vec2(40, fisherAddTimeLabel->getPositionY()-90) );
	m_fisherAddTimeSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	this->addChild(m_fisherAddTimeSettingButtonItem, 1);

	// 加算時間
	int fisher_add_time;
    if(m_player == 0) {
    	fisher_add_time = m_fisher_additional_time1;
    } else {
    	fisher_add_time = m_fisher_additional_time2;
    }
	char fisherAddTimeStr[5];
	sprintf(fisherAddTimeStr, "%d", fisher_add_time);

	m_fisherAddTimeDispLabel = Label::createWithSystemFont(fisherAddTimeStr, "HiraKakuProN-W6", 48);
	m_fisherAddTimeDispLabel->setPosition(Vec2(m_fisherAddTimeSettingButtonItem->getPositionX(),
			m_fisherAddTimeSettingButtonItem->getContentSize().height/2));
	m_fisherAddTimeDispLabel->setColor(m_buttonFontColor);
	m_fisherAddTimeDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_fisherAddTimeSettingButtonItem->addChild(m_fisherAddTimeDispLabel, 150);

	m_fisherAddTimeUnitLabel = Label::createWithSystemFont("秒", "HiraKakuProN-W6", 48);
	m_fisherAddTimeUnitLabel->setPosition(Vec2(m_fisherAddTimeSettingButtonItem->getContentSize().width - 50,
			m_fisherAddTimeSettingButtonItem->getContentSize().height/2));
	m_fisherAddTimeUnitLabel->setColor(m_buttonFontColor);
	m_fisherAddTimeUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_fisherAddTimeSettingButtonItem->addChild(m_fisherAddTimeUnitLabel, 150);

	this->addChild(m_fisherAddTimeSettingButtonItem, 1);

	// 加算時間マイナス
	m_fisherAddTimeMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(FisherAddTimeSettingScene::fisherAddTimeMinusButtonDidPushed, this));

	m_fisherAddTimeMinusButton = Menu::create(m_fisherAddTimeMinusButtonItem, NULL);
	m_fisherAddTimeMinusButton->setPosition( Vec2(m_winSize.width-200, m_fisherAddTimeSettingButtonItem->getPositionY()) );

    this->addChild( m_fisherAddTimeMinusButton , 200);

    // 加算時間プラス
    m_fisherAddTimePlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(FisherAddTimeSettingScene::fisherAddTimePlusButtonDidPushed, this));

    m_fisherAddTimePlusButton = Menu::create(m_fisherAddTimePlusButtonItem, NULL);
    m_fisherAddTimePlusButton->setPosition( Vec2(m_winSize.width-100, m_fisherAddTimeSettingButtonItem->getPositionY()) );

    this->addChild( m_fisherAddTimePlusButton , 200);

    return true;
}

// button action
void FisherAddTimeSettingScene::backButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    UserDefault::getInstance()->setIntegerForKey("fisher_additional_time1", m_fisher_additional_time1);
    UserDefault::getInstance()->setIntegerForKey("fisher_additional_time2", m_fisher_additional_time2);

    Scene *nextScene = CCTransitionSlideInL::create(0.1f, FisherSettingScene::scene());
    Director::getInstance()->replaceScene(nextScene);
}

void FisherAddTimeSettingScene::fisherAddTimePlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char fisherAddTimeStr[5];
	sprintf(fisherAddTimeStr, "%d", fisherAddTimeIncrement(m_player));
	m_fisherAddTimeDispLabel->setString(fisherAddTimeStr);
}

void FisherAddTimeSettingScene::fisherAddTimeMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char fisherAddTimeStr[5];
	sprintf(fisherAddTimeStr, "%d", fisherAddTimeDecrement(m_player));
	m_fisherAddTimeDispLabel->setString(fisherAddTimeStr);
}

int FisherAddTimeSettingScene::fisherAddTimeIncrement(int player) {
	int fisher_additional_time;
    if(player == 0) {
    	if(m_fisher_additional_time1 < m_fisherAddTimeMaxLimit) {
    		m_fisher_additional_time1++;
    		fisher_additional_time = m_fisher_additional_time1;
    	} else {
    		m_fisher_additional_time1 = m_fisherAddTimeMinLimit;
    		fisher_additional_time = m_fisher_additional_time1;
    	}
    } else {
    	if(m_fisher_additional_time2 < m_fisherAddTimeMaxLimit) {
    		m_fisher_additional_time2++;
    		fisher_additional_time = m_fisher_additional_time2;
    	} else {
    		m_fisher_additional_time2 = m_fisherAddTimeMinLimit;
    		fisher_additional_time = m_fisher_additional_time2;
    	}
    }
    return fisher_additional_time;
}

int FisherAddTimeSettingScene::fisherAddTimeDecrement(int player) {
	int fisher_additional_time;
    if(player == 0) {
    	if(m_fisher_additional_time1 > m_fisherAddTimeMinLimit) {
    		m_fisher_additional_time1--;
    		fisher_additional_time = m_fisher_additional_time1;
    	} else {
    		m_fisher_additional_time1 = m_fisherAddTimeMaxLimit;
    		fisher_additional_time = m_fisher_additional_time1;
    	}
    } else {
    	if(m_fisher_additional_time2 > m_fisherAddTimeMinLimit) {
    		m_fisher_additional_time2--;
    		fisher_additional_time = m_fisher_additional_time2;
    	} else {
    		m_fisher_additional_time2 = m_fisherAddTimeMaxLimit;
    		fisher_additional_time = m_fisher_additional_time2;
    	}
    }
    return fisher_additional_time;
}
