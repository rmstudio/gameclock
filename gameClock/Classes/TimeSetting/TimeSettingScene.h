#ifndef _TIMESETTING_H_
#define _TIMESETTING_H_

#include "cocos2d.h"
#include "DialogLayer.h"
#include "extensions/cocos-ext.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class TimeSettingScene : public cocos2d::LayerGradient {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(TimeSettingScene);
    
    Size m_winSize;
    
    // 設定値
    int m_player;										// プレイヤー
    int m_rule;											// ルール
    int m_hour1;										// 持ち時間（プレイヤー１）
    int m_hour2;										// 持ち時間（プレイヤー２）
    int m_minite1;										// 持ち時間（プレイヤー１）
    int m_minite2;										// 持ち時間（プレイヤー２）
    int m_hourMaxLimit = 99;							// 時間最大値
    int m_hourMinLimit = 0;								// 時間最小値
    int m_miniteMaxLimit = 59;							// 分最大値
    int m_miniteMinLimit = 0;							// 分最小値

    // 設定画面背景色
    Color4B m_backgroundColorStart;
    Color4B m_backgroundColorEnd;

    // フォントカラー
    Color3B m_titleFontColor;
    Color3B m_subjectFontColor;
    Color3B m_buttonFontColor;

    // レイヤー
    LayerGradient *m_settingTitleLayer;

    Sprite *m_hourSettingButtonItem;
    Sprite *m_miniteSettingButtonItem;

    MenuItemImage *m_settingBtnItem;

    Menu *m_miniteSettingButton;
    Menu *m_settingButton;

    Label *m_hourLabel;
    Label *m_miniteLabel;

    Label *m_hourDispLabel;
    Label *m_miniteDispLabel;

    Label *m_hourUnitLabel;
    Label *m_miniteUnitLabel;

    MenuItemImage *m_hourPlusButtonItem;
    MenuItemImage *m_hourMinusButtonItem;
    MenuItemImage *m_minitePlusButtonItem;
    MenuItemImage *m_miniteMinusButtonItem;

    Menu *m_hourPlusButton;
    Menu *m_hourMinusButton;
    Menu *m_minitePlusButton;
    Menu *m_miniteMinusButton;

    MenuItemImage *m_backButtonItem;
    Menu *m_backButton;

    void loadCommonSetting();

    void backButtonDidPushed(Ref* pSender);

    void hourPlusButtonDidPushed(Ref* pSender);
    void hourMinusButtonDidPushed(Ref* pSender);
    void minitePlusButtonDidPushed(Ref* pSender);
    void miniteMinusButtonDidPushed(Ref* pSender);

private:
    int hourIncrement(int player);
    int hourDecrement(int player);
    int miniteIncrement(int player);
    int miniteDecrement(int player);
};

#endif

