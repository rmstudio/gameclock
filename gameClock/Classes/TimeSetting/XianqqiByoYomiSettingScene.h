/*
 * ByoYomiTimeSetting.h
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#ifndef _XIANQQI_BYO_YOMI_SETTING_H_
#define _XIANQQI_BYO_YOMI_SETTING_H_

#include "cocos2d.h"
#include "DialogLayer.h"
#include "TimeSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class XianqqiByoYomiSettingScene : public TimeSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(XianqqiByoYomiSettingScene);

    Size m_winSize;

    // 設定値
    int m_xianqqi_byo_yomi_time1;							// 秒読み（プレイヤー１）
    int m_xianqqi_byo_yomi_time2;							// 秒読み（プレイヤー２）
    int m_xianqqiByoYomiTimeMaxLimit = 60;					// 秒読み最大値
    int m_xianqqiByoYomiTimeMinLimit = 1;					// 秒読み最小値

    Sprite *m_xianqqiByoYomiSettingButtonItem;
    Menu *m_xianqqiByoYomiSettingButton;

    MenuItemImage *m_xianqqiByoYomiTimeMinusButtonItem;
    Menu *m_xianqqiByoYomiTimeMinusButton;

    MenuItemImage *m_xianqqiByoYomiTimePlusButtonItem;
    Menu *m_xianqqiByoYomiTimePlusButton;

    Label *m_xianqqiByoYomiDispLabel;
    Label *m_xianqqiByoYomiUnitLabel;

    MenuItemImage *m_backButtonItem;
    Menu *m_backButton;

    // button action
    void backButtonDidPushed(Ref* pSender);
    void xianqqiByoYomiTimePlusButtonDidPushed(Ref* pSender);
    void xianqqiByoYomiTimeMinusButtonDidPushed(Ref* pSender);

private:
    int xianqqiByoYomiTimeIncrement(int player);
    int xianqqiByoYomiTimeDecrement(int player);
};


#endif
