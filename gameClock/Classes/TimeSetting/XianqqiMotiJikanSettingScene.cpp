/*
 * ByoYomiTimeSetteing.cpp
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#define COCOS2D_DEBUG 1

#include "Setting/XianqqiSettingScene.h"
#include "TimeSetting/XianqqiMotiJikanSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

Scene* XianqqiMotiJikanSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();

    // 'layer' is an autorelease object
    XianqqiMotiJikanSettingScene *layer = XianqqiMotiJikanSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance


bool XianqqiMotiJikanSettingScene::init()
{

	loadCommonSetting();

	m_winSize = Director::getInstance()->getWinSize();

    // 持ち時間
    m_xianqqi_moti_jikan2_1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_moti_jikan2_1", 15);
    m_xianqqi_moti_jikan2_2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_moti_jikan2_2", 15);

    Label *settingTitle = Label::createWithSystemFont("持ち時間設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    m_settingTitleLayer->addChild(settingTitle, 1);

    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(XianqqiMotiJikanSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, m_settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width + 200,
    							m_backButtonItem->getContentSize().height));
    m_settingTitleLayer->addChild(m_backButton, 100);

    this->addChild( m_settingTitleLayer , 160);

	Label *xianqqiMotiJikanLabel = Label::createWithSystemFont("持ち時間", "HiraKakuProN-W6", 55);
	xianqqiMotiJikanLabel->setPosition(Vec2(50, m_settingTitleLayer->getPositionY()-80));
	xianqqiMotiJikanLabel->setColor(Color3B(68, 68, 68));
	xianqqiMotiJikanLabel->setAnchorPoint(Vec2(0.0f, 0.5f));

	this->addChild(xianqqiMotiJikanLabel, 1);

	// 持ち時間パネル
    m_xianqqiMotiJikanSettingButtonItem = Sprite::create("res/setting_menu_small.png");
    m_xianqqiMotiJikanSettingButtonItem->setPosition( Vec2(40, xianqqiMotiJikanLabel->getPositionY()-90) );
    m_xianqqiMotiJikanSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	// 持ち時間
	int xianqqi_moti_jikan;
    if(m_player == 0) {
    	xianqqi_moti_jikan = m_xianqqi_moti_jikan2_1;
    } else {
    	xianqqi_moti_jikan = m_xianqqi_moti_jikan2_2;
    }
	char xianqqiMotiJikanStr[5];
	sprintf(xianqqiMotiJikanStr, "%d", xianqqi_moti_jikan);

	m_xianqqiMotiJikanDispLabel = Label::createWithSystemFont(xianqqiMotiJikanStr, "HiraKakuProN-W6", 48);
	m_xianqqiMotiJikanDispLabel->setPosition(Vec2(m_xianqqiMotiJikanSettingButtonItem->getPositionX(),
			m_xianqqiMotiJikanSettingButtonItem->getContentSize().height/2));
	m_xianqqiMotiJikanDispLabel->setColor(m_buttonFontColor);
	m_xianqqiMotiJikanDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_xianqqiMotiJikanSettingButtonItem->addChild(m_xianqqiMotiJikanDispLabel, 150);

	m_xianqqiMotiJikanUnitLabel = Label::createWithSystemFont("分", "HiraKakuProN-W6", 48);
	m_xianqqiMotiJikanUnitLabel->setPosition(Vec2(m_xianqqiMotiJikanSettingButtonItem->getContentSize().width - 50,
			m_xianqqiMotiJikanSettingButtonItem->getContentSize().height/2));
	m_xianqqiMotiJikanUnitLabel->setColor(m_buttonFontColor);
	m_xianqqiMotiJikanUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_xianqqiMotiJikanSettingButtonItem->addChild(m_xianqqiMotiJikanUnitLabel, 150);

	this->addChild(m_xianqqiMotiJikanSettingButtonItem, 1);

	// 持ち時間マイナス
	m_xianqqiMotiJikanMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(XianqqiMotiJikanSettingScene::xianqqiMotiJikanMinusButtonDidPushed, this));

	m_xianqqiMotiJikanMinusButton = Menu::create(m_xianqqiMotiJikanMinusButtonItem, NULL);
	m_xianqqiMotiJikanMinusButton->setPosition( Vec2(m_winSize.width-200, m_xianqqiMotiJikanSettingButtonItem->getPositionY()) );

    this->addChild( m_xianqqiMotiJikanMinusButton , 200);

    // 持ち時間プラス
    m_xianqqiMotiJikanPlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(XianqqiMotiJikanSettingScene::xianqqiMotiJikanPlusButtonDidPushed, this));

    m_xianqqiMotiJikanPlusButton = Menu::create(m_xianqqiMotiJikanPlusButtonItem, NULL);
    m_xianqqiMotiJikanPlusButton->setPosition( Vec2(m_winSize.width-100, m_xianqqiMotiJikanSettingButtonItem->getPositionY()) );

    this->addChild( m_xianqqiMotiJikanPlusButton , 200);

    return true;
}

// button action
void XianqqiMotiJikanSettingScene::backButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    UserDefault::getInstance()->setIntegerForKey("xianqqi_moti_jikan2_1", m_xianqqi_moti_jikan2_1);
    UserDefault::getInstance()->setIntegerForKey("xianqqi_moti_jikan2_2", m_xianqqi_moti_jikan2_2);

    Scene *nextScene = CCTransitionSlideInL::create(0.1f, XianqqiSettingScene::scene());
    Director::getInstance()->replaceScene(nextScene);
}

void XianqqiMotiJikanSettingScene::xianqqiMotiJikanPlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char xianqqiMotiJikanStr[5];
	sprintf(xianqqiMotiJikanStr, "%d", xianqqiMotiJikanIncrement(m_player));
	m_xianqqiMotiJikanDispLabel->setString(xianqqiMotiJikanStr);
}

void XianqqiMotiJikanSettingScene::xianqqiMotiJikanMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char xianqqiMotiJikanStr[5];
	sprintf(xianqqiMotiJikanStr, "%d", xianqqiMotiJikanDecrement(m_player));
	m_xianqqiMotiJikanDispLabel->setString(xianqqiMotiJikanStr);
}

int XianqqiMotiJikanSettingScene::xianqqiMotiJikanIncrement(int player) {
	int xianqqi_moti_jikan;
    if(player == 0) {
    	if(m_xianqqi_moti_jikan2_1 < m_xianqqiMotiJikanMaxLimit) {
    		m_xianqqi_moti_jikan2_1++;
    		xianqqi_moti_jikan = m_xianqqi_moti_jikan2_1;
    	} else {
    		m_xianqqi_moti_jikan2_1 = m_xianqqiMotiJikanMinLimit;
    		xianqqi_moti_jikan = m_xianqqi_moti_jikan2_1;
    	}
    } else {
    	if(m_xianqqi_moti_jikan2_2 < m_xianqqiMotiJikanMaxLimit) {
    		m_xianqqi_moti_jikan2_2++;
    		xianqqi_moti_jikan = m_xianqqi_moti_jikan2_2;
    	} else {
    		m_xianqqi_moti_jikan2_2 = m_xianqqiMotiJikanMinLimit;
    		xianqqi_moti_jikan = m_xianqqi_moti_jikan2_2;
    	}
    }
    return xianqqi_moti_jikan;
}

int XianqqiMotiJikanSettingScene::xianqqiMotiJikanDecrement(int player) {
	int xianqqi_moti_jikan;
    if(player == 0) {
    	if(m_xianqqi_moti_jikan2_1 > m_xianqqiMotiJikanMinLimit) {
    		m_xianqqi_moti_jikan2_1--;
    		xianqqi_moti_jikan = m_xianqqi_moti_jikan2_1;
    	} else {
    		m_xianqqi_moti_jikan2_1 = m_xianqqiMotiJikanMaxLimit;
    		xianqqi_moti_jikan = m_xianqqi_moti_jikan2_1;
    	}
    } else {
    	if(m_xianqqi_moti_jikan2_2 > m_xianqqiMotiJikanMinLimit) {
    		m_xianqqi_moti_jikan2_2--;
    		xianqqi_moti_jikan = m_xianqqi_moti_jikan2_2;
    	} else {
    		m_xianqqi_moti_jikan2_2 = m_xianqqiMotiJikanMaxLimit;
    		xianqqi_moti_jikan = m_xianqqi_moti_jikan2_2;
    	}
    }
    return xianqqi_moti_jikan;
}
