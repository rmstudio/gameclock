/*
 * ByoYomiTimeSetteing.cpp
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#define COCOS2D_DEBUG 1

#include "Setting/CanadaSettingScene.h"
#include "TimeSetting/CanadaKiteiTekazuSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

Scene* CanadaKiteiTekazuSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();

    // 'layer' is an autorelease object
    CanadaKiteiTekazuSettingScene *layer = CanadaKiteiTekazuSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance


bool CanadaKiteiTekazuSettingScene::init()
{

	loadCommonSetting();

	m_winSize = Director::getInstance()->getWinSize();

    // 規定手数
    m_canada_kitei_tekazu1 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_tekazu1", 10);
    m_canada_kitei_tekazu2 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_tekazu2", 10);

    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(CanadaKiteiTekazuSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, m_settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width + 200,
    							m_backButtonItem->getContentSize().height));
    m_settingTitleLayer->addChild(m_backButton, 100);

    this->addChild( m_settingTitleLayer , 160);

    Label *settingTitle = Label::createWithSystemFont("規定手数設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    m_settingTitleLayer->addChild(settingTitle, 1);

    // 規定手数ラベル
	Label *canadaKiteiTekazuLabel = Label::createWithSystemFont("規定手数", "HiraKakuProN-W6", 55);
	canadaKiteiTekazuLabel->setPosition(Vec2(50, m_settingTitleLayer->getPositionY()-80));
	canadaKiteiTekazuLabel->setColor(Color3B(68, 68, 68));
	canadaKiteiTekazuLabel->setAnchorPoint(Vec2(0.0f, 0.5f));

	// 規定手数表示パネル
    m_canadaKiteiTekazuSettingButtonItem = Sprite::create("res/setting_menu_small.png");
    m_canadaKiteiTekazuSettingButtonItem->setPosition( Vec2(40, canadaKiteiTekazuLabel->getPositionY()-90) );
    m_canadaKiteiTekazuSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	this->addChild(canadaKiteiTekazuLabel, 1);

	// 規定時間
	int canada_kitei_tekazu;
    if(m_player == 0) {
    	canada_kitei_tekazu = m_canada_kitei_tekazu1;
    } else {
    	canada_kitei_tekazu = m_canada_kitei_tekazu2;
    }
	char canadaKiteiTekazuStr[5];
	sprintf(canadaKiteiTekazuStr, "%d", canada_kitei_tekazu);

	m_canadaKiteiTekazuDispLabel = Label::createWithSystemFont(canadaKiteiTekazuStr, "HiraKakuProN-W6", 48);
	m_canadaKiteiTekazuDispLabel->setPosition(Vec2(m_canadaKiteiTekazuSettingButtonItem->getPositionX(),
			m_canadaKiteiTekazuSettingButtonItem->getContentSize().height/2));
	m_canadaKiteiTekazuDispLabel->setColor(m_buttonFontColor);
	m_canadaKiteiTekazuDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_canadaKiteiTekazuSettingButtonItem->addChild(m_canadaKiteiTekazuDispLabel, 150);

	m_canadaKiteiTekazuUnitLabel = Label::createWithSystemFont("手", "HiraKakuProN-W6", 48);
	m_canadaKiteiTekazuUnitLabel->setPosition(Vec2(m_canadaKiteiTekazuSettingButtonItem->getContentSize().width - 50,
			m_canadaKiteiTekazuSettingButtonItem->getContentSize().height/2));
	m_canadaKiteiTekazuUnitLabel->setColor(m_buttonFontColor);
	m_canadaKiteiTekazuUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_canadaKiteiTekazuSettingButtonItem->addChild(m_canadaKiteiTekazuUnitLabel, 150);

	this->addChild(m_canadaKiteiTekazuSettingButtonItem, 1);

	// 規定手数マイナス
	m_canadaKiteiTekazuMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(CanadaKiteiTekazuSettingScene::canadaKiteiTekazuMinusButtonDidPushed, this));

	m_canadaKiteiTekazuMinusButton = Menu::create(m_canadaKiteiTekazuMinusButtonItem, NULL);
	m_canadaKiteiTekazuMinusButton->setPosition( Vec2(m_winSize.width-200, m_canadaKiteiTekazuSettingButtonItem->getPositionY()) );

    this->addChild( m_canadaKiteiTekazuMinusButton , 200);

    // 規定手数プラス
    m_canadaKiteiTekazuPlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(CanadaKiteiTekazuSettingScene::canadaKiteiTekazuPlusButtonDidPushed, this));

    m_canadaKiteiTekazuPlusButton = Menu::create(m_canadaKiteiTekazuPlusButtonItem, NULL);
    m_canadaKiteiTekazuPlusButton->setPosition( Vec2(m_winSize.width-100, m_canadaKiteiTekazuSettingButtonItem->getPositionY()) );

    this->addChild( m_canadaKiteiTekazuPlusButton , 200);

    return true;
}

// button action
void CanadaKiteiTekazuSettingScene::backButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    UserDefault::getInstance()->setIntegerForKey("canada_kitei_tekazu1", m_canada_kitei_tekazu1);
    UserDefault::getInstance()->setIntegerForKey("canada_kitei_tekazu2", m_canada_kitei_tekazu2);

    Scene *nextScene = CCTransitionSlideInL::create(0.1f, CanadaSettingScene::scene());
    Director::getInstance()->replaceScene(nextScene);
}

void CanadaKiteiTekazuSettingScene::canadaKiteiTekazuPlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char canadaKiteiTekazuStr[5];
	sprintf(canadaKiteiTekazuStr, "%d", canadaKiteiTekazuIncrement(m_player));
	m_canadaKiteiTekazuDispLabel->setString(canadaKiteiTekazuStr);
}

void CanadaKiteiTekazuSettingScene::canadaKiteiTekazuMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char canadaKiteiTekazuStr[5];
	sprintf(canadaKiteiTekazuStr, "%d", canadaKiteiTekazuDecrement(m_player));
	m_canadaKiteiTekazuDispLabel->setString(canadaKiteiTekazuStr);
}

int CanadaKiteiTekazuSettingScene::canadaKiteiTekazuIncrement(int player) {
	int canada_kitei_tekazu;
    if(player == 0) {
    	if(m_canada_kitei_tekazu1 < m_canadaKiteiTekazuMaxLimit) {
    		m_canada_kitei_tekazu1++;
    		canada_kitei_tekazu = m_canada_kitei_tekazu1;
    	} else {
    		m_canada_kitei_tekazu1 = m_canadaKiteiTekazuMinLimit;
    		canada_kitei_tekazu = m_canada_kitei_tekazu1;
    	}
    } else {
    	if(m_canada_kitei_tekazu2 < m_canadaKiteiTekazuMaxLimit) {
    		m_canada_kitei_tekazu2++;
    		canada_kitei_tekazu = m_canada_kitei_tekazu2;
    	} else {
    		m_canada_kitei_tekazu2 = m_canadaKiteiTekazuMinLimit;
    		canada_kitei_tekazu = m_canada_kitei_tekazu2;
    	}
    }
    return canada_kitei_tekazu;
}

int CanadaKiteiTekazuSettingScene::canadaKiteiTekazuDecrement(int player) {
	int canada_kitei_tekazu;
    if(player == 0) {
    	if(m_canada_kitei_tekazu1 > m_canadaKiteiTekazuMinLimit) {
    		m_canada_kitei_tekazu1--;
    		canada_kitei_tekazu = m_canada_kitei_tekazu1;
    	} else {
    		m_canada_kitei_tekazu1 = m_canadaKiteiTekazuMaxLimit;
    		canada_kitei_tekazu = m_canada_kitei_tekazu1;
    	}
    } else {
    	if(m_canada_kitei_tekazu2 > m_canadaKiteiTekazuMinLimit) {
    		m_canada_kitei_tekazu2--;
    		canada_kitei_tekazu = m_canada_kitei_tekazu2;
    	} else {
    		m_canada_kitei_tekazu2 = m_canadaKiteiTekazuMaxLimit;
    		canada_kitei_tekazu = m_canada_kitei_tekazu2;
    	}
    }
    return canada_kitei_tekazu;
}
