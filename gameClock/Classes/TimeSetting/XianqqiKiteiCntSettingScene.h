
#ifndef _XIANQQI_KITEI_CNT1_SETTING_H_
#define _XIANQQI_KITEI_CNT1_SETTING_H_

#include "cocos2d.h"
#include "DialogLayer.h"
#include "TimeSettingScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

class XianqqiKiteiCntSettingScene : public TimeSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(XianqqiKiteiCntSettingScene);

    Size m_winSize;

    // 設定値
    int m_xianqqi_kitei_cnt1;								// 規定回数（プレイヤー１）
    int m_xianqqi_kitei_cnt2;								// 規定回数（プレイヤー２）
    int m_xianqqiKiteiCntMaxLimit = 99;					 	// 規定回数最大値
    int m_xianqqiKiteiCntMinLimit = 1;						// 規定回数最小値

    Sprite *m_xianqqiKiteiCntSettingButtonItem;
    Menu *m_xianqqiKiteiCntSettingButton;

    MenuItemImage *m_xianqqiKiteiCntMinusButtonItem;
    Menu *m_xianqqiKiteiCntMinusButton;

    MenuItemImage *m_xianqqiKiteiCntPlusButtonItem;
    Menu *m_xianqqiKiteiCntPlusButton;

    Label *m_xianqqiKiteiCntDispLabel;
    Label *m_xianqqiKiteiCntUnitLabel;

    MenuItemImage *m_backButtonItem;
    Menu *m_backButton;

    // button action
    void backButtonDidPushed(Ref* pSender);
    void xianqqiKiteiCntPlusButtonDidPushed(Ref* pSender);
    void xianqqiKiteiCntMinusButtonDidPushed(Ref* pSender);

private:
    int xianqqiKiteiCntIncrement(int player);
    int xianqqiKiteiCntDecrement(int player);
};


#endif
