/*
 * ByoYomiTimeSetting.h
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#ifndef _CANADA_KITEI_TIME_SETTING_H_
#define _CANADA_KITEI_TIME_SETTING_H_

#include "cocos2d.h"
#include "DialogLayer.h"
#include "TimeSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class CanadaKiteiTimeSettingScene : public TimeSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(CanadaKiteiTimeSettingScene);

    Size m_winSize;

    // 設定値
    int m_canada_kitei_time1;								// 規定時間（プレイヤー１）
    int m_canada_kitei_time2;								// 規定時間（プレイヤー２）
    int m_canadaKiteiTimeMaxLimit = 60;						// 規定時間最大値
    int m_canadaKiteiTimeMinLimit = 1;						// 規定時間最小値

    Sprite *m_canadaKiteiTimeSettingButtonItem;
    Menu *m_canadaKiteiTimeSettingButton;

    MenuItemImage *m_canadaKiteiTimeMinusButtonItem;
    Menu *m_canadaKiteiTimeMinusButton;

    MenuItemImage *m_canadaKiteiTimePlusButtonItem;
    Menu *m_canadaKiteiTimePlusButton;

    Label *m_canadaKiteiTimeDispLabel;
    Label *m_canadaKiteiTimeUnitLabel;

    // 増減ボタン
    ControlStepper *m_canadaKiteiTimeInputButton;	// 考慮時間入力ボタン

    MenuItemImage *m_backButtonItem;
    Menu *m_backButton;

    // button action
    void backButtonDidPushed(Ref* pSender);
    void canadaKiteiTimePlusButtonDidPushed(Ref* pSender);
    void canadaKiteiTimeMinusButtonDidPushed(Ref* pSender);

private:
    int canadaKiteiTimeIncrement(int player);
    int canadaKiteiTimeDecrement(int player);
};


#endif
