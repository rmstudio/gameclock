/*
 * ByoYomiTimeSetting.h
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#ifndef _FISHER_ADD_TIME_SETTING_H_
#define _FISHER_ADD_TIME_SETTING_H_

#include "cocos2d.h"
#include "DialogLayer.h"
#include "TimeSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class FisherAddTimeSettingScene : public TimeSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(FisherAddTimeSettingScene);

    Size m_winSize;

    // 設定値
    int m_fisher_additional_time1;						// 加算時間（プレイヤー１）
    int m_fisher_additional_time2;						// 加算時間（プレイヤー２）
    int m_fisherAddTimeMaxLimit = 60;					// 加算時間最大値
    int m_fisherAddTimeMinLimit = 1;					// 加算時間最小値

    Sprite *m_fisherAddTimeSettingButtonItem;
    Menu *m_fisherAddTimeSettingButton;

    MenuItemImage *m_fisherAddTimeMinusButtonItem;
    Menu *m_fisherAddTimeMinusButton;

    MenuItemImage *m_fisherAddTimePlusButtonItem;
    Menu *m_fisherAddTimePlusButton;

    Label *m_fisherAddTimeDispLabel;
    Label *m_fisherAddTimeUnitLabel;

    MenuItemImage *m_backButtonItem;
    Menu *m_backButton;

    // button action
    void backButtonDidPushed(Ref* pSender);
    void fisherAddTimePlusButtonDidPushed(Ref* pSender);
    void fisherAddTimeMinusButtonDidPushed(Ref* pSender);

private:
    int fisherAddTimeIncrement(int player);
    int fisherAddTimeDecrement(int player);
};


#endif
