/*
 * ByoYomiTimeSetting.h
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#ifndef _CANADA_KITEI_TEKAZU_SETTING_H_
#define _CANADA_KITEI_TEKAZU_SETTING_H_

#include "cocos2d.h"
#include "DialogLayer.h"
#include "TimeSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class CanadaKiteiTekazuSettingScene : public TimeSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(CanadaKiteiTekazuSettingScene);

    Size m_winSize;

    // 設定値
    int m_canada_kitei_tekazu1;								// 規定手数（プレイヤー１）
    int m_canada_kitei_tekazu2;								// 規定手数（プレイヤー２）
    int m_canadaKiteiTekazuMaxLimit = 99;					// 規定手数最大値
    int m_canadaKiteiTekazuMinLimit = 1;					// 規定手数最小値

    Sprite *m_canadaKiteiTekazuSettingButtonItem;
    Menu *m_canadaKiteiTekazuSettingButton;

    MenuItemImage *m_canadaKiteiTekazuMinusButtonItem;
    Menu *m_canadaKiteiTekazuMinusButton;

    MenuItemImage *m_canadaKiteiTekazuPlusButtonItem;
    Menu *m_canadaKiteiTekazuPlusButton;

    Label *m_canadaKiteiTekazuDispLabel;
    Label *m_canadaKiteiTekazuUnitLabel;

    MenuItemImage *m_backButtonItem;
    Menu *m_backButton;

    // button action
    void backButtonDidPushed(Ref* pSender);
    void canadaKiteiTekazuPlusButtonDidPushed(Ref* pSender);
    void canadaKiteiTekazuMinusButtonDidPushed(Ref* pSender);

private:
    int canadaKiteiTekazuIncrement(int player);
    int canadaKiteiTekazuDecrement(int player);
};


#endif
