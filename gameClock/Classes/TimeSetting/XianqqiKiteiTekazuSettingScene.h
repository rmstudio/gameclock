/*
 * ByoYomiTimeSetting.h
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#ifndef _XIANQQI_KITEI_TEKAZU_SETTING_H_
#define _XIANQQI_KITEI_TEKAZU_SETTING_H_

#include "cocos2d.h"
#include "DialogLayer.h"
#include "TimeSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class XianqqiKiteiTekazuSettingScene : public TimeSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(XianqqiKiteiTekazuSettingScene);

    Size m_winSize;

    // 設定値
    int m_xianqqi_kitei_tekazu1_1;							// 規定手数（プレイヤー１）
    int m_xianqqi_kitei_tekazu1_2;							// 規定手数（プレイヤー２）
    int m_xianqqiKiteiTekazuMaxLimit = 99;					// 規定手数最大値
    int m_xianqqiKiteiTekazuMinLimit = 1;					// 規定手数最小値

    Sprite *m_xianqqiKiteiTekazuSettingButtonItem;
    Menu *m_xianqqiKiteiTekazuSettingButton;

    MenuItemImage *m_xianqqiKiteiTekazuMinusButtonItem;
    Menu *m_xianqqiKiteiTekazuMinusButton;

    MenuItemImage *m_xianqqiKiteiTekazuPlusButtonItem;
    Menu *m_xianqqiKiteiTekazuPlusButton;

    Label *m_xianqqiKiteiTekazuDispLabel;
    Label *m_xianqqiKiteiTekazuUnitLabel;

    // 増減ボタン
    ControlStepper *m_xianqqiKiteiTekazuInputButton;	// 考慮時間入力ボタン

    MenuItemImage *m_backButtonItem;
    Menu *m_backButton;

    // button action
    void backButtonDidPushed(Ref* pSender);
    void xianqqiKiteiTekazuPlusButtonDidPushed(Ref* pSender);
    void xianqqiKiteiTekazuMinusButtonDidPushed(Ref* pSender);

private:
    int xianqqiKiteiTekazuIncrement(int player);
    int xianqqiKiteiTekazuDecrement(int player);
};


#endif
