
#define COCOS2D_DEBUG 1

#include "Setting/XianqqiSettingScene.h"
#include "TimeSetting/XianqqiKiteiTekazu2SettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

Scene* XianqqiKiteiTekazu2SettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();

    // 'layer' is an autorelease object
    XianqqiKiteiTekazu2SettingScene *layer = XianqqiKiteiTekazu2SettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance


bool XianqqiKiteiTekazu2SettingScene::init()
{

	loadCommonSetting();

	m_winSize = Director::getInstance()->getWinSize();

    // 規定手数
    m_xianqqi_kitei_tekazu2_1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu2_1", 5);
    m_xianqqi_kitei_tekazu2_2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu2_2", 5);

    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(XianqqiKiteiTekazu2SettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, m_settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width + 200,
    							m_backButtonItem->getContentSize().height));
    m_settingTitleLayer->addChild(m_backButton, 100);

    this->addChild( m_settingTitleLayer , 160);

    Label *settingTitle = Label::createWithSystemFont("規定手数設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    m_settingTitleLayer->addChild(settingTitle, 1);

	Label *xianqqiKiteiTekazuLabel = Label::createWithSystemFont("規定手数", "HiraKakuProN-W6", 48);
	xianqqiKiteiTekazuLabel->setPosition(Vec2(50, m_settingTitleLayer->getPositionY()-80));
	xianqqiKiteiTekazuLabel->setColor(Color3B(68, 68, 68));
	xianqqiKiteiTekazuLabel->setAnchorPoint(Vec2(0.0f, 0.5f));

	this->addChild(xianqqiKiteiTekazuLabel, 1);

	// 規定手数パネル
    m_xianqqiKiteiTekazuSettingButtonItem = Sprite::create("res/setting_menu_small.png");
    m_xianqqiKiteiTekazuSettingButtonItem->setPosition( Vec2(40, xianqqiKiteiTekazuLabel->getPositionY()-90) );
    m_xianqqiKiteiTekazuSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

    this->addChild(m_xianqqiKiteiTekazuSettingButtonItem, 1);

	// 規定手数
	int xianqqi_kitei_tekazu;
    if(m_player == 0) {
    	xianqqi_kitei_tekazu = m_xianqqi_kitei_tekazu2_1;
    } else {
    	xianqqi_kitei_tekazu = m_xianqqi_kitei_tekazu2_2;
    }
	char xianqqiKiteiTekazuStr[5];
	sprintf(xianqqiKiteiTekazuStr, "%d", xianqqi_kitei_tekazu);

	m_xianqqiKiteiTekazuDispLabel = Label::createWithSystemFont(xianqqiKiteiTekazuStr, "HiraKakuProN-W6", 48);
	m_xianqqiKiteiTekazuDispLabel->setPosition(Vec2(m_xianqqiKiteiTekazuSettingButtonItem->getPositionX(),
			m_xianqqiKiteiTekazuSettingButtonItem->getContentSize().height/2));
	m_xianqqiKiteiTekazuDispLabel->setColor(m_buttonFontColor);
	m_xianqqiKiteiTekazuDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_xianqqiKiteiTekazuSettingButtonItem->addChild(m_xianqqiKiteiTekazuDispLabel, 150);

	m_xianqqiKiteiTekazuUnitLabel = Label::createWithSystemFont("手", "HiraKakuProN-W6", 48);
	m_xianqqiKiteiTekazuUnitLabel->setPosition(Vec2(m_xianqqiKiteiTekazuSettingButtonItem->getContentSize().width - 50,
			m_xianqqiKiteiTekazuSettingButtonItem->getContentSize().height/2));
	m_xianqqiKiteiTekazuUnitLabel->setColor(m_buttonFontColor);
	m_xianqqiKiteiTekazuUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_xianqqiKiteiTekazuSettingButtonItem->addChild(m_xianqqiKiteiTekazuUnitLabel, 150);

	this->addChild(m_xianqqiKiteiTekazuSettingButtonItem, 1);

	// 加算時間マイナス
	m_xianqqiKiteiTekazuMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(XianqqiKiteiTekazu2SettingScene::xianqqiKiteiTekazuMinusButtonDidPushed, this));

	m_xianqqiKiteiTekazuMinusButton = Menu::create(m_xianqqiKiteiTekazuMinusButtonItem, NULL);
	m_xianqqiKiteiTekazuMinusButton->setPosition( Vec2(m_winSize.width-200, m_xianqqiKiteiTekazuSettingButtonItem->getPositionY()) );

    this->addChild( m_xianqqiKiteiTekazuMinusButton , 200);

    // 加算時間プラス
    m_xianqqiKiteiTekazuPlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(XianqqiKiteiTekazu2SettingScene::xianqqiKiteiTekazuPlusButtonDidPushed, this));

    m_xianqqiKiteiTekazuPlusButton = Menu::create(m_xianqqiKiteiTekazuPlusButtonItem, NULL);
    m_xianqqiKiteiTekazuPlusButton->setPosition( Vec2(m_winSize.width-100, m_xianqqiKiteiTekazuSettingButtonItem->getPositionY()) );

    this->addChild( m_xianqqiKiteiTekazuPlusButton , 200);

    return true;
}

// button action
void XianqqiKiteiTekazu2SettingScene::backButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    UserDefault::getInstance()->setIntegerForKey("xianqqi_kitei_tekazu2_1", m_xianqqi_kitei_tekazu2_1);
    UserDefault::getInstance()->setIntegerForKey("xianqqi_kitei_tekazu2_2", m_xianqqi_kitei_tekazu2_2);

    Scene *nextScene = CCTransitionSlideInL::create(0.1f, XianqqiSettingScene::scene());
    Director::getInstance()->replaceScene(nextScene);
}

void XianqqiKiteiTekazu2SettingScene::xianqqiKiteiTekazuPlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char xianqqiKiteiTekazuStr[5];
	sprintf(xianqqiKiteiTekazuStr, "%d", xianqqiKiteiTekazuIncrement(m_player));
	m_xianqqiKiteiTekazuDispLabel->setString(xianqqiKiteiTekazuStr);
}

void XianqqiKiteiTekazu2SettingScene::xianqqiKiteiTekazuMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char xianqqiKiteiTekazuStr[5];
	sprintf(xianqqiKiteiTekazuStr, "%d", xianqqiKiteiTekazuDecrement(m_player));
	m_xianqqiKiteiTekazuDispLabel->setString(xianqqiKiteiTekazuStr);
}

int XianqqiKiteiTekazu2SettingScene::xianqqiKiteiTekazuIncrement(int player) {
	int xianqqi_kitei_tekazu;
    if(player == 0) {
    	if(m_xianqqi_kitei_tekazu2_1 < m_xianqqiKiteiTekazuMaxLimit) {
    		m_xianqqi_kitei_tekazu2_1++;
    		xianqqi_kitei_tekazu = m_xianqqi_kitei_tekazu2_1;
    	} else {
    		m_xianqqi_kitei_tekazu2_1 = m_xianqqiKiteiTekazuMinLimit;
    		xianqqi_kitei_tekazu = m_xianqqi_kitei_tekazu2_1;
    	}
    } else {
    	if(m_xianqqi_kitei_tekazu2_2 < m_xianqqiKiteiTekazuMaxLimit) {
    		m_xianqqi_kitei_tekazu2_2++;
    		xianqqi_kitei_tekazu = m_xianqqi_kitei_tekazu2_2;
    	} else {
    		m_xianqqi_kitei_tekazu2_2 = m_xianqqiKiteiTekazuMinLimit;
    		xianqqi_kitei_tekazu = m_xianqqi_kitei_tekazu2_2;
    	}
    }
    return xianqqi_kitei_tekazu;
}

int XianqqiKiteiTekazu2SettingScene::xianqqiKiteiTekazuDecrement(int player) {
	int xianqqi_kitei_tekazu;
    if(player == 0) {
    	if(m_xianqqi_kitei_tekazu2_1 > m_xianqqiKiteiTekazuMinLimit) {
    		m_xianqqi_kitei_tekazu2_1--;
    		xianqqi_kitei_tekazu = m_xianqqi_kitei_tekazu2_1;
    	} else {
    		m_xianqqi_kitei_tekazu2_1 = m_xianqqiKiteiTekazuMaxLimit;
    		xianqqi_kitei_tekazu = m_xianqqi_kitei_tekazu2_1;
    	}
    } else {
    	if(m_xianqqi_kitei_tekazu2_2 > m_xianqqiKiteiTekazuMinLimit) {
    		m_xianqqi_kitei_tekazu2_2--;
    		xianqqi_kitei_tekazu = m_xianqqi_kitei_tekazu2_2;
    	} else {
    		m_xianqqi_kitei_tekazu2_2 = m_xianqqiKiteiTekazuMaxLimit;
    		xianqqi_kitei_tekazu = m_xianqqi_kitei_tekazu2_2;
    	}
    }
    return xianqqi_kitei_tekazu;
}

