/*
 * ByoYomiTimeSetteing.cpp
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#define COCOS2D_DEBUG 1

#include "Setting/CanadaSettingScene.h"
#include "TimeSetting/CanadaKiteiTimeSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

Scene* CanadaKiteiTimeSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();

    // 'layer' is an autorelease object
    CanadaKiteiTimeSettingScene *layer = CanadaKiteiTimeSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance


bool CanadaKiteiTimeSettingScene::init()
{

	loadCommonSetting();

	m_winSize = Director::getInstance()->getWinSize();

    // 規定時間
    m_canada_kitei_time1 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_time1", 30);
    m_canada_kitei_time2 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_time2", 30);

    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(CanadaKiteiTimeSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, m_settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width + 200,
    							m_backButtonItem->getContentSize().height));
    m_settingTitleLayer->addChild(m_backButton, 100);

    this->addChild( m_settingTitleLayer , 160);

    Label *settingTitle = Label::createWithSystemFont("規定時間設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    m_settingTitleLayer->addChild(settingTitle, 1);

	Label *canadaKiteiTimeLabel = Label::createWithSystemFont("規定時間", "HiraKakuProN-W6", 55);
	canadaKiteiTimeLabel->setPosition(Vec2(50, m_settingTitleLayer->getPositionY()-80));
	canadaKiteiTimeLabel->setColor(Color3B(68, 68, 68));
	canadaKiteiTimeLabel->setAnchorPoint(Vec2(0.0f, 0.5f));

	this->addChild(canadaKiteiTimeLabel, 1);

	// 規定時間表示ラベル
    m_canadaKiteiTimeSettingButtonItem = Sprite::create("res/setting_menu_small.png");
    m_canadaKiteiTimeSettingButtonItem->setPosition( Vec2(40, canadaKiteiTimeLabel->getPositionY()-90) );
    m_canadaKiteiTimeSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	// 規定時間
	int canada_kitei_time;
    if(m_player == 0) {
    	canada_kitei_time = m_canada_kitei_time1;
    } else {
    	canada_kitei_time = m_canada_kitei_time2;
    }
	char canadaKiteiTimeStr[5];
	sprintf(canadaKiteiTimeStr, "%d", canada_kitei_time);

	m_canadaKiteiTimeDispLabel = Label::createWithSystemFont(canadaKiteiTimeStr, "HiraKakuProN-W6", 48);
	m_canadaKiteiTimeDispLabel->setPosition(Vec2(m_canadaKiteiTimeSettingButtonItem->getPositionX(),
			m_canadaKiteiTimeSettingButtonItem->getContentSize().height/2));
	m_canadaKiteiTimeDispLabel->setColor(m_buttonFontColor);
	m_canadaKiteiTimeDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_canadaKiteiTimeSettingButtonItem->addChild(m_canadaKiteiTimeDispLabel, 150);

	m_canadaKiteiTimeUnitLabel = Label::createWithSystemFont("分", "HiraKakuProN-W6", 48);
	m_canadaKiteiTimeUnitLabel->setPosition(Vec2(m_canadaKiteiTimeSettingButtonItem->getContentSize().width - 50,
			m_canadaKiteiTimeSettingButtonItem->getContentSize().height/2));
	m_canadaKiteiTimeUnitLabel->setColor(m_buttonFontColor);
	m_canadaKiteiTimeUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_canadaKiteiTimeSettingButtonItem->addChild(m_canadaKiteiTimeUnitLabel, 150);

	this->addChild(m_canadaKiteiTimeSettingButtonItem, 1);

	// 規定時間マイナス
	m_canadaKiteiTimeMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(CanadaKiteiTimeSettingScene::canadaKiteiTimeMinusButtonDidPushed, this));

	m_canadaKiteiTimeMinusButton = Menu::create(m_canadaKiteiTimeMinusButtonItem, NULL);
	m_canadaKiteiTimeMinusButton->setPosition( Vec2(m_winSize.width-200, m_canadaKiteiTimeSettingButtonItem->getPositionY()) );

    this->addChild( m_canadaKiteiTimeMinusButton , 200);

    // 規定時間プラス
    m_canadaKiteiTimePlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(CanadaKiteiTimeSettingScene::canadaKiteiTimePlusButtonDidPushed, this));

    m_canadaKiteiTimePlusButton = Menu::create(m_canadaKiteiTimePlusButtonItem, NULL);
    m_canadaKiteiTimePlusButton->setPosition( Vec2(m_winSize.width-100, m_canadaKiteiTimeSettingButtonItem->getPositionY()) );

    this->addChild( m_canadaKiteiTimePlusButton , 200);

    return true;
}

// button action
void CanadaKiteiTimeSettingScene::backButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    UserDefault::getInstance()->setIntegerForKey("canada_kitei_time1", m_canada_kitei_time1);
    UserDefault::getInstance()->setIntegerForKey("canada_kitei_time2", m_canada_kitei_time2);

    Scene *nextScene = CCTransitionSlideInL::create(0.1f, CanadaSettingScene::scene());
    Director::getInstance()->replaceScene(nextScene);
}

void CanadaKiteiTimeSettingScene::canadaKiteiTimePlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char canadaKiteiTimeStr[5];
	sprintf(canadaKiteiTimeStr, "%d", canadaKiteiTimeIncrement(m_player));
	m_canadaKiteiTimeDispLabel->setString(canadaKiteiTimeStr);
}

void CanadaKiteiTimeSettingScene::canadaKiteiTimeMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char canadaKiteiTimeStr[5];
	sprintf(canadaKiteiTimeStr, "%d", canadaKiteiTimeDecrement(m_player));
	m_canadaKiteiTimeDispLabel->setString(canadaKiteiTimeStr);
}

int CanadaKiteiTimeSettingScene::canadaKiteiTimeIncrement(int player) {
	int canada_kitei_time;
    if(player == 0) {
    	if(m_canada_kitei_time1 < m_canadaKiteiTimeMaxLimit) {
    		m_canada_kitei_time1++;
    		canada_kitei_time = m_canada_kitei_time1;
    	} else {
    		m_canada_kitei_time1 = m_canadaKiteiTimeMinLimit;
    		canada_kitei_time = m_canada_kitei_time1;
    	}
    } else {
    	if(m_canada_kitei_time2 < m_canadaKiteiTimeMaxLimit) {
    		m_canada_kitei_time2++;
    		canada_kitei_time = m_canada_kitei_time2;
    	} else {
    		m_canada_kitei_time2 = m_canadaKiteiTimeMinLimit;
    		canada_kitei_time = m_canada_kitei_time2;
    	}
    }
    return canada_kitei_time;
}

int CanadaKiteiTimeSettingScene::canadaKiteiTimeDecrement(int player) {
	int canada_kitei_time;
    if(player == 0) {
    	if(m_canada_kitei_time1 > m_canadaKiteiTimeMinLimit) {
    		m_canada_kitei_time1--;
    		canada_kitei_time = m_canada_kitei_time1;
    	} else {
    		m_canada_kitei_time1 = m_canadaKiteiTimeMaxLimit;
    		canada_kitei_time = m_canada_kitei_time1;
    	}
    } else {
    	if(m_canada_kitei_time2 > m_canadaKiteiTimeMinLimit) {
    		m_canada_kitei_time2--;
    		canada_kitei_time = m_canada_kitei_time2;
    	} else {
    		m_canada_kitei_time2 = m_canadaKiteiTimeMaxLimit;
    		canada_kitei_time = m_canada_kitei_time2;
    	}
    }
    return canada_kitei_time;
}
