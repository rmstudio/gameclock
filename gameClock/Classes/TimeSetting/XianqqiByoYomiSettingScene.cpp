/*
 * ByoYomiTimeSetteing.cpp
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#define COCOS2D_DEBUG 1

#include "Setting/XianqqiSettingScene.h"
#include "TimeSetting/XianqqiByoYomiSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

Scene* XianqqiByoYomiSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();

    // 'layer' is an autorelease object
    XianqqiByoYomiSettingScene *layer = XianqqiByoYomiSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance


bool XianqqiByoYomiSettingScene::init()
{

	loadCommonSetting();

	m_winSize = Director::getInstance()->getWinSize();

    // 秒読み
    m_xianqqi_byo_yomi_time1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_byo_yomi_time1", 10);
    m_xianqqi_byo_yomi_time2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_byo_yomi_time2", 10);

    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(XianqqiByoYomiSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, m_settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width + 200,
    							m_backButtonItem->getContentSize().height));
    m_settingTitleLayer->addChild(m_backButton, 100);

    this->addChild( m_settingTitleLayer , 160);

    Label *settingTitle = Label::createWithSystemFont("秒読み設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    m_settingTitleLayer->addChild(settingTitle, 1);

    // 考慮回数ラベル
	Label *xianqqiByoYomiLabel = Label::createWithSystemFont("秒読み", "HiraKakuProN-W6", 55);
	xianqqiByoYomiLabel->setPosition(Vec2(50, m_settingTitleLayer->getPositionY()-80));
	xianqqiByoYomiLabel->setColor(Color3B(68, 68, 68));
	xianqqiByoYomiLabel->setAnchorPoint(Vec2(0.0f, 0.5f));

	this->addChild(xianqqiByoYomiLabel, 1);

	// 秒読みパネル
    m_xianqqiByoYomiSettingButtonItem = Sprite::create("res/setting_menu_small.png");
    m_xianqqiByoYomiSettingButtonItem->setPosition( Vec2(40, xianqqiByoYomiLabel->getPositionY()-90) );
    m_xianqqiByoYomiSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	// 秒読み表示
	int xianqqi_byo_yomi_time;
    if(m_player == 0) {
    	xianqqi_byo_yomi_time = m_xianqqi_byo_yomi_time1;
    } else {
    	xianqqi_byo_yomi_time = m_xianqqi_byo_yomi_time2;
    }
	char xianqqiByoYomiTimeStr[5];
	sprintf(xianqqiByoYomiTimeStr, "%d", xianqqi_byo_yomi_time);

	m_xianqqiByoYomiDispLabel = Label::createWithSystemFont(xianqqiByoYomiTimeStr, "HiraKakuProN-W6", 48);
	m_xianqqiByoYomiDispLabel->setPosition(Vec2(m_xianqqiByoYomiSettingButtonItem->getPositionX(),
			m_xianqqiByoYomiSettingButtonItem->getContentSize().height/2));
	m_xianqqiByoYomiDispLabel->setColor(m_buttonFontColor);
	m_xianqqiByoYomiDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_xianqqiByoYomiSettingButtonItem->addChild(m_xianqqiByoYomiDispLabel, 150);

	m_xianqqiByoYomiUnitLabel = Label::createWithSystemFont("秒", "HiraKakuProN-W6", 48);
	m_xianqqiByoYomiUnitLabel->setPosition(Vec2(m_xianqqiByoYomiSettingButtonItem->getContentSize().width - 50,
			m_xianqqiByoYomiSettingButtonItem->getContentSize().height/2));
	m_xianqqiByoYomiUnitLabel->setColor(m_buttonFontColor);
	m_xianqqiByoYomiUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_xianqqiByoYomiSettingButtonItem->addChild(m_xianqqiByoYomiUnitLabel, 150);

	this->addChild(m_xianqqiByoYomiSettingButtonItem, 1);

	// 秒読みマイナス
	m_xianqqiByoYomiTimeMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(XianqqiByoYomiSettingScene::xianqqiByoYomiTimeMinusButtonDidPushed, this));

	m_xianqqiByoYomiTimeMinusButton = Menu::create(m_xianqqiByoYomiTimeMinusButtonItem, NULL);
	m_xianqqiByoYomiTimeMinusButton->setPosition( Vec2(m_winSize.width-200, m_xianqqiByoYomiSettingButtonItem->getPositionY()) );

    this->addChild( m_xianqqiByoYomiTimeMinusButton , 200);

    // 秒読みプラス
    m_xianqqiByoYomiTimePlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(XianqqiByoYomiSettingScene::xianqqiByoYomiTimePlusButtonDidPushed, this));

    m_xianqqiByoYomiTimePlusButton = Menu::create(m_xianqqiByoYomiTimePlusButtonItem, NULL);
    m_xianqqiByoYomiTimePlusButton->setPosition( Vec2(m_winSize.width-100, m_xianqqiByoYomiSettingButtonItem->getPositionY()) );

    this->addChild( m_xianqqiByoYomiTimePlusButton , 200);

    return true;
}

// button action
void XianqqiByoYomiSettingScene::backButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    UserDefault::getInstance()->setIntegerForKey("xianqqi_byo_yomi_time1", m_xianqqi_byo_yomi_time1);
    UserDefault::getInstance()->setIntegerForKey("xianqqi_byo_yomi_time2", m_xianqqi_byo_yomi_time2);

    Scene *nextScene = CCTransitionSlideInL::create(0.1f, XianqqiSettingScene::scene());
    Director::getInstance()->replaceScene(nextScene);
}

void XianqqiByoYomiSettingScene::xianqqiByoYomiTimePlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char xianqqiByoYomiTimeStr[5];
	sprintf(xianqqiByoYomiTimeStr, "%d", xianqqiByoYomiTimeIncrement(m_player));
	m_xianqqiByoYomiDispLabel->setString(xianqqiByoYomiTimeStr);
}

void XianqqiByoYomiSettingScene::xianqqiByoYomiTimeMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char xianqqiByoYomiTimeStr[5];
	sprintf(xianqqiByoYomiTimeStr, "%d", xianqqiByoYomiTimeDecrement(m_player));
	m_xianqqiByoYomiDispLabel->setString(xianqqiByoYomiTimeStr);
}

int XianqqiByoYomiSettingScene::xianqqiByoYomiTimeIncrement(int player) {
	int xianqqi_byo_yomi_time;
    if(player == 0) {
    	if(m_xianqqi_byo_yomi_time1 < m_xianqqiByoYomiTimeMaxLimit) {
    		m_xianqqi_byo_yomi_time1++;
    		xianqqi_byo_yomi_time = m_xianqqi_byo_yomi_time1;
    	} else {
    		m_xianqqi_byo_yomi_time1 = m_xianqqiByoYomiTimeMinLimit;
    		xianqqi_byo_yomi_time = m_xianqqi_byo_yomi_time1;
    	}
    } else {
    	if(m_xianqqi_byo_yomi_time2 < m_xianqqiByoYomiTimeMaxLimit) {
    		m_xianqqi_byo_yomi_time2++;
    		xianqqi_byo_yomi_time = m_xianqqi_byo_yomi_time2;
    	} else {
    		m_xianqqi_byo_yomi_time2 = m_xianqqiByoYomiTimeMinLimit;
    		xianqqi_byo_yomi_time = m_xianqqi_byo_yomi_time2;
    	}
    }
    return xianqqi_byo_yomi_time;
}

int XianqqiByoYomiSettingScene::xianqqiByoYomiTimeDecrement(int player) {
	int xianqqi_byo_yomi_time;
    if(player == 0) {
    	if(m_xianqqi_byo_yomi_time1 > m_xianqqiByoYomiTimeMinLimit) {
    		m_xianqqi_byo_yomi_time1--;
    		xianqqi_byo_yomi_time = m_xianqqi_byo_yomi_time1;
    	} else {
    		m_xianqqi_byo_yomi_time1 = m_xianqqiByoYomiTimeMaxLimit;
    		xianqqi_byo_yomi_time = m_xianqqi_byo_yomi_time1;
    	}
    } else {
    	if(m_xianqqi_byo_yomi_time2 > m_xianqqiByoYomiTimeMinLimit) {
    		m_xianqqi_byo_yomi_time2--;
    		xianqqi_byo_yomi_time = m_xianqqi_byo_yomi_time2;
    	} else {
    		m_xianqqi_byo_yomi_time2 = m_xianqqiByoYomiTimeMaxLimit;
    		xianqqi_byo_yomi_time = m_xianqqi_byo_yomi_time2;
    	}
    }
    return xianqqi_byo_yomi_time;
}
