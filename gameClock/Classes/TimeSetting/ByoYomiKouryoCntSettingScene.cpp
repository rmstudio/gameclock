/*
 * ByoYomiTimeSetteing.cpp
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#define COCOS2D_DEBUG 1

#include "Setting/ByoYomiSettingScene.h"
#include "TimeSetting/ByoYomiKouryoCntSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

Scene* ByoYomiKouryoCntSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();

    // 'layer' is an autorelease object
    ByoYomiKouryoCntSettingScene *layer = ByoYomiKouryoCntSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance


bool ByoYomiKouryoCntSettingScene::init()
{
	loadCommonSetting();

	m_winSize = Director::getInstance()->getWinSize();

    // 考慮回数
    m_byo_yomi_kouryo_cnt1 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_cnt1", 3);
    m_byo_yomi_kouryo_cnt2 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_cnt2", 3);

    Label *settingTitle = Label::createWithSystemFont("考慮回数設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    settingTitle->setColor(m_titleFontColor);
    settingTitle->setAnchorPoint(Vec2(0.5f, 0.5f));
    m_settingTitleLayer->addChild(settingTitle, 1);

    auto keyboardListener = cocos2d::EventListenerKeyboard::create();
    keyboardListener->onKeyReleased = CC_CALLBACK_2(ByoYomiKouryoCntSettingScene::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);

    // 戻るボタン
    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(ByoYomiKouryoCntSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, m_settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width + 200,
    							m_backButtonItem->getContentSize().height));
    m_settingTitleLayer->addChild(m_backButton, 100);

    this->addChild( m_settingTitleLayer , 160);

    // 考慮回数ラベル
	Label *kouryoCntLabel = Label::createWithSystemFont("考慮回数", "HiraKakuProN-W6", 55);
	kouryoCntLabel->setPosition(Vec2(50, m_settingTitleLayer->getPositionY()-80));
	kouryoCntLabel->setColor(Color3B(68, 68, 68));
	kouryoCntLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	this->addChild(kouryoCntLabel, 150);

	// 考慮回数パネル
	m_byoYomiKouryoCntSettingButtonItem = Sprite::create("res/setting_menu_small.png");
	m_byoYomiKouryoCntSettingButtonItem->setPosition( Vec2(40, kouryoCntLabel->getPositionY()-90) );
	m_byoYomiKouryoCntSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	this->addChild(m_byoYomiKouryoCntSettingButtonItem, 1);

	// 秒読み考慮回数表示
	int byo_yomi_kouryo_cnt;
    if(m_player == 0) {
    	byo_yomi_kouryo_cnt = m_byo_yomi_kouryo_cnt1;
    } else {
    	byo_yomi_kouryo_cnt = m_byo_yomi_kouryo_cnt2;
    }
	char byoYomiKouryoCntStr[5];
	sprintf(byoYomiKouryoCntStr, "%d", byo_yomi_kouryo_cnt);

	m_byoYomiKouryoCntDispLabel = Label::createWithSystemFont(byoYomiKouryoCntStr, "HiraKakuProN-W6", 48);
	m_byoYomiKouryoCntDispLabel->setPosition(Vec2(m_byoYomiKouryoCntSettingButtonItem->getPositionX(),
			m_byoYomiKouryoCntSettingButtonItem->getContentSize().height/2));
	m_byoYomiKouryoCntDispLabel->setColor(m_buttonFontColor);
	m_byoYomiKouryoCntDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_byoYomiKouryoCntSettingButtonItem->addChild(m_byoYomiKouryoCntDispLabel, 150);

	m_byoYomiKouryoCntUnitLabel = Label::createWithSystemFont("回", "HiraKakuProN-W6", 48);
	m_byoYomiKouryoCntUnitLabel->setPosition(Vec2(m_byoYomiKouryoCntSettingButtonItem->getContentSize().width - 50,
			m_byoYomiKouryoCntSettingButtonItem->getContentSize().height/2));
	m_byoYomiKouryoCntUnitLabel->setColor(m_buttonFontColor);
	m_byoYomiKouryoCntUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_byoYomiKouryoCntSettingButtonItem->addChild(m_byoYomiKouryoCntUnitLabel, 150);

	this->addChild(m_byoYomiKouryoCntSettingButtonItem, 1);

	// 秒読み考慮回数マイナス
	m_byoYomiKouryoCntMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(ByoYomiKouryoCntSettingScene::byoYomiKouryoCntMinusButtonDidPushed, this));

	m_byoYomiKouryoCntMinusButton = Menu::create(m_byoYomiKouryoCntMinusButtonItem, NULL);
	m_byoYomiKouryoCntMinusButton->setPosition( Vec2(m_winSize.width-200, m_byoYomiKouryoCntSettingButtonItem->getPositionY()) );

    this->addChild( m_byoYomiKouryoCntMinusButton , 200);

    // 秒読み考慮回数プラス
    m_byoYomiKouryoCntPlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(ByoYomiKouryoCntSettingScene::byoYomiKouryoCntPlusButtonDidPushed, this));

    m_byoYomiKouryoCntPlusButton = Menu::create(m_byoYomiKouryoCntPlusButtonItem, NULL);
    m_byoYomiKouryoCntPlusButton->setPosition( Vec2(m_winSize.width-100, m_byoYomiKouryoCntSettingButtonItem->getPositionY()) );

    this->addChild( m_byoYomiKouryoCntPlusButton , 200);

    return true;
}

// button action
void ByoYomiKouryoCntSettingScene::backButtonDidPushed(Ref* pSender)
{
    backSettingScene();
}

void ByoYomiKouryoCntSettingScene::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event * event)
{
    if (keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
    {
    	backSettingScene();
    }
}

void ByoYomiKouryoCntSettingScene::backSettingScene() {

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    UserDefault::getInstance()->setIntegerForKey("byo_yomi_kouryo_cnt1", m_byo_yomi_kouryo_cnt1);
    UserDefault::getInstance()->setIntegerForKey("byo_yomi_kouryo_cnt2", m_byo_yomi_kouryo_cnt2);

    Scene *nextScene = CCTransitionSlideInL::create(0.1f, ByoYomiSettingScene::scene());
    Director::getInstance()->replaceScene(nextScene);
}

void ByoYomiKouryoCntSettingScene::byoYomiKouryoCntPlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char byoYomiKouryoCntStr[5];
	sprintf(byoYomiKouryoCntStr, "%d", byoYomiKouryoCntIncrement(m_player));
	m_byoYomiKouryoCntDispLabel->setString(byoYomiKouryoCntStr);
}

void ByoYomiKouryoCntSettingScene::byoYomiKouryoCntMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char byoYomiKouryoCntStr[5];
	sprintf(byoYomiKouryoCntStr, "%d", byoYomiKouryoCntDecrement(m_player));
	m_byoYomiKouryoCntDispLabel->setString(byoYomiKouryoCntStr);
}

int ByoYomiKouryoCntSettingScene::byoYomiKouryoCntIncrement(int player) {
	int byo_yomi_kouryo_cnt;
    if(player == 0) {
    	if(m_byo_yomi_kouryo_cnt1 < m_byoYomiKouryoCntMaxLimit) {
    		m_byo_yomi_kouryo_cnt1++;
    		byo_yomi_kouryo_cnt = m_byo_yomi_kouryo_cnt1;
    	} else {
    		m_byo_yomi_kouryo_cnt1 = m_byoYomiKouryoCntMinLimit;
    		byo_yomi_kouryo_cnt = m_byo_yomi_kouryo_cnt1;
    	}
    } else {
    	if(m_byo_yomi_kouryo_cnt2 < m_byoYomiKouryoCntMaxLimit) {
    		m_byo_yomi_kouryo_cnt2++;
    		byo_yomi_kouryo_cnt = m_byo_yomi_kouryo_cnt2;
    	} else {
    		m_byo_yomi_kouryo_cnt2 = m_byoYomiKouryoCntMinLimit;
    		byo_yomi_kouryo_cnt = m_byo_yomi_kouryo_cnt2;
    	}
    }
    return byo_yomi_kouryo_cnt;
}

int ByoYomiKouryoCntSettingScene::byoYomiKouryoCntDecrement(int player) {
	int byo_yomi_kouryo_cnt;
    if(player == 0) {
    	if(m_byo_yomi_kouryo_cnt1 > m_byoYomiKouryoCntMinLimit) {
    		m_byo_yomi_kouryo_cnt1--;
    		byo_yomi_kouryo_cnt = m_byo_yomi_kouryo_cnt1;
    	} else {
    		m_byo_yomi_kouryo_cnt1 = m_byoYomiKouryoCntMaxLimit;
    		byo_yomi_kouryo_cnt = m_byo_yomi_kouryo_cnt1;
    	}
    } else {
    	if(m_byo_yomi_kouryo_cnt2 > m_byoYomiKouryoCntMinLimit) {
    		m_byo_yomi_kouryo_cnt2--;
    		byo_yomi_kouryo_cnt = m_byo_yomi_kouryo_cnt2;
    	} else {
    		m_byo_yomi_kouryo_cnt2 = m_byoYomiKouryoCntMaxLimit;
    		byo_yomi_kouryo_cnt = m_byo_yomi_kouryo_cnt2;
    	}
    }
    return byo_yomi_kouryo_cnt;
}
