/*
 * ByoYomiTimeSetting.h
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#ifndef _XIANQQI_MOTI_JIKAN_SETTING_H_
#define _XIANQQI_MOTI_JIKAN_SETTING_H_

#include "cocos2d.h"
#include "DialogLayer.h"
#include "TimeSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class XianqqiMotiJikanSettingScene : public TimeSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(XianqqiMotiJikanSettingScene);

    Size m_winSize;

    // 設定値
    int m_xianqqi_moti_jikan2_1;							// 持ち時間２（プレイヤー１）
    int m_xianqqi_moti_jikan2_2;							// 持ち時間２（プレイヤー２）
    int m_xianqqiMotiJikanMaxLimit = 60;					// 持ち時間最大値
    int m_xianqqiMotiJikanMinLimit = 1;						// 持ち時間最小値

    Sprite *m_xianqqiMotiJikanSettingButtonItem;
    Menu *m_xianqqiMotiJikanSettingButton;

    MenuItemImage *m_xianqqiMotiJikanMinusButtonItem;
    Menu *m_xianqqiMotiJikanMinusButton;

    MenuItemImage *m_xianqqiMotiJikanPlusButtonItem;
    Menu *m_xianqqiMotiJikanPlusButton;

    Label *m_xianqqiMotiJikanDispLabel;
    Label *m_xianqqiMotiJikanUnitLabel;

    // 増減ボタン
    ControlStepper *m_xianqqiMotiJikanInputButton;	// 考慮時間入力ボタン

    MenuItemImage *m_backButtonItem;
    Menu *m_backButton;

    // button action
    void backButtonDidPushed(Ref* pSender);
    void xianqqiMotiJikanPlusButtonDidPushed(Ref* pSender);
    void xianqqiMotiJikanMinusButtonDidPushed(Ref* pSender);

private:
    int xianqqiMotiJikanIncrement(int player);
    int xianqqiMotiJikanDecrement(int player);
};


#endif
