
#define COCOS2D_DEBUG 1

#include "Setting/XianqqiSettingScene.h"
#include "TimeSetting/XianqqiKiteiCntSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

Scene* XianqqiKiteiCntSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();

    // 'layer' is an autorelease object
    XianqqiKiteiCntSettingScene *layer = XianqqiKiteiCntSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance


bool XianqqiKiteiCntSettingScene::init()
{

	loadCommonSetting();

	m_winSize = Director::getInstance()->getWinSize();

    // 規定回数
    m_xianqqi_kitei_cnt1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_cnt1", 10);
    m_xianqqi_kitei_cnt2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_cnt2", 10);

    Label *settingTitle = Label::createWithSystemFont("規定回数設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    m_settingTitleLayer->addChild(settingTitle, 1);

    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(XianqqiKiteiCntSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, m_settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width + 200,
    							m_backButtonItem->getContentSize().height));
    m_settingTitleLayer->addChild(m_backButton, 100);

    this->addChild( m_settingTitleLayer , 160);

	Label *xianqqiKiteiCntLabel = Label::createWithSystemFont("規定回数", "HiraKakuProN-W6", 55);
	xianqqiKiteiCntLabel->setPosition(Vec2(50, m_settingTitleLayer->getPositionY()-80));
	xianqqiKiteiCntLabel->setColor(Color3B(68, 68, 68));
	xianqqiKiteiCntLabel->setAnchorPoint(Vec2(0.0f, 0.5f));

	this->addChild(xianqqiKiteiCntLabel, 1);

	// 規定回数パネル
    m_xianqqiKiteiCntSettingButtonItem = Sprite::create("res/setting_menu_small.png");
    m_xianqqiKiteiCntSettingButtonItem->setPosition( Vec2(40, xianqqiKiteiCntLabel->getPositionY()-90) );
    m_xianqqiKiteiCntSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	// 規定回数
	int xianqqi_kitei_cnt;
    if(m_player == 0) {
    	xianqqi_kitei_cnt = m_xianqqi_kitei_cnt1;
    } else {
    	xianqqi_kitei_cnt = m_xianqqi_kitei_cnt2;
    }
	char xianqqiKiteiCntStr[5];
	sprintf(xianqqiKiteiCntStr, "%d", xianqqi_kitei_cnt);

	m_xianqqiKiteiCntDispLabel = Label::createWithSystemFont(xianqqiKiteiCntStr, "HiraKakuProN-W6", 48);
	m_xianqqiKiteiCntDispLabel->setPosition(Vec2(m_xianqqiKiteiCntSettingButtonItem->getPositionX(),
			m_xianqqiKiteiCntSettingButtonItem->getContentSize().height/2));
	m_xianqqiKiteiCntDispLabel->setColor(m_buttonFontColor);
	m_xianqqiKiteiCntDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_xianqqiKiteiCntSettingButtonItem->addChild(m_xianqqiKiteiCntDispLabel, 150);

	m_xianqqiKiteiCntUnitLabel = Label::createWithSystemFont("回", "HiraKakuProN-W6", 48);
	m_xianqqiKiteiCntUnitLabel->setPosition(Vec2(m_xianqqiKiteiCntSettingButtonItem->getContentSize().width - 50,
			m_xianqqiKiteiCntSettingButtonItem->getContentSize().height/2));
	m_xianqqiKiteiCntUnitLabel->setColor(m_buttonFontColor);
	m_xianqqiKiteiCntUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_xianqqiKiteiCntSettingButtonItem->addChild(m_xianqqiKiteiCntUnitLabel, 150);

	this->addChild(m_xianqqiKiteiCntSettingButtonItem, 1);

	// 規定回数マイナス
	m_xianqqiKiteiCntMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(XianqqiKiteiCntSettingScene::xianqqiKiteiCntMinusButtonDidPushed, this));

	m_xianqqiKiteiCntMinusButton = Menu::create(m_xianqqiKiteiCntMinusButtonItem, NULL);
	m_xianqqiKiteiCntMinusButton->setPosition( Vec2(m_winSize.width-200, m_xianqqiKiteiCntSettingButtonItem->getPositionY()) );

    this->addChild( m_xianqqiKiteiCntMinusButton , 200);

    // 規定回数プラス
    m_xianqqiKiteiCntPlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(XianqqiKiteiCntSettingScene::xianqqiKiteiCntPlusButtonDidPushed, this));

    m_xianqqiKiteiCntPlusButton = Menu::create(m_xianqqiKiteiCntPlusButtonItem, NULL);
    m_xianqqiKiteiCntPlusButton->setPosition( Vec2(m_winSize.width-100, m_xianqqiKiteiCntSettingButtonItem->getPositionY()) );

    this->addChild( m_xianqqiKiteiCntPlusButton , 200);

    return true;
}

// button action
void XianqqiKiteiCntSettingScene::backButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    UserDefault::getInstance()->setIntegerForKey("xianqqi_kitei_cnt1", m_xianqqi_kitei_cnt1);
    UserDefault::getInstance()->setIntegerForKey("xianqqi_kitei_cnt2", m_xianqqi_kitei_cnt2);

    Scene *nextScene = CCTransitionSlideInL::create(0.1f, XianqqiSettingScene::scene());
    Director::getInstance()->replaceScene(nextScene);
}

void XianqqiKiteiCntSettingScene::xianqqiKiteiCntPlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char xianqqiKiteiCntStr[5];
	sprintf(xianqqiKiteiCntStr, "%d", xianqqiKiteiCntIncrement(m_player));
	m_xianqqiKiteiCntDispLabel->setString(xianqqiKiteiCntStr);
}

void XianqqiKiteiCntSettingScene::xianqqiKiteiCntMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char xianqqiKiteiCntStr[5];
	sprintf(xianqqiKiteiCntStr, "%d", xianqqiKiteiCntDecrement(m_player));
	m_xianqqiKiteiCntDispLabel->setString(xianqqiKiteiCntStr);
}

int XianqqiKiteiCntSettingScene::xianqqiKiteiCntIncrement(int player) {
	int xianqqi_kitei_cnt;
    if(player == 0) {
    	if(m_xianqqi_kitei_cnt1 < m_xianqqiKiteiCntMaxLimit) {
    		m_xianqqi_kitei_cnt1++;
    		xianqqi_kitei_cnt = m_xianqqi_kitei_cnt1;
    	} else {
    		m_xianqqi_kitei_cnt1 = m_xianqqiKiteiCntMinLimit;
    		xianqqi_kitei_cnt = m_xianqqi_kitei_cnt1;
    	}
    } else {
    	if(m_xianqqi_kitei_cnt2 < m_xianqqiKiteiCntMaxLimit) {
    		m_xianqqi_kitei_cnt2++;
    		xianqqi_kitei_cnt = m_xianqqi_kitei_cnt2;
    	} else {
    		m_xianqqi_kitei_cnt2 = m_xianqqiKiteiCntMinLimit;
    		xianqqi_kitei_cnt = m_xianqqi_kitei_cnt2;
    	}
    }
    return xianqqi_kitei_cnt;
}

int XianqqiKiteiCntSettingScene::xianqqiKiteiCntDecrement(int player) {
	int xianqqi_kitei_cnt;
    if(player == 0) {
    	if(m_xianqqi_kitei_cnt1 > m_xianqqiKiteiCntMinLimit) {
    		m_xianqqi_kitei_cnt1--;
    		xianqqi_kitei_cnt = m_xianqqi_kitei_cnt1;
    	} else {
    		m_xianqqi_kitei_cnt1 = m_xianqqiKiteiCntMaxLimit;
    		xianqqi_kitei_cnt = m_xianqqi_kitei_cnt1;
    	}
    } else {
    	if(m_xianqqi_kitei_cnt2 > m_xianqqiKiteiCntMinLimit) {
    		m_xianqqi_kitei_cnt2--;
    		xianqqi_kitei_cnt = m_xianqqi_kitei_cnt2;
    	} else {
    		m_xianqqi_kitei_cnt2 = m_xianqqiKiteiCntMaxLimit;
    		xianqqi_kitei_cnt = m_xianqqi_kitei_cnt2;
    	}
    }
    return xianqqi_kitei_cnt;
}
