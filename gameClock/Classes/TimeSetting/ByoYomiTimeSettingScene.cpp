/*
 * ByoYomiTimeSetteing.cpp
 *
 *  Created on: 2014/10/23
 *      Author: ryuji_muraoka
 */

#define COCOS2D_DEBUG 1

#include "Setting/ByoYomiSettingScene.h"
#include "TimeSetting/ByoYomiTimeSettingScene.h"
#include "GameClockScene.h"
#include "CCPageControl.h"
#include "CCSlidingLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

Scene* ByoYomiTimeSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();

    // 'layer' is an autorelease object
    ByoYomiTimeSettingScene *layer = ByoYomiTimeSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance


bool ByoYomiTimeSettingScene::init()
{

	loadCommonSetting();

    m_winSize = Director::getInstance()->getWinSize();

    // 秒読み時間
    m_byo_yomi_time1 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_time1", 30);
    m_byo_yomi_time2 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_time2", 30);

    Label *settingTitle = Label::createWithSystemFont("秒読み時間設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    settingTitle->setColor(m_titleFontColor);
    settingTitle->setAnchorPoint(Vec2(0.5f, 0.5f));
    m_settingTitleLayer->addChild(settingTitle, 1);

    // 戻るボタン
    m_backButtonItem = MenuItemImage::create(
			 "res/back_btn.png",
			 "res/back_btn_hilighted.png",
			 CC_CALLBACK_1(ByoYomiTimeSettingScene::backButtonDidPushed, this));

    m_backButton = Menu::create(m_backButtonItem, NULL);
    m_backButton->setPosition( Vec2(50, m_settingTitleLayer->getContentSize().height / 2) );
    m_backButton->setContentSize(Size(m_backButtonItem->getContentSize().width + 200,
    							m_backButtonItem->getContentSize().height));
    m_settingTitleLayer->addChild(m_backButton, 100);

    // 秒読み時間ラベル
	Label *byoYomiTimeLabel = Label::createWithSystemFont("秒読み時間", "HiraKakuProN-W6", 55);
	byoYomiTimeLabel->setPosition(Vec2(50, m_settingTitleLayer->getPositionY()-80));
	byoYomiTimeLabel->setColor(m_subjectFontColor);
	byoYomiTimeLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	this->addChild(byoYomiTimeLabel, 150);

	// 秒読み時間
    m_byoYomiTimeSettingButtonItem = Sprite::create("res/setting_menu_small.png");
    m_byoYomiTimeSettingButtonItem->setPosition( Vec2(40, byoYomiTimeLabel->getPositionY()-90) );
    m_byoYomiTimeSettingButtonItem->setAnchorPoint(Vec2(0.0f, 0.5f));

	int byo_yomi_time;
    if(m_player == 0) {
    	byo_yomi_time = m_byo_yomi_time1;
    } else {
    	byo_yomi_time = m_byo_yomi_time2;
    }
	char byoYomiTimeStr[5];
	sprintf(byoYomiTimeStr, "%d", byo_yomi_time);

	// 秒読み時間表示
	m_byoYomiTimeDispLabel = Label::createWithSystemFont(byoYomiTimeStr, "HiraKakuProN-W6", 48);
	m_byoYomiTimeDispLabel->setPosition(Vec2(m_byoYomiTimeSettingButtonItem->getPositionX(),
    		m_byoYomiTimeSettingButtonItem->getContentSize().height/2));
	m_byoYomiTimeDispLabel->setColor(m_buttonFontColor);
	m_byoYomiTimeDispLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
    m_byoYomiTimeSettingButtonItem->addChild(m_byoYomiTimeDispLabel, 150);

    // 秒読み時間単位
	m_byoYomiTimeUnitLabel = Label::createWithSystemFont("秒", "HiraKakuProN-W6", 48);
	m_byoYomiTimeUnitLabel->setPosition(Vec2(m_byoYomiTimeSettingButtonItem->getContentSize().width - 50,
    		m_byoYomiTimeSettingButtonItem->getContentSize().height/2));
	m_byoYomiTimeUnitLabel->setColor(m_buttonFontColor);
	m_byoYomiTimeUnitLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
    m_byoYomiTimeSettingButtonItem->addChild(m_byoYomiTimeUnitLabel, 150);

	this->addChild(m_byoYomiTimeSettingButtonItem, 1);

	// 秒読み時間マイナス
	m_byoYomiMinusButtonItem = MenuItemImage::create(
			 "res/time_minus_button.png",
			 "res/time_minus_button.png",
			 CC_CALLBACK_1(ByoYomiTimeSettingScene::byoYomiTimeMinusButtonDidPushed, this));

	m_byoYomiMinusButton = Menu::create(m_byoYomiMinusButtonItem, NULL);
	m_byoYomiMinusButton->setPosition( Vec2(m_winSize.width-200, m_byoYomiTimeSettingButtonItem->getPositionY()) );

    this->addChild( m_byoYomiMinusButton , 200);

    // 秒読み時間プラス
    m_byoYomiPlusButtonItem = MenuItemImage::create(
			 "res/time_plus_button.png",
			 "res/time_plus_button.png",
			 CC_CALLBACK_1(ByoYomiTimeSettingScene::byoYomiTimePlusButtonDidPushed, this));

    m_byoYomiPlusButton = Menu::create(m_byoYomiPlusButtonItem, NULL);
    m_byoYomiPlusButton->setPosition( Vec2(m_winSize.width-100, m_byoYomiTimeSettingButtonItem->getPositionY()) );

    this->addChild( m_byoYomiPlusButton , 200);

    return true;
}

// button action
void ByoYomiTimeSettingScene::backButtonDidPushed(Ref* pSender)
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/change_scene.mp3");

    UserDefault::getInstance()->setIntegerForKey("byo_yomi_time1", m_byo_yomi_time1);
    UserDefault::getInstance()->setIntegerForKey("byo_yomi_time2", m_byo_yomi_time2);

    Scene *nextScene = CCTransitionSlideInL::create(0.1f, ByoYomiSettingScene::scene());
    Director::getInstance()->replaceScene(nextScene);
}

void ByoYomiTimeSettingScene::byoYomiTimePlusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char byoYomiTimeStr[5];
	sprintf(byoYomiTimeStr, "%d", byoYomiTimeIncrement(m_player));
	m_byoYomiTimeDispLabel->setString(byoYomiTimeStr);
}

void ByoYomiTimeSettingScene::byoYomiTimeMinusButtonDidPushed(Ref* pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/select.mp3");
	char byoYomiTimeStr[5];
	sprintf(byoYomiTimeStr, "%d", byoYomiTimeDecrement(m_player));
	m_byoYomiTimeDispLabel->setString(byoYomiTimeStr);
}

int ByoYomiTimeSettingScene::byoYomiTimeIncrement(int player) {
	int byo_yomi_time;
    if(player == 0) {
    	if(m_byo_yomi_time1 < m_byoYomiTimeMaxLimit) {
    		m_byo_yomi_time1++;
    		byo_yomi_time = m_byo_yomi_time1;
    	} else {
    		m_byo_yomi_time1 = m_byoYomiTimeMinLimit;
    		byo_yomi_time = m_byo_yomi_time1;
    	}
    } else {
    	if(m_byo_yomi_time2 < m_byoYomiTimeMaxLimit) {
    		m_byo_yomi_time2++;
    		byo_yomi_time = m_byo_yomi_time2;
    	} else {
    		m_byo_yomi_time2 = m_byoYomiTimeMinLimit;
    		byo_yomi_time = m_byo_yomi_time2;
    	}
    }
    return byo_yomi_time;
}

int ByoYomiTimeSettingScene::byoYomiTimeDecrement(int player) {
	int byo_yomi_time;
    if(player == 0) {
    	if(m_byo_yomi_time1 > m_byoYomiTimeMinLimit) {
    		m_byo_yomi_time1--;
    		byo_yomi_time = m_byo_yomi_time1;
    	} else {
    		m_byo_yomi_time1 = m_byoYomiTimeMaxLimit;
    		byo_yomi_time = m_byo_yomi_time1;
    	}
    } else {
    	if(m_byo_yomi_time2 > m_byoYomiTimeMinLimit) {
    		m_byo_yomi_time2--;
    		byo_yomi_time = m_byo_yomi_time2;
    	} else {
    		m_byo_yomi_time2 = m_byoYomiTimeMaxLimit;
    		byo_yomi_time = m_byo_yomi_time2;
    	}
    }
    return byo_yomi_time;
}
