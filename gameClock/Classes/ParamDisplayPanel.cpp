
#include "ParamDisplayPanel.h"

using namespace cocos2d;

bool ParamDisplayPanel::init() {

    if ( !Layer::init() )
    {
        return false;
    }

    m_backGroundLayer = Sprite::create("res/time_disp_panel_base.png");
    m_backGroundLayer->setPosition(Vec2(0, 0));
    m_backGroundLayer->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    this->addChild( m_backGroundLayer , 110);

    m_headlineLayer = Sprite::create("res/time_disp_panel_title.png");
    m_headlineLayer->setPosition(Vec2(0, m_backGroundLayer->getContentSize().height -
    		m_headlineLayer->getContentSize().height));
    m_headlineLayer->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    this->addChild( m_headlineLayer , 160);

    m_layerWidth = m_backGroundLayer->getSpriteFrame()->getRect().size.width;

	return true;
}

void ParamDisplayPanel::setTitle(const std::string& title)
{
	Size winSize = Director::getInstance()->getVisibleSize();

    // タイトル
	m_titleLabel = Label::createWithSystemFont(title, "HiraKakuProN-W6", 18);
	m_titleLabel->setColor(Color3B(255, 255, 255));
	m_titleLabel->setPosition(Vec2(m_headlineLayer->getContentSize().width/2,
    							m_headlineLayer->getContentSize().height/2));

    m_headlineLayer->addChild(m_titleLabel, 170);
}

void ParamDisplayPanel::setValue(int value)
{
	Size winSize = Director::getInstance()->getVisibleSize();

	char valueStr[15];
	sprintf(valueStr, "%02d", value);

    // 時間
	m_timeLabel = Label::createWithSystemFont(valueStr, "HiraKakuProN-W6", 48);
	m_timeLabel->setColor(Color3B(98, 98, 98));
	m_timeLabel->setPosition(Vec2(m_backGroundLayer->getContentSize().width/2,
    		(m_backGroundLayer->getContentSize().height-m_headlineLayer->getContentSize().height)/2));

    m_backGroundLayer->addChild(m_timeLabel, 170);
}

void ParamDisplayPanel::setUnit(const std::string& unit)
{
	Size winSize = Director::getInstance()->getVisibleSize();

    // 単位
	m_unitLabel = Label::createWithSystemFont(unit, "HiraKakuProN-W6", 15);
	m_unitLabel->setColor(Color3B(98, 98, 98));
	m_unitLabel->setPosition(Vec2(m_backGroundLayer->getContentSize().width - 20, 20));

    m_backGroundLayer->addChild(m_unitLabel, 170);
}
