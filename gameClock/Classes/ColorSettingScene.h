
#include "cocos2d.h"
#include "DialogLayer.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class ColorSettingScene : public cocos2d::LayerGradient {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(ColorSettingScene);
    
    Size m_winSize;

    int m_rule;								// ルール

    Color3B m_grayFont;

    Color4B m_startColorRed;				// 赤
    Color4B m_endColorRed;
    Color4B m_startColorBlue;				// 青
    Color4B m_endColorBlue;
    Color4B m_startColorGreen;				// 緑
    Color4B m_endColorGreen;
    Color4B m_startColorOrange;				// オレンジ（フォント：グレー）
    Color4B m_endColorOrange;

    Color4B m_startColorDeepPink;			// ディープピンク
    Color4B m_endColorDeepPink;
    Color4B m_startColorLightSeaGreen;		// ライトシーグリーン
    Color4B m_endColorLightSeaGreen;
    Color4B m_startColorOliveGreen;			// ダークオリーブグリーン
    Color4B m_endColorOliveGreen;
    Color4B m_startColorYellow;				// 黄
    Color4B m_endColorYellow;

    Color4B m_startColorHotPink;			// ホットピンク
    Color4B m_endColorHotPink;
    Color4B m_startColorSkyBlue;			// スカイブルー（フォント：グレー）
    Color4B m_endColorSkyBlue;
    Color4B m_startColorPaleGreen;			// パールグリーン
    Color4B m_endColorPaleGreen;
    Color4B m_startColorKhaki;				// カーキ
    Color4B m_endColorKhaki;

    Color4B m_startColorMediumViolet;		// ミディアムバイオレット
    Color4B m_endColorMediumViolet;
    Color4B m_startColorPurple;				// パープル
    Color4B m_endColorPurple;
    Color4B m_startColorDarkGreen;			// 深緑
    Color4B m_endColorDarkGreen;
    Color4B m_startColorBrown;				// 茶色
    Color4B m_endColorBrown;

    Color4B m_startColorMaroon;				// マルーン
    Color4B m_endColorMaroon;
    Color4B m_startColorNavy;				// ネイビー
    Color4B m_endColorNavy;
    Color4B m_startColorSilver;				// シルバー
    Color4B m_endColorSilver;
    Color4B m_startColorBlack;				// ブラック
    Color4B m_endColorBlack;
    
    Color4B m_colorTurqoise;				// ターコイズ
    Color4B m_colorEmerald;					// エメラルド
    Color4B m_colorPeterRiver;				// ピーター・リバー
    Color4B m_colorAmethyst;				// アメジスト
    Color4B m_colorWetAspalt;				// ウェット・アスファルト
    Color4B m_colorGreenSea;				// グリーン・シー
    Color4B m_colorNephritis;				// ネフリシス
    Color4B m_colorBelizeHole;				// ベリーズホール
    Color4B m_colorWisteria;				// ウィステリア
    Color4B m_colorMidnightBlue;			// ミッドナイト・ブルー
    Color4B m_colorSunFlower;				// サンフラワー
    Color4B m_colorCarrot;					// キャロット
    Color4B m_colorAlizarin;				// アリザリン
    Color4B m_colorClouds;					// クラウド
    Color4B m_colorConcrete;				// コンクリート
    Color4B m_colorOrange;					// オレンジ
    Color4B m_colorPumpkin;					// パンプキン
    Color4B m_colorPomegranate;				// ポームグラネート
    Color4B m_colorSilver;					// シルバー
    Color4B m_colorAsbestos;				// アスベストス

    LayerGradient *m_color1Layer;
    LayerGradient *m_color2Layer;
    LayerGradient *m_color3Layer;
    LayerGradient *m_color4Layer;
    LayerGradient *m_color5Layer;
    LayerGradient *m_color6Layer;
    LayerGradient *m_color7Layer;
    LayerGradient *m_color8Layer;
    LayerGradient *m_color9Layer;
    LayerGradient *m_color10Layer;
    LayerGradient *m_color11Layer;
    LayerGradient *m_color12Layer;
    LayerGradient *m_color13Layer;
    LayerGradient *m_color14Layer;
    LayerGradient *m_color15Layer;
    LayerGradient *m_color16Layer;
    LayerGradient *m_color17Layer;
    LayerGradient *m_color18Layer;
    LayerGradient *m_color19Layer;
    LayerGradient *m_color20Layer;

    MenuItemImage *m_color1Item;
    MenuItemImage *m_color2Item;
    MenuItemImage *m_color3Item;
    MenuItemImage *m_color4Item;
    MenuItemImage *m_color5Item;
    MenuItemImage *m_color6Item;
    MenuItemImage *m_color7Item;
    MenuItemImage *m_color8Item;
    MenuItemImage *m_color9Item;
    MenuItemImage *m_color10Item;
    MenuItemImage *m_color11Item;
    MenuItemImage *m_color12Item;
    MenuItemImage *m_color13Item;
    MenuItemImage *m_color14Item;
    MenuItemImage *m_color15Item;
    MenuItemImage *m_color16Item;
    MenuItemImage *m_color17Item;
    MenuItemImage *m_color18Item;
    MenuItemImage *m_color19Item;
    MenuItemImage *m_color20Item;

    Menu *m_color1ItemButton;
    Menu *m_color2ItemButton;
    Menu *m_color3ItemButton;
    Menu *m_color4ItemButton;
    Menu *m_color5ItemButton;
    Menu *m_color6ItemButton;
    Menu *m_color7ItemButton;
    Menu *m_color8ItemButton;
    Menu *m_color9ItemButton;
    Menu *m_color10ItemButton;
    Menu *m_color11ItemButton;
    Menu *m_color12ItemButton;
    Menu *m_color13ItemButton;
    Menu *m_color14ItemButton;
    Menu *m_color15ItemButton;
    Menu *m_color16ItemButton;
    Menu *m_color17ItemButton;
    Menu *m_color18ItemButton;
    Menu *m_color19ItemButton;
    Menu *m_color20ItemButton;

    Label *m_selectLabel1_1;
    Label *m_selectLabel1_2;
    Label *m_selectLabel1_3;
    Label *m_selectLabel1_4;
    Label *m_selectLabel1_5;
    Label *m_selectLabel1_6;
    Label *m_selectLabel1_7;
    Label *m_selectLabel1_8;
    Label *m_selectLabel1_9;
    Label *m_selectLabel1_10;
    Label *m_selectLabel1_11;
    Label *m_selectLabel1_12;
    Label *m_selectLabel1_13;
    Label *m_selectLabel1_14;
    Label *m_selectLabel1_15;
    Label *m_selectLabel1_16;
    Label *m_selectLabel1_17;
    Label *m_selectLabel1_18;
    Label *m_selectLabel1_19;
    Label *m_selectLabel1_20;

    Label *m_selectLabel2_1;
    Label *m_selectLabel2_2;
    Label *m_selectLabel2_3;
    Label *m_selectLabel2_4;
    Label *m_selectLabel2_5;
    Label *m_selectLabel2_6;
    Label *m_selectLabel2_7;
    Label *m_selectLabel2_8;
    Label *m_selectLabel2_9;
    Label *m_selectLabel2_10;
    Label *m_selectLabel2_11;
    Label *m_selectLabel2_12;
    Label *m_selectLabel2_13;
    Label *m_selectLabel2_14;
    Label *m_selectLabel2_15;
    Label *m_selectLabel2_16;
    Label *m_selectLabel2_17;
    Label *m_selectLabel2_18;
    Label *m_selectLabel2_19;
    Label *m_selectLabel2_20;

    MenuItemImage *m_settingBtnItem;
    Menu *m_settingButton;

    MenuItemImage *m_backButtonItem;
    Menu *m_backButton;

    int m_color1;										// 背景色（プレイヤー１）
    int m_color2;										// 背景色（プレイヤー２）

    // 設定値
    void colorSelected(Ref * pSender);
    void color1Selected(Ref * pSender);
    void color2Selected(Ref * pSender);
    void color3Selected(Ref * pSender);
    void color4Selected(Ref * pSender);
    void color5Selected(Ref * pSender);
    void color6Selected(Ref * pSender);
    void backButtonDidPushed(Ref* pSender);
};


