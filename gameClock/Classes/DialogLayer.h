
#ifndef __Modal__ModalScene__
#define __Modal__ModalScene__

#include "cocos2d.h"
#include "cocos-ext.h"

using namespace cocos2d;

class DialogLayer : public cocos2d::LayerGradient
{
public:
    virtual bool init();
    void menuCloseCallback(Ref* pSender);
    virtual bool onTouchBegan(cocos2d::Touch *pTouch, cocos2d::Event *pEvent);
    CREATE_FUNC(DialogLayer);

//    LayerColor *m_settingLayerbg;
//    LayerGradient *m_settingTitleLayer;
    Sprite *m_backGroundLayer;
    Sprite *m_headlineLayer;

    // 設定値
    int m_time1;
    int m_time2;
    int m_coler1;
    int m_coler2;
    int m_rule;

    char m_title[50];

    Menu *m_cancelButton;
    Menu *m_positiveButton;

    std::string *m_dialogKind;

    //void registerWithTouchDispatcher();
    void setTitle(const char* title, __String *dialogKind);
    void setTitlePosition(__String *dialogKind);
    void cancelButtonDidPushed(Ref* pSender);
    void positiveButtonDidPushed(Ref* pSender);
};

#endif /* defined(__Modal__ModalScene__) */
