#ifndef __ScrollMenuView_H__
#define __ScrollMenuView_H__

#include <cocos2d.h>
#include "ScrollMenu.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;

class ScrollMenuView : public  cocos2d::ui::ScrollView {
private:
    // タッチ開始時の座標
    Vec2 _touchStartPoint;

protected:
    // コンストラクタ
    ScrollMenuView();
    // オブジェクト作成時実行処理
    bool init();
    bool init(ScrollMenu* menu);

public:
    // オブジェクト作成用function
    static ScrollMenuView* create();

    static ScrollMenuView* create(ScrollMenu* menu);

    // オーバーライドするタッチ開始イベント
    bool onTouchBegan(Touch *touch, Event *unusedEvent);
    // オーバーライドするタッチ終了イベント
    void onTouchMoved(Touch *touch, Event *unusedEvent);
};

#endif
