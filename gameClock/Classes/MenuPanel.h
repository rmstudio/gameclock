#include "cocos2d.h"

USING_NS_CC;

class MenuPanel : public Menu
{
public:
	MenuPanel(){}
	virtual ~MenuPanel(){}
	static MenuPanel* create(MenuItem* item, ...);
	static MenuPanel* createWithArray(Vector<MenuItem*> pArrayOfItems);
	static MenuPanel* createWithItems(MenuItem* item, va_list args);
	static MenuPanel* create();
};
