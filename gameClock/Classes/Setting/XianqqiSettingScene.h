#ifndef __XianqqiSettingScene_h__
#define __XianqqiSettingScene_h__

#include "cocos2d.h"
#include "DialogLayer.h"
#include "cocos-ext.h"
#include "CCSlidingLayer.h"
#include "BaseSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class XianqqiSettingScene : public BaseSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(XianqqiSettingScene);
    
    float m_start_y;
    float m_end_y;

    // display
    void showRuleTitle();
    void showRuleMenu();
    void showTime1Title();
    void showTime2Title();
    void showColorTitle();
    void showColor1Menu();
    void showColor2Menu();

    void showXianqqiTime1Menu();
    void showXianqqiTime2Menu();

    void xianqqiKiteiTekazu1_1ButtonDidPushed(Ref* pSender);
    void xianqqiKiteiTekazu1_2ButtonDidPushed(Ref* pSender);
    void xianqqiMotiJikan1ButtonDidPushed(Ref* pSender);
    void xianqqiMotiJikan2ButtonDidPushed(Ref* pSender);
    void xianqqiKiteiTekazu2_1ButtonDidPushed(Ref* pSender);
    void xianqqiKiteiTekazu2_2ButtonDidPushed(Ref* pSender);
    void xianqqiKiteiCnt1ButtonDidPushed(Ref* pSender);
    void xianqqiKiteiCnt2ButtonDidPushed(Ref* pSender);
    void xianqqiByoYomi1ButtonDidPushed(Ref* pSender);
    void xianqqiByoYomi2ButtonDidPushed(Ref* pSender);

};

#endif
