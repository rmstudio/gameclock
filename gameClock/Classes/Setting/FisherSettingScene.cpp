#define COCOS2D_DEBUG 1

#include "FisherSettingScene.h"
#include "TimeSetting/TimeSettingScene.h"
#include "TimeSetting/FisherAddTimeSettingScene.h"
#include "RuleSettingScene.h"
#include "ColorSettingScene.h"
#include "GameClockScene.h"
#include "cocos-ext.h"
#include "CCPageControl.h"
#include "MenuPanel.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

enum BUTTON {
    BUTTON_TIME1   = 0,
    BUTTON_TIME2  = 1,
    BUTTON_COLOR1  = 2,
    BUTTON_COLOR2  = 3,
    BUTTON_RULE  = 3,
};

enum RULE {
    RULE_KIREMAKE   = 0,
    RULE_BYOYOMI  = 1,
    RULE_FISHER  = 2,
    RULE_CANADA  = 3,
    RULE_XIANQQI  = 4,
};

enum PLAYER {
	PLAYER_SENTE = 0,
	PLAYER_GOTE = 1,
};

Scene* FisherSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    FisherSettingScene *layer = FisherSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool FisherSettingScene::init()
{

	loadCommonSetting();

    loadSaveParams();

    setBackGroundColor();

    // タイトルラベル
    m_headerBarLayer = LayerGradient::create(m_settingHeaderBgColorStart, m_settingHeaderBgColorEnd);
    m_headerBarLayer->setContentSize( Size(m_winSize.width, 120) );
    m_headerBarLayer->setPosition(Vec2(0, m_winSize.height-120));
    this->addChild( m_headerBarLayer , 160);

    Label *settingTitle = Label::createWithSystemFont("ゲーム設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    m_headerBarLayer->addChild(settingTitle, 1);

    float slidingLayerHeight;

    slidingLayerHeight = m_winSize.height - 50;

    m_scrollLayer = ScrollMenuView::create();
    m_scrollLayer->setContentSize(Size(m_winSize.width, m_winSize.height-m_headerBarHeight-m_footerBarHeight));
    m_scrollLayer->setInnerContainerSize(Size(m_winSize.width, slidingLayerHeight));
    m_scrollLayer->setPosition(Vec2(0, m_footerBarHeight));
    m_scrollLayer->setColor(Color3B(196, 229, 103));
    m_scrollLayer->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_scrollLayer->setBounceEnabled(true);
    this->addChild(m_scrollLayer, 90);

	showRuleTitle();

	showRuleMenu();

	showTime1Title();

	showFisherTime1Menu();

	showTime2Title();

	showFisherTime2Menu();

	showColorTitle();

	showColor1Menu();

	showColor2Menu();

    // ボタン背面
	m_footerBarLayer = LayerGradient::create(m_settingFooterBgColorStart, m_settingFooterBgColorEnd);
    m_footerBarLayer->setAnchorPoint(Point::ZERO);
    m_footerBarLayer->setPosition(Vec2(0, 0));
    m_footerBarLayer->setContentSize(Size(m_winSize.width, 200));
    this->addChild(m_footerBarLayer, 120);

    // スタートボタン
    m_startButtonItem = MenuItemImage::create(
												 "res/start_btn.png",
												 "res/start_btn.png",
												 CC_CALLBACK_1(FisherSettingScene::startButtonDidPushed, this));
    Size buttonSize = Size(5, 5);
    m_startButtonItem->setPosition( Vec2(m_winSize.width/2, 100) );
    m_startButton = Menu::create(m_startButtonItem, NULL);
    m_startButton->setPosition( Point::ZERO );
    m_footerBarLayer->addChild(m_startButton, 150);

    this->addChild(m_scrollLayer, 1);

	log("rule:%d", m_rule);

    return true;
}

void FisherSettingScene::showRuleTitle() {

	int scrollLayerHeight =  m_scrollLayer->getInnerContainerSize().height;

    // 見出し（ルール）
	m_ruleHeadLineLabel = Label::createWithSystemFont("ルール", "HiraKakuProN-W6", 55);
	m_ruleHeadLineLabel->setPosition(Vec2(50, scrollLayerHeight-80));
	m_ruleHeadLineLabel->setColor(m_subjectFontColor);
	m_ruleHeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_ruleHeadLineLabel, 150);

}

void FisherSettingScene::showRuleMenu() {

	int startPositionY = m_ruleHeadLineLabel->getPositionY()-90;

	// 設定値表示セル（ルール）
    m_ruleSettingButtonItem = MenuItemImage::create(
													 "res/setting_menu.png",
													 "res/setting_menu.png",
													 CC_CALLBACK_1(FisherSettingScene::ruleButtonDidPushed, this));
    m_ruleSettingButtonItem->setPosition(Vec2(m_winSize.width/2, startPositionY));

    // ボタン内部見出し（ルール）
	Label *ruleLabel = Label::createWithSystemFont("ルール", "HiraKakuProN-W6", 48);
	ruleLabel->setPosition(Vec2(30, m_ruleSettingButtonItem->getContentSize().height/2));
	ruleLabel->setColor(m_buttonFontColor);
	ruleLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_ruleSettingButtonItem->addChild(ruleLabel, 150);

	m_ruleSettingButtonItem->removeChild(m_ruleDispLabel, true);

	m_ruleDispLabel = Label::createWithSystemFont("フィッシャー", "HiraKakuProN-W6", 48);

	m_ruleDispLabel->setPosition(Vec2(m_winSize.width-150, m_ruleSettingButtonItem->getContentSize().height/2));
    m_ruleDispLabel->setColor(m_buttonFontColor);
    m_ruleDispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_ruleSettingButtonItem->addChild(m_ruleDispLabel, 150);

	m_ruleSettingButton = ScrollMenu::create(m_ruleSettingButtonItem, NULL);
	m_ruleSettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_ruleSettingButton, 1);

}

void FisherSettingScene::showTime1Title() {

	int startPositionY = m_ruleSettingButtonItem->getPositionY()-110;

    // 見出し（持ち時間（player1））
	m_time1HeadLineLabel = Label::createWithSystemFont("時間設定(先手)", "HiraKakuProN-W6", 55);
	m_time1HeadLineLabel->setPosition(Vec2(50, startPositionY));
	m_time1HeadLineLabel->setColor(m_subjectFontColor);
	m_time1HeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_time1HeadLineLabel, 150);

}

void FisherSettingScene::showTime1Menu() {

	int startPositionY = m_time1HeadLineLabel->getPositionY()-90;

	// 設定値表示セル（持ち時間(先手)）
	m_time1SettingButtonItem = MenuItemImage::create(
			"res/setting_menu_top.png",
			"res/setting_menu_top.png",
			CC_CALLBACK_1(FisherSettingScene::time1ButtonDidPushed, this));
	m_time1SettingButtonItem->setPosition(Vec2(m_winSize.width/2, startPositionY));

	Label *timeLabel1 = Label::createWithSystemFont("持ち時間", "HiraKakuProN-W6", 48);
	timeLabel1->setPosition(Vec2(30, m_time1SettingButtonItem->getContentSize().height/2));
	timeLabel1->setColor(m_buttonFontColor);
	timeLabel1->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_time1SettingButtonItem->addChild(timeLabel1, 150);

	char hour1Str[15];
	sprintf(hour1Str, "%d 時間", m_hour1);
	m_hour1DispLabel = Label::createWithSystemFont(hour1Str, "HiraKakuProN-W6", 48);
	m_hour1DispLabel->setPosition(Vec2(m_winSize.width-300, m_time1SettingButtonItem->getContentSize().height/2));
	m_hour1DispLabel->setColor(m_buttonFontColor);
	m_hour1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
    m_time1SettingButtonItem->addChild(m_hour1DispLabel, 150);

	char minite1Str[15];
	sprintf(minite1Str, "%d 分", m_minite1);
	m_minite1DispLabel = Label::createWithSystemFont(minite1Str, "HiraKakuProN-W6", 48);
	m_minite1DispLabel->setPosition(Vec2(m_winSize.width-130, m_time1SettingButtonItem->getContentSize().height/2));
	m_minite1DispLabel->setColor(m_buttonFontColor);
	m_minite1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
    m_time1SettingButtonItem->addChild(m_minite1DispLabel, 150);

	m_time1SettingButton = ScrollMenu::create(m_time1SettingButtonItem, NULL);
	m_time1SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_time1SettingButton, 1);

}

void FisherSettingScene::showFisherTime1Menu() {

	int startPositionY = m_time1HeadLineLabel->getPositionY()-90;

	// 設定値表示セル（持ち時間(先手)）
	m_time1SettingButtonItem = MenuItemImage::create(
			"res/setting_menu_top.png",
			"res/setting_menu_top.png",
			CC_CALLBACK_1(FisherSettingScene::time1ButtonDidPushed, this));
	m_time1SettingButtonItem->setPosition(Vec2(m_winSize.width/2, startPositionY));

	Label *timeLabel1 = Label::createWithSystemFont("持ち時間", "HiraKakuProN-W6", 48);
	timeLabel1->setPosition(Vec2(30, m_time1SettingButtonItem->getContentSize().height/2));
	timeLabel1->setColor(m_buttonFontColor);
	timeLabel1->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_time1SettingButtonItem->addChild(timeLabel1, 150);

	char hour1Str[15];
	sprintf(hour1Str, "%d 時間", m_hour1);
	m_hour1DispLabel = Label::createWithSystemFont(hour1Str, "HiraKakuProN-W6", 48);
	m_hour1DispLabel->setPosition(Vec2(m_winSize.width-300, m_time1SettingButtonItem->getContentSize().height/2));
	m_hour1DispLabel->setColor(m_buttonFontColor);
	m_hour1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_time1SettingButtonItem->addChild(m_hour1DispLabel, 150);

	char minite1Str[15];
	sprintf(minite1Str, "%d 分", m_minite1);
	m_minite1DispLabel = Label::createWithSystemFont(minite1Str, "HiraKakuProN-W6", 48);
	m_minite1DispLabel->setPosition(Vec2(m_winSize.width-130, m_time1SettingButtonItem->getContentSize().height/2));
	m_minite1DispLabel->setColor(m_buttonFontColor);
	m_minite1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_time1SettingButtonItem->addChild(m_minite1DispLabel, 150);

	m_time1SettingButton = ScrollMenu::create(m_time1SettingButtonItem, NULL);
	m_time1SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_time1SettingButton, 1);

	// 加算時間（先手）
	m_fisherAddTime1SettingButtonItem = MenuItemImage::create(
																 "res/setting_menu_bottom.png",
																 "res/setting_menu_bottom.png",
																 CC_CALLBACK_1(FisherSettingScene::fisherAddTime1ButtonDidPushed, this));
	m_fisherAddTime1SettingButtonItem->setPosition(Vec2(m_winSize.width/2, m_time1SettingButtonItem->getPositionY()-90));

	Label *fisherTime1Label = Label::createWithSystemFont("加算時間", "HiraKakuProN-W6", 48);
	fisherTime1Label->setPosition(Vec2(30, m_fisherAddTime1SettingButtonItem->getContentSize().height/2));
	fisherTime1Label->setColor(m_buttonFontColor);
	fisherTime1Label->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_fisherAddTime1SettingButtonItem->addChild(fisherTime1Label, 150);

	char fisherAdditionalTime1Str[15];
	sprintf(fisherAdditionalTime1Str, "%d 秒", m_fisher_additional_time1);
	Label *fisherAdditionalTime1DispLabel = Label::createWithSystemFont(fisherAdditionalTime1Str, "HiraKakuProN-W6", 48);
	fisherAdditionalTime1DispLabel->setPosition(Vec2(m_winSize.width-130, m_fisherAddTime1SettingButtonItem->getContentSize().height/2));
	fisherAdditionalTime1DispLabel->setColor(m_buttonFontColor);
	fisherAdditionalTime1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_fisherAddTime1SettingButtonItem->addChild(fisherAdditionalTime1DispLabel, 150);

	m_fisherAddTime1SettingButton = ScrollMenu::create(m_fisherAddTime1SettingButtonItem, NULL);
	m_fisherAddTime1SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_fisherAddTime1SettingButton, 1);

}

void FisherSettingScene::showTime2Title() {

	int startPositionY = m_fisherAddTime1SettingButtonItem->getPositionY()-110;

	// 持ち時間（player2）
	m_time2HeadLineLabel = Label::createWithSystemFont("時間設定(後手)", "HiraKakuProN-W6", 55);
	m_time2HeadLineLabel->setPosition(Vec2(50, startPositionY));
	m_time2HeadLineLabel->setColor(m_subjectFontColor);
	m_time2HeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_time2HeadLineLabel, 150);
}

void FisherSettingScene::showTime2Menu() {

	int startPositionY = m_time2HeadLineLabel->getPositionY()-90;

	m_time2SettingButtonItem = MenuItemImage::create(
													 "res/setting_menu_top.png",
													 "res/setting_menu_top.png",
													 CC_CALLBACK_1(FisherSettingScene::time2ButtonDidPushed, this));
	m_time2SettingButtonItem->setPosition( Vec2(m_winSize.width/2, startPositionY) );

	Label *timeLabel2 = Label::createWithSystemFont("持ち時間", "HiraKakuProN-W6", 48);
	timeLabel2->setPosition(Vec2(30, m_time2SettingButtonItem->getContentSize().height/2));
	timeLabel2->setColor(m_buttonFontColor);
	timeLabel2->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_time2SettingButtonItem->addChild(timeLabel2, 150);

	char hour2Str[15];
	sprintf(hour2Str, "%d 時間", m_hour2);
	m_hour2DispLabel = Label::createWithSystemFont(hour2Str, "HiraKakuProN-W6", 48);
	m_hour2DispLabel->setPosition(Vec2(m_winSize.width-300, m_time1SettingButtonItem->getContentSize().height/2));
	m_hour2DispLabel->setColor(m_buttonFontColor);
	m_hour2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
    m_time2SettingButtonItem->addChild(m_hour2DispLabel, 150);

	char minite2Str[15];
	sprintf(minite2Str, "%d 分", m_minite2);
	m_minite2DispLabel = Label::createWithSystemFont(minite2Str, "HiraKakuProN-W6", 48);
	m_minite2DispLabel->setPosition(Vec2(m_winSize.width-130, m_time2SettingButtonItem->getContentSize().height/2));
	m_minite2DispLabel->setColor(m_buttonFontColor);
	m_minite2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_time2SettingButtonItem->addChild(m_minite2DispLabel, 150);

	m_time2SettingButton = ScrollMenu::create(m_time2SettingButtonItem, NULL);
	m_time2SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_time2SettingButton, 1);

}

void FisherSettingScene::showFisherTime2Menu() {

	int startPositionY = m_time2HeadLineLabel->getPositionY()-90;

	// 設定値表示セル（持ち時間(後手)）
	m_time2SettingButtonItem = MenuItemImage::create(
			"res/setting_menu_top.png",
			"res/setting_menu_top.png",
			CC_CALLBACK_1(FisherSettingScene::time2ButtonDidPushed, this));
	m_time2SettingButtonItem->setPosition(Vec2(m_winSize.width/2, startPositionY));

	Label *timeLabel2 = Label::createWithSystemFont("持ち時間", "HiraKakuProN-W6", 48);
	timeLabel2->setPosition(Vec2(30, m_time2SettingButtonItem->getContentSize().height/2));
	timeLabel2->setColor(m_buttonFontColor);
	timeLabel2->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_time2SettingButtonItem->addChild(timeLabel2, 150);

	char hour2Str[15];
	sprintf(hour2Str, "%d 時間", m_hour2);
	m_hour2DispLabel = Label::createWithSystemFont(hour2Str, "HiraKakuProN-W6", 48);
	m_hour2DispLabel->setPosition(Vec2(m_winSize.width-300, m_time2SettingButtonItem->getContentSize().height/2));
	m_hour2DispLabel->setColor(m_buttonFontColor);
	m_hour2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_time2SettingButtonItem->addChild(m_hour2DispLabel, 150);

	char minite2Str[15];
	sprintf(minite2Str, "%d 分", m_minite2);
	m_minite2DispLabel = Label::createWithSystemFont(minite2Str, "HiraKakuProN-W6", 48);
	m_minite2DispLabel->setPosition(Vec2(m_winSize.width-130, m_time2SettingButtonItem->getContentSize().height/2));
	m_minite2DispLabel->setColor(m_buttonFontColor);
	m_minite2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_time2SettingButtonItem->addChild(m_minite2DispLabel, 150);

	m_time2SettingButton = ScrollMenu::create(m_time2SettingButtonItem, NULL);
	m_time2SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_time2SettingButton, 1);

	// 加算時間（後手）
	m_fisherAddTime2SettingButtonItem = MenuItemImage::create(
																 "res/setting_menu_bottom.png",
																 "res/setting_menu_bottom.png",
																 CC_CALLBACK_1(FisherSettingScene::fisherAddTime2ButtonDidPushed, this));
	m_fisherAddTime2SettingButtonItem->setPosition(Vec2(m_winSize.width/2, m_time2SettingButtonItem->getPositionY()-90));

	Label *fisherTime2Label = Label::createWithSystemFont("加算時間", "HiraKakuProN-W6", 48);
	fisherTime2Label->setPosition(Vec2(30, m_fisherAddTime2SettingButtonItem->getContentSize().height/2));
	fisherTime2Label->setColor(m_buttonFontColor);
	fisherTime2Label->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_fisherAddTime2SettingButtonItem->addChild(fisherTime2Label, 150);

	char fisherAdditionalTime2Str[15];
	sprintf(fisherAdditionalTime2Str, "%d 秒", m_fisher_additional_time2);
	Label *fisherAdditionalTime2DispLabel = Label::createWithSystemFont(fisherAdditionalTime2Str, "HiraKakuProN-W6", 48);
	fisherAdditionalTime2DispLabel->setPosition(Vec2(m_winSize.width-130, m_fisherAddTime2SettingButtonItem->getContentSize().height/2));
	fisherAdditionalTime2DispLabel->setColor(m_buttonFontColor);
	fisherAdditionalTime2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_fisherAddTime2SettingButtonItem->addChild(fisherAdditionalTime2DispLabel, 150);

	m_fisherAddTime2SettingButton = ScrollMenu::create(m_fisherAddTime2SettingButtonItem, NULL);
	m_fisherAddTime2SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_fisherAddTime2SettingButton, 1);

}

void FisherSettingScene::showColorTitle() {

	int startPositionY = m_fisherAddTime2SettingButtonItem->getPositionY() - 110;

	log("color title startPositionY:%d", startPositionY);

	// 背景色（player1）
	m_colorHeadLineLabel = Label::createWithSystemFont("背景色設定", "HiraKakuProN-W6", 55);
	m_colorHeadLineLabel->setPosition(Vec2(50, startPositionY));
	m_colorHeadLineLabel->setColor(m_subjectFontColor);
	m_colorHeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_colorHeadLineLabel, 150);
}

void FisherSettingScene::showColor1Menu() {

	int startPositionY =  m_colorHeadLineLabel->getPositionY()-90;

	m_color1SettingButtonItem = MenuItemImage::create(
													  "res/setting_menu_top.png",
													  "res/setting_menu_top.png",
													  CC_CALLBACK_1(FisherSettingScene::color1ButtonDidPushed, this));
	m_color1SettingButtonItem->setPosition( Vec2(m_winSize.width/2, startPositionY) );

	Label *colorLabel1 = Label::createWithSystemFont("背景色(先手)", "HiraKakuProN-W6", 48);
	colorLabel1->setPosition(Vec2(30, m_time2SettingButtonItem->getContentSize().height/2));
	colorLabel1->setColor(m_buttonFontColor);
	colorLabel1->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_color1SettingButtonItem->addChild(colorLabel1, 150);

	LayerGradient *baseLayer1 = LayerGradient::create(m_colorLabelBorderColor, m_colorLabelBorderColor);
	baseLayer1->setContentSize(Size(70, 70));
	baseLayer1->setAnchorPoint(Vec2(1.0f, 0.5f));
	baseLayer1->setPosition(Vec2(m_winSize.width-200, 13));

	m_color1Layer = LayerGradient::create(m_startcolor1, m_endcolor1);
	m_color1Layer->setContentSize(Size(68, 68));
	m_color1Layer->setAnchorPoint(Vec2(0.0f, 0.0f));
	m_color1Layer->setPosition(Vec2(1, 1));
	baseLayer1->addChild(m_color1Layer, 10);

	m_color1SettingButtonItem->addChild(baseLayer1, 10);

	m_color1SettingButton = ScrollMenu::create(m_color1SettingButtonItem, NULL);
	m_color1SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_color1SettingButton, 1);
}

void FisherSettingScene::showColor2Menu() {

	int startPositionY =  m_color1SettingButtonItem->getPositionY()-95;

	// 背景色（player2）
	m_color2SettingButtonItem = MenuItemImage::create(
														"res/setting_menu_bottom.png",
														"res/setting_menu_bottom.png",
														CC_CALLBACK_1(FisherSettingScene::color2ButtonDidPushed, this));
	m_color2SettingButtonItem->setPosition( Vec2(m_winSize.width/2, startPositionY) );

	Label *colorLabel2 = Label::createWithSystemFont("背景色(後手)", "HiraKakuProN-W6", 48);
	colorLabel2->setPosition(Vec2(30, m_time2SettingButtonItem->getContentSize().height/2));
	colorLabel2->setColor(m_buttonFontColor);
	colorLabel2->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_color2SettingButtonItem->addChild(colorLabel2, 150);

	LayerGradient *baseLayer2 = LayerGradient::create(m_colorLabelBorderColor, m_colorLabelBorderColor);
	baseLayer2->setContentSize(Size(70, 70));
	baseLayer2->setAnchorPoint(Vec2(1.0f, 0.5f));
	baseLayer2->setPosition(Vec2(m_winSize.width-200, 13));

	m_color2Layer = LayerGradient::create(m_startcolor2, m_endcolor2);
	m_color2Layer->setContentSize(Size(68, 68));
	m_color2Layer->setAnchorPoint(Vec2(0.0f, 0.0f));
	m_color2Layer->setPosition(Vec2(1, 1));
	baseLayer2->addChild(m_color2Layer, 10);

	m_color2SettingButtonItem->addChild(baseLayer2, 10);

	m_color2SettingButton = ScrollMenu::create(m_color2SettingButtonItem, NULL);
	m_color2SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_color2SettingButton, 1);
}

void FisherSettingScene::fisherAddTime1ButtonDidPushed(Ref* pSender)
{
	if(!flicking) {
		UserDefault::getInstance()->setIntegerForKey("player", 0);
		Scene *nextScene = CCTransitionSlideInR::create(0.1f, FisherAddTimeSettingScene::scene());
		Director::getInstance()->replaceScene(nextScene);
	}
}

void FisherSettingScene::fisherAddTime2ButtonDidPushed(Ref* pSender)
{
	if(!flicking) {
		UserDefault::getInstance()->setIntegerForKey("player", 1);
		Scene *nextScene = CCTransitionSlideInR::create(0.1f, FisherAddTimeSettingScene::scene());
		Director::getInstance()->replaceScene(nextScene);
	}
}
