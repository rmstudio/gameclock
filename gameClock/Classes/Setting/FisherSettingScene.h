#ifndef __FisherSettingScene_h__
#define __FisherSettingScene_h__

#include "cocos2d.h"
#include "DialogLayer.h"
#include "cocos-ext.h"
#include "CCSlidingLayer.h"
#include "BaseSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class FisherSettingScene : public BaseSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(FisherSettingScene);

    // display
    void showRuleTitle();
    void showRuleMenu();
    void showTime1Title();
    void showTime1Menu();
    void showTime2Title();
    void showTime2Menu();
    void showColorTitle();
    void showColor1Menu();
    void showColor2Menu();

    void showFisherTime1Menu();
    void showFisherTime2Menu();

    // button aciton
    void fisherAddTime1ButtonDidPushed(Ref* pSender);
    void fisherAddTime2ButtonDidPushed(Ref* pSender);

private:

};

#endif
