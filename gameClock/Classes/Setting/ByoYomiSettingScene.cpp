#define COCOS2D_DEBUG 1

#include "ByoYomiSettingScene.h"
#include "TimeSetting/ByoYomiTimeSettingScene.h"
#include "TimeSetting/ByoYomiKouryoCntSettingScene.h"
#include "RuleSettingScene.h"
#include "ColorSettingScene.h"
#include "GameClockScene.h"
#include "cocos-ext.h"
#include "CCPageControl.h"
#include "MenuPanel.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

enum BUTTON {
    BUTTON_TIME1   = 0,
    BUTTON_TIME2  = 1,
    BUTTON_COLOR1  = 2,
    BUTTON_COLOR2  = 3,
    BUTTON_RULE  = 3,
};

enum RULE {
    RULE_KIREMAKE   = 0,
    RULE_BYOYOMI  = 1,
    RULE_FISHER  = 2,
    RULE_CANADA  = 3,
    RULE_XIANQQI  = 4,
};

enum PLAYER {
	PLAYER_SENTE = 0,
	PLAYER_GOTE = 1,
};

Scene* ByoYomiSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    ByoYomiSettingScene *layer = ByoYomiSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool ByoYomiSettingScene::init()
{

	loadCommonSetting();

    loadSaveParams();

    setBackGroundColor();

    // タイトルラベル
    m_headerBarLayer = LayerGradient::create(m_settingHeaderBgColorStart, m_settingHeaderBgColorEnd);
    m_headerBarLayer->setContentSize( Size(m_winSize.width, m_headerBarHeight) );
    m_headerBarLayer->setPosition(Vec2(0, m_winSize.height-120));
    this->addChild( m_headerBarLayer , 160);

    Label *settingTitle = Label::createWithSystemFont("ゲーム設定", "HiraKakuProN-W6", 64);
    settingTitle->setPosition( Vec2(m_winSize.width / 2, 60) );
    m_headerBarLayer->addChild(settingTitle, 1);

    float slidingLayerHeight = m_winSize.height + 100;

    m_scrollLayer = ScrollMenuView::create();
    m_scrollLayer->setContentSize(Size(m_winSize.width, m_winSize.height-m_headerBarHeight-m_footerBarHeight));
    m_scrollLayer->setInnerContainerSize(Size(m_winSize.width, slidingLayerHeight));
    m_scrollLayer->setPosition(Vec2(0, m_footerBarHeight));
    m_scrollLayer->setColor(Color3B(196, 229, 103));
    m_scrollLayer->setDirection(ui::ScrollView::Direction::VERTICAL);
    m_scrollLayer->setBounceEnabled(true);
    this->addChild(m_scrollLayer, 90);

	showRuleTitle();

	showRuleMenu();

	showTime1Title();

	showByoYomiTime1Menu();

	showTime2Title();

	showByoYomiTime2Menu();

	showColorTitle();

	showColor1Menu();

	showColor2Menu();

    // ボタン背面
	m_footerBarLayer = LayerGradient::create(m_settingFooterBgColorStart, m_settingFooterBgColorEnd);
	m_footerBarLayer->setAnchorPoint(Point::ZERO);
    m_footerBarLayer->setPosition(Vec2(0, 0));
    m_footerBarLayer->setContentSize(Size(m_winSize.width, 200));
    this->addChild(m_footerBarLayer, 120);

    // スタートボタン
    m_startButtonItem = MenuItemImage::create(
												 "res/start_btn.png",
												 "res/start_btn.png",
												 CC_CALLBACK_1(ByoYomiSettingScene::startButtonDidPushed, this));
    Size buttonSize = Size(5, 5);
    m_startButtonItem->setPosition( Vec2(m_winSize.width/2, 100) );
    m_startButton = Menu::create(m_startButtonItem, NULL);
    m_startButton->setPosition( Point::ZERO );
    m_footerBarLayer->addChild(m_startButton, 150);

	log("rule:%d", m_rule);

    return true;
}

void ByoYomiSettingScene::showRuleTitle() {

	int scrollLayerHeight =  m_scrollLayer->getInnerContainerSize().height;

    // 見出し（ルール）
	m_ruleHeadLineLabel = Label::createWithSystemFont("ルール", "HiraKakuProN-W6", 55);
	m_ruleHeadLineLabel->setPosition(Vec2(50, scrollLayerHeight-80));
	m_ruleHeadLineLabel->setColor(m_subjectFontColor);
	m_ruleHeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_ruleHeadLineLabel, 150);

}

void ByoYomiSettingScene::showRuleMenu() {

	int startPositionY = m_ruleHeadLineLabel->getPositionY()-90;

	// 設定値表示セル（ルール）
    m_ruleSettingButtonItem = MenuItemImage::create(
													 "res/setting_menu.png",
													 "res/setting_menu.png",
													 CC_CALLBACK_1(ByoYomiSettingScene::ruleButtonDidPushed, this));
    m_ruleSettingButtonItem->setPosition(Vec2(m_winSize.width/2, startPositionY));

    // ボタン内部見出し（ルール）
	Label *ruleLabel = Label::createWithSystemFont("ルール", "HiraKakuProN-W6", 48);
	ruleLabel->setPosition(Vec2(30, m_ruleSettingButtonItem->getContentSize().height/2));
	ruleLabel->setColor(m_buttonFontColor);
	ruleLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_ruleSettingButtonItem->addChild(ruleLabel, 150);

	m_ruleSettingButtonItem->removeChild(m_ruleDispLabel, true);

    // ボタン内部設定値（ルール）
	m_ruleDispLabel = Label::createWithSystemFont("秒読み", "HiraKakuProN-W6", 48);

	m_ruleDispLabel->setPosition(Vec2(m_winSize.width-150, m_ruleSettingButtonItem->getContentSize().height/2));
    m_ruleDispLabel->setColor(m_buttonFontColor);
    m_ruleDispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_ruleSettingButtonItem->addChild(m_ruleDispLabel, 150);

	m_ruleSettingButton = ScrollMenu::create(m_ruleSettingButtonItem, NULL);
	m_ruleSettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_ruleSettingButton, 1);

}

void ByoYomiSettingScene::showTime1Title() {

	int startPositionY = m_ruleSettingButtonItem->getPositionY()-110;

    // 見出し（持ち時間（player1））
	m_time1HeadLineLabel = Label::createWithSystemFont("時間設定(先手)", "HiraKakuProN-W6", 55);
	m_time1HeadLineLabel->setPosition(Vec2(50, startPositionY));
	m_time1HeadLineLabel->setColor(m_subjectFontColor);
	m_time1HeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_time1HeadLineLabel, 150);

}

void ByoYomiSettingScene::showByoYomiTime1Menu() {

	int startPositionY = m_time1HeadLineLabel->getPositionY()-90;

	// 設定値表示セル（持ち時間(先手)）
	m_time1SettingButtonItem = MenuItemImage::create(
			"res/setting_menu_top.png",
			"res/setting_menu_top.png",
			CC_CALLBACK_1(ByoYomiSettingScene::time1ButtonDidPushed, this));
	m_time1SettingButtonItem->setPosition(Vec2(m_winSize.width/2, startPositionY));

	Label *timeLabel1 = Label::createWithSystemFont("持ち時間", "HiraKakuProN-W6", 48);
	timeLabel1->setPosition(Vec2(30, m_time1SettingButtonItem->getContentSize().height/2));
	timeLabel1->setColor(m_buttonFontColor);
	timeLabel1->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_time1SettingButtonItem->addChild(timeLabel1, 150);

	char hour1Str[15];
	sprintf(hour1Str, "%d 時間", m_hour1);
	m_hour1DispLabel = Label::createWithSystemFont(hour1Str, "HiraKakuProN-W6", 48);
	m_hour1DispLabel->setPosition(Vec2(m_winSize.width-300, m_time1SettingButtonItem->getContentSize().height/2));
	m_hour1DispLabel->setColor(m_buttonFontColor);
	m_hour1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
    m_time1SettingButtonItem->addChild(m_hour1DispLabel, 150);

	char minite1Str[15];
	sprintf(minite1Str, "%d 分", m_minite1);
	m_minite1DispLabel = Label::createWithSystemFont(minite1Str, "HiraKakuProN-W6", 48);
	m_minite1DispLabel->setPosition(Vec2(m_winSize.width-130, m_time1SettingButtonItem->getContentSize().height/2));
	m_minite1DispLabel->setColor(m_buttonFontColor);
	m_minite1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
    m_time1SettingButtonItem->addChild(m_minite1DispLabel, 150);

	m_time1SettingButton = ScrollMenu::create(m_time1SettingButtonItem, NULL);
	m_time1SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_time1SettingButton, 1);

	// 秒読み時間（先手）
	m_byoYomiTime1SettingButtonItem = MenuItemImage::create(
																 "res/setting_menu_middle.png",
																 "res/setting_menu_middle.png",
																 CC_CALLBACK_1(ByoYomiSettingScene::byoYomiTime1ButtonDidPushed, this));
	m_byoYomiTime1SettingButtonItem->setPosition(Vec2(m_winSize.width/2, m_time1SettingButtonItem->getPositionY()-90));

	Label *byouYomiTime1Label = Label::createWithSystemFont("秒読み", "HiraKakuProN-W6", 48);
	byouYomiTime1Label->setPosition(Vec2(30, m_byoYomiTime1SettingButtonItem->getContentSize().height/2));
	byouYomiTime1Label->setColor(m_buttonFontColor);
	byouYomiTime1Label->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_byoYomiTime1SettingButtonItem->addChild(byouYomiTime1Label, 150);

	char byoYomiTime1Str[15];
	sprintf(byoYomiTime1Str, "%d 秒", m_byo_yomi_time1);
	Label *byouYomiTime1DispLabel = Label::createWithSystemFont(byoYomiTime1Str, "HiraKakuProN-W6", 48);
	byouYomiTime1DispLabel->setPosition(Vec2(m_winSize.width-130, m_byoYomiTime1SettingButtonItem->getContentSize().height/2));
	byouYomiTime1DispLabel->setColor(m_buttonFontColor);
	byouYomiTime1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_byoYomiTime1SettingButtonItem->addChild(byouYomiTime1DispLabel, 150);

	m_byouYomiTime1SettingButton = ScrollMenu::create(m_byoYomiTime1SettingButtonItem, NULL);
	m_byouYomiTime1SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_byouYomiTime1SettingButton, 1);

	// 考慮回数（先手）
	m_byoYomiKouryoCnt1SettingButtonItem = MenuItemImage::create(
																 "res/setting_menu_middle.png",
																 "res/setting_menu_middle.png",
																 CC_CALLBACK_1(ByoYomiSettingScene::byoYomiKouryoCnt1ButtonDidPushed, this));
	m_byoYomiKouryoCnt1SettingButtonItem->setPosition(Vec2(m_winSize.width/2, m_byoYomiTime1SettingButtonItem->getPositionY()-90));

	Label *kouryoCnt1Label = Label::createWithSystemFont("考慮回数", "HiraKakuProN-W6", 48);
	kouryoCnt1Label->setPosition(Vec2(30, m_byoYomiKouryoCnt1SettingButtonItem->getContentSize().height/2));
	kouryoCnt1Label->setColor(m_buttonFontColor);
	kouryoCnt1Label->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_byoYomiKouryoCnt1SettingButtonItem->addChild(kouryoCnt1Label, 150);

	char byoYomiKouryoCnt1Str[15];
	sprintf(byoYomiKouryoCnt1Str, "%d 回", m_byo_yomi_kouryo_cnt_current1);
	Label *byouYomiKouryoCnt1DispLabel = Label::createWithSystemFont(byoYomiKouryoCnt1Str, "HiraKakuProN-W6", 48);
	byouYomiKouryoCnt1DispLabel->setPosition(Vec2(m_winSize.width-130, m_byoYomiTime1SettingButtonItem->getContentSize().height/2));
	byouYomiKouryoCnt1DispLabel->setColor(m_buttonFontColor);
	byouYomiKouryoCnt1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_byoYomiKouryoCnt1SettingButtonItem->addChild(byouYomiKouryoCnt1DispLabel, 150);

	m_byoYomiKouryoCnt1SettingButton = ScrollMenu::create(m_byoYomiKouryoCnt1SettingButtonItem, NULL);
	m_byoYomiKouryoCnt1SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_byoYomiKouryoCnt1SettingButton, 1);

}

void ByoYomiSettingScene::showTime2Title() {

	int startPositionY = m_byoYomiKouryoCnt1SettingButtonItem->getPositionY()-110;

	// 持ち時間（player2）
	m_time2HeadLineLabel = Label::createWithSystemFont("時間設定(後手)", "HiraKakuProN-W6", 55);
	m_time2HeadLineLabel->setPosition(Vec2(50, startPositionY));
	m_time2HeadLineLabel->setColor(m_subjectFontColor);
	m_time2HeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_time2HeadLineLabel, 150);
}


void ByoYomiSettingScene::showByoYomiTime2Menu() {

	int startPositionY = m_time2HeadLineLabel->getPositionY()-90;

	// 持ち時間（player2）
	m_time2SettingButtonItem = MenuItemImage::create(
			"res/setting_menu_top.png",
			"res/setting_menu_top.png",
			CC_CALLBACK_1(ByoYomiSettingScene::time2ButtonDidPushed, this));
	m_time2SettingButtonItem->setPosition( Vec2(m_winSize.width/2, startPositionY) );

	Label *timeLabel2 = Label::createWithSystemFont("持ち時間", "HiraKakuProN-W6", 48);
	timeLabel2->setPosition(Vec2(30, m_time2SettingButtonItem->getContentSize().height/2));
	timeLabel2->setColor(m_buttonFontColor);
	timeLabel2->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_time2SettingButtonItem->addChild(timeLabel2, 150);

	char hour2Str[15];
	sprintf(hour2Str, "%d 時間", m_hour2);
	m_hour2DispLabel = Label::createWithSystemFont(hour2Str, "HiraKakuProN-W6", 48);
	m_hour2DispLabel->setPosition(Vec2(m_winSize.width-300, m_time2SettingButtonItem->getContentSize().height/2));
	m_hour2DispLabel->setColor(m_buttonFontColor);
	m_hour2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
    m_time2SettingButtonItem->addChild(m_hour2DispLabel, 150);

	char minite2Str[15];
	sprintf(minite2Str, "%d 分", m_minite2);
	m_minite2DispLabel = Label::createWithSystemFont(minite2Str, "HiraKakuProN-W6", 48);
	m_minite2DispLabel->setPosition(Vec2(m_winSize.width-130, m_time2SettingButtonItem->getContentSize().height/2));
	m_minite2DispLabel->setColor(m_buttonFontColor);
	m_minite2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_time2SettingButtonItem->addChild(m_minite2DispLabel, 150);

	m_time2SettingButton = ScrollMenu::create(m_time2SettingButtonItem, NULL);
	m_time2SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_time2SettingButton, 1);

	// 秒読み時間（後手）
	m_byoYomiTime2SettingButtonItem = MenuItemImage::create(
																 "res/setting_menu_middle.png",
																 "res/setting_menu_middle.png",
																 CC_CALLBACK_1(ByoYomiSettingScene::byoYomiTime2ButtonDidPushed, this));
	m_byoYomiTime2SettingButtonItem->setPosition(Vec2(m_winSize.width/2, m_time2SettingButtonItem->getPositionY()-90));

	Label *byouYomiTime2Label = Label::createWithSystemFont("秒読み", "HiraKakuProN-W6", 48);
	byouYomiTime2Label->setPosition(Vec2(30, m_byoYomiTime2SettingButtonItem->getContentSize().height/2));
	byouYomiTime2Label->setColor(m_buttonFontColor);
	byouYomiTime2Label->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_byoYomiTime2SettingButtonItem->addChild(byouYomiTime2Label, 150);

	char byoYomiTime2Str[15];
	sprintf(byoYomiTime2Str, "%d 秒", m_byo_yomi_time2);
	Label *byouYomiTime2DispLabel = Label::createWithSystemFont(byoYomiTime2Str, "HiraKakuProN-W6", 48);
	byouYomiTime2DispLabel->setPosition(Vec2(m_winSize.width-130, m_byoYomiTime1SettingButtonItem->getContentSize().height/2));
	byouYomiTime2DispLabel->setColor(m_buttonFontColor);
	byouYomiTime2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_byoYomiTime2SettingButtonItem->addChild(byouYomiTime2DispLabel, 150);

	m_byouYomiTime2SettingButton = ScrollMenu::create(m_byoYomiTime2SettingButtonItem, NULL);
	m_byouYomiTime2SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_byouYomiTime2SettingButton, 1);

	// 考慮回数（後手）
	m_byoYomiKouryoCnt2SettingButtonItem = MenuItemImage::create(
																 "res/setting_menu_middle.png",
																 "res/setting_menu_middle.png",
																 CC_CALLBACK_1(ByoYomiSettingScene::byoYomiKouryoCnt2ButtonDidPushed, this));
	m_byoYomiKouryoCnt2SettingButtonItem->setPosition(Vec2(m_winSize.width/2, m_byoYomiTime2SettingButtonItem->getPositionY()-90));

	Label *kouryoCnt2Label = Label::createWithSystemFont("考慮回数", "HiraKakuProN-W6", 48);
	kouryoCnt2Label->setPosition(Vec2(30, m_byoYomiKouryoCnt2SettingButtonItem->getContentSize().height/2));
	kouryoCnt2Label->setColor(m_buttonFontColor);
	kouryoCnt2Label->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_byoYomiKouryoCnt2SettingButtonItem->addChild(kouryoCnt2Label, 150);

	char byoYomiKouryoCnt2Str[15];
	sprintf(byoYomiKouryoCnt2Str, "%d 回", m_byo_yomi_kouryo_cnt_current2);
	Label *byouYomiKouryoCnt2DispLabel = Label::createWithSystemFont(byoYomiKouryoCnt2Str, "HiraKakuProN-W6", 48);
	byouYomiKouryoCnt2DispLabel->setPosition(Vec2(m_winSize.width-130, m_byoYomiTime1SettingButtonItem->getContentSize().height/2));
	byouYomiKouryoCnt2DispLabel->setColor(m_buttonFontColor);
	byouYomiKouryoCnt2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_byoYomiKouryoCnt2SettingButtonItem->addChild(byouYomiKouryoCnt2DispLabel, 150);

	m_byoYomiKouryoCnt2SettingButton = ScrollMenu::create(m_byoYomiKouryoCnt2SettingButtonItem, NULL);
	m_byoYomiKouryoCnt2SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_byoYomiKouryoCnt2SettingButton, 1);

}

void ByoYomiSettingScene::showColorTitle() {

	int startPositionY = m_byoYomiKouryoCnt2SettingButtonItem->getPositionY()-110;

	log("color title startPositionY:%d", startPositionY);

	// 背景色（player1）
	m_colorHeadLineLabel = Label::createWithSystemFont("背景色設定", "HiraKakuProN-W6", 55);
	m_colorHeadLineLabel->setPosition(Vec2(50, startPositionY));
	m_colorHeadLineLabel->setColor(m_subjectFontColor);
	m_colorHeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_colorHeadLineLabel, 150);
}

void ByoYomiSettingScene::showColor1Menu() {

	int startPositionY =  m_colorHeadLineLabel->getPositionY()-90;

	m_color1SettingButtonItem = MenuItemImage::create(
													  "res/setting_menu_top.png",
													  "res/setting_menu_top.png",
													  CC_CALLBACK_1(ByoYomiSettingScene::color1ButtonDidPushed, this));
	m_color1SettingButtonItem->setPosition( Vec2(m_winSize.width/2, startPositionY) );

	Label *colorLabel1 = Label::createWithSystemFont("背景色(先手)", "HiraKakuProN-W6", 48);
	colorLabel1->setPosition(Vec2(30, m_time2SettingButtonItem->getContentSize().height/2));
	colorLabel1->setColor(m_buttonFontColor);
	colorLabel1->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_color1SettingButtonItem->addChild(colorLabel1, 150);

	LayerGradient *baseLayer1 = LayerGradient::create(m_colorLabelBorderColor, m_colorLabelBorderColor);
	baseLayer1->setContentSize(Size(70, 70));
	baseLayer1->setAnchorPoint(Vec2(1.0f, 0.5f));
	baseLayer1->setPosition(Vec2(m_winSize.width-200, 13));

	m_color1Layer = LayerGradient::create(m_startcolor1, m_endcolor1);
	m_color1Layer->setContentSize(Size(68, 68));
	m_color1Layer->setAnchorPoint(Vec2(0.0f, 0.0f));
	m_color1Layer->setPosition(Vec2(1, 1));
	baseLayer1->addChild(m_color1Layer, 10);

	m_color1SettingButtonItem->addChild(baseLayer1, 10);

	m_color1SettingButton = ScrollMenu::create(m_color1SettingButtonItem, NULL);
	m_color1SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_color1SettingButton, 1);
}

void ByoYomiSettingScene::showColor2Menu() {

	int startPositionY =  m_color1SettingButtonItem->getPositionY()-95;

	// 背景色（player2）
	m_color2SettingButtonItem = MenuItemImage::create(
														"res/setting_menu_bottom.png",
														"res/setting_menu_bottom.png",
														CC_CALLBACK_1(ByoYomiSettingScene::color2ButtonDidPushed, this));
	m_color2SettingButtonItem->setPosition( Vec2(m_winSize.width/2, startPositionY) );

	Label *colorLabel2 = Label::createWithSystemFont("背景色(後手)", "HiraKakuProN-W6", 48);
	colorLabel2->setPosition(Vec2(30, m_time2SettingButtonItem->getContentSize().height/2));
	colorLabel2->setColor(m_buttonFontColor);
	colorLabel2->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_color2SettingButtonItem->addChild(colorLabel2, 150);

	LayerGradient *baseLayer2 = LayerGradient::create(m_colorLabelBorderColor, m_colorLabelBorderColor);
	baseLayer2->setContentSize(Size(70, 70));
	baseLayer2->setAnchorPoint(Vec2(1.0f, 0.5f));
	baseLayer2->setPosition(Vec2(m_winSize.width-200, 13));

	m_color2Layer = LayerGradient::create(m_startcolor2, m_endcolor2);
	m_color2Layer->setContentSize(Size(68, 68));
	m_color2Layer->setAnchorPoint(Vec2(0.0f, 0.0f));
	m_color2Layer->setPosition(Vec2(1, 1));
	baseLayer2->addChild(m_color2Layer, 10);

	m_color2SettingButtonItem->addChild(baseLayer2, 10);

	m_color2SettingButton = ScrollMenu::create(m_color2SettingButtonItem, NULL);
	m_color2SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_color2SettingButton, 1);
}

void ByoYomiSettingScene::byoYomiTime1ButtonDidPushed(Ref* pSender)
{
	if(!flicking) {
		UserDefault::getInstance()->setIntegerForKey("player", 0);
		Scene *nextScene = CCTransitionSlideInR::create(0.1f, ByoYomiTimeSettingScene::scene());
		Director::getInstance()->replaceScene(nextScene);
	}
}

void ByoYomiSettingScene::byoYomiTime2ButtonDidPushed(Ref* pSender)
{
	if(!flicking) {
		UserDefault::getInstance()->setIntegerForKey("player", 1);
		Scene *nextScene = CCTransitionSlideInR::create(0.1f, ByoYomiTimeSettingScene::scene());
		Director::getInstance()->replaceScene(nextScene);
	}
}

void ByoYomiSettingScene::byoYomiKouryoCnt1ButtonDidPushed(Ref* pSender)
{
	if(!flicking) {
		UserDefault::getInstance()->setIntegerForKey("player", 0);
		Scene *nextScene = CCTransitionSlideInR::create(0.1f, ByoYomiKouryoCntSettingScene::scene());
		Director::getInstance()->replaceScene(nextScene);
	}
}

void ByoYomiSettingScene::byoYomiKouryoCnt2ButtonDidPushed(Ref* pSender)
{
	if(!flicking) {
		UserDefault::getInstance()->setIntegerForKey("player", 1);
		Scene *nextScene = CCTransitionSlideInR::create(0.1f, ByoYomiKouryoCntSettingScene::scene());
		Director::getInstance()->replaceScene(nextScene);
	}
}
