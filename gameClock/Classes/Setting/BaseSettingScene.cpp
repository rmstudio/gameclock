#define COCOS2D_DEBUG 1

#include "BaseSettingScene.h"
#include "TimeSetting/TimeSettingScene.h"
#include "RuleSettingScene.h"
#include "ColorSettingScene.h"
#include "GameClockScene.h"
#include "cocos-ext.h"
#include "CCPageControl.h"
#include "MenuPanel.h"
#include "platform/android/jni/JniHelper.h"
#include "SimpleAudioEngine.h"
#include <jni.h>
#include <string>
#include <iostream>

USING_NS_CC;
USING_NS_CC_EXT;

using namespace cocos2d;
using namespace cocos2d::extension;
using namespace CocosDenshion;

enum BUTTON {
    BUTTON_TIME1   = 0,
    BUTTON_TIME2  = 1,
    BUTTON_COLOR1  = 2,
    BUTTON_COLOR2  = 3,
    BUTTON_RULE  = 3,
};

enum RULE {
    RULE_KIREMAKE   = 0,
    RULE_BYOYOMI  = 1,
    RULE_FISHER  = 2,
    RULE_CANADA  = 3,
    RULE_XIANQQI  = 4,
};

enum PLAYER {
	PLAYER_SENTE = 0,
	PLAYER_GOTE = 1,
};

Scene* BaseSettingScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    BaseSettingScene *layer = BaseSettingScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool BaseSettingScene::init()
{
    return true;
}

void BaseSettingScene::loadCommonSetting() {

    m_settingBgColorStart = Color4B(237, 238, 217, 255);
    m_settingBgColorEnd = Color4B(237, 238, 217, 255);

    m_settingHeaderBgColorStart = Color4B(196, 229, 103, 255);
    m_settingHeaderBgColorEnd = Color4B(64, 132, 31, 255);
    m_settingFooterBgColorStart = Color4B(64, 132, 31, 255);
    m_settingFooterBgColorEnd = Color4B(64, 132, 31, 255);

    m_titleFontColor = Color3B(255, 255, 255);
    m_subjectFontColor = Color3B(98, 98, 98);
    m_buttonFontColor = Color3B(98, 98, 98);

    m_colorLabelBorderColor = Color4B(98, 98, 98, 255);

    m_fontColor = Color3B(98, 98, 98);

	m_headerBarHeight =  120;
	m_footerBarHeight =  200;

    LayerGradient::initWithColor( m_settingBgColorStart, m_settingBgColorEnd );

    flicking = false;

    // サウンド
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0f);
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/start.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/change_scene.mp3");

    m_winSize = Director::getInstance()->getWinSize();

    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();

    listener->onTouchBegan = CC_CALLBACK_2(BaseSettingScene::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(BaseSettingScene::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(BaseSettingScene::onTouchEnded, this);

    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	m_headerBarHeight =  120;
	m_footerBarHeight =  200;
}

// button action
void BaseSettingScene::startButtonDidPushed(Ref* pSender)
{
	if(checkParameter()) {
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/start.mp3");
		Scene *nextScene = CCTransitionSlideInR::create(0.1f, GameClockScene::scene());
		Director::getInstance()->replaceScene(nextScene);
	}
}

void BaseSettingScene::time1ButtonDidPushed(Ref* pSender)
{
	UserDefault::getInstance()->setIntegerForKey("player", 0);
	Scene *nextScene = CCTransitionSlideInR::create(0.1f, TimeSettingScene::scene());
	Director::getInstance()->replaceScene(nextScene);
}

void BaseSettingScene::time2ButtonDidPushed(Ref* pSender)
{
	UserDefault::getInstance()->setIntegerForKey("player", 1);
	Scene *nextScene = CCTransitionSlideInR::create(0.1f, TimeSettingScene::scene());
	Director::getInstance()->replaceScene(nextScene);
}

void BaseSettingScene::color1ButtonDidPushed(Ref* pSender)
{
	UserDefault::getInstance()->setIntegerForKey("player", 0);
	UserDefault::getInstance()->setIntegerForKey("player_color", 0);
	Scene *nextScene = TransitionSlideInR::create(0.1f, ColorSettingScene::scene());
	Director::getInstance()->replaceScene(nextScene);
}

void BaseSettingScene::color2ButtonDidPushed(Ref* pSender)
{
	UserDefault::getInstance()->setIntegerForKey("player", 1);
	UserDefault::getInstance()->setIntegerForKey("player_color", 1);
	Scene *nextScene = CCTransitionSlideInR::create(0.1f, ColorSettingScene::scene());
	Director::getInstance()->replaceScene(nextScene);
}

void BaseSettingScene::ruleButtonDidPushed(Ref* pSender)
{
	Scene *nextScene = CCTransitionSlideInR::create(0.1f, RuleSettingScene::scene());
	Director::getInstance()->replaceScene(nextScene);
}

void BaseSettingScene::forceStartButtonDidPushed(Ref* pSender)
{
	m_scrollLayer->setTouchEnabled(true);
	updateActiveStateButtonUnderDialog(true);

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/start.mp3");
	Scene *nextScene = CCTransitionSlideInR::create(0.1f, GameClockScene::scene());
	Director::getInstance()->replaceScene(nextScene);
}

void BaseSettingScene::cancelButtonDidPushed(Ref* pSender)
{
	m_scrollLayer->setTouchEnabled(true);
	updateActiveStateButtonUnderDialog(true);

	this->removeChild(m_batteryAlertDialogLayer, true);
	this->removeChild(m_pauseLayerbg, true);
}

void BaseSettingScene::loadSaveParams() {

    // 持ち時間
    m_hour1 = UserDefault::getInstance()->getIntegerForKey("hour1" , 0);
    m_hour2 = UserDefault::getInstance()->getIntegerForKey("hour2", 0);
    m_minite1 = UserDefault::getInstance()->getIntegerForKey("minite1" , 30);
    m_minite2 = UserDefault::getInstance()->getIntegerForKey("minite2", 30);

    // 背景色
    m_color1 = UserDefault::getInstance()->getIntegerForKey("color1", 0);
    m_color2 = UserDefault::getInstance()->getIntegerForKey("color2", 1);

    // ルール
    m_rule = UserDefault::getInstance()->getIntegerForKey("rule", 0);

    // 秒読み時間
    m_byo_yomi_time1 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_time1", 30);
    m_byo_yomi_time2 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_time2", 30);

    // 秒読み考慮回数
    m_byo_yomi_kouryo_cnt_current1 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_cnt1", 3);
    m_byo_yomi_kouryo_cnt_current2 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_cnt2", 3);

    // 最大秒読み考慮回数
    m_byo_yomi_kouryo_cnt1 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_cnt1", 3);
    m_byo_yomi_kouryo_cnt2 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_cnt2", 3);

    // 秒読み考慮時間
    m_byo_yomi_kouryo_time1 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_time1", 10);
    m_byo_yomi_kouryo_time2 = UserDefault::getInstance()->getIntegerForKey("byo_yomi_kouryo_time2", 10);

    // フィッシャー加算時間
    m_fisher_additional_time1 = UserDefault::getInstance()->getIntegerForKey("fisher_additional_time1", 10);
    m_fisher_additional_time2 = UserDefault::getInstance()->getIntegerForKey("fisher_additional_time2", 10);

    // カナダ式時間
    m_canada_kitei_time1 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_time1", 30);
    m_canada_kitei_time2 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_time2", 30);

    // カナダ式手数
    m_canada_kitei_tekazu1 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_tekazu1", 10);
    m_canada_kitei_tekazu2 = UserDefault::getInstance()->getIntegerForKey("canada_kitei_tekazu2", 10);

    // シャンチー国際 規定手数１
    m_xianqqi_kitei_tekazu1_1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu1_1", 10);
    m_xianqqi_kitei_tekazu1_2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu1_2", 10);

    // シャンチー国際 持ち時間２
    m_xianqqi_moti_jikan2_1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_moti_jikan2_1", 15);
    m_xianqqi_moti_jikan2_2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_moti_jikan2_2", 15);

    // シャンチー国際 規定手数２
    m_xianqqi_kitei_tekazu2_1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu2_1", 5);
    m_xianqqi_kitei_tekazu2_2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_tekazu2_2", 5);

    // シャンチー国際 規定回数
    m_xianqqi_kitei_cnt1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_cnt1", 5);
    m_xianqqi_kitei_cnt2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_kitei_cnt2", 5);

    // シャンチー国際 秒読み
    m_xianqqi_byo_yomi_time1 = UserDefault::getInstance()->getIntegerForKey("xianqqi_byo_yomi_time1", 30);
    m_xianqqi_byo_yomi_time2 = UserDefault::getInstance()->getIntegerForKey("xianqqi_byo_yomi_time2", 30);
}

void BaseSettingScene::setBackGroundColor() {

	    m_colorTurqoise = Color4B(26, 188, 156, 255);
	    m_colorGreenSea = Color4B(22, 160, 133, 255);
	    m_colorSunFlower = Color4B(241, 196, 15, 255);
	    m_colorOrange = Color4B(243, 156, 18, 255);
	    m_colorEmerald = Color4B(46, 204, 113, 255);
	    m_colorNephritis = Color4B(39, 174, 96, 255);
	    m_colorCarrot = Color4B(230, 126, 34, 255);
	    m_colorPumpkin = Color4B(211, 84, 0, 255);
	    m_colorPeterRiver = Color4B(52, 152, 219, 255);
	    m_colorBelizeHole = Color4B(41, 128, 185, 255);
	    m_colorAlizarin = Color4B(231, 76, 60, 255);
	    m_colorPomegranate = Color4B(192, 57, 43, 255);
	    m_colorAmethyst = Color4B(155, 89, 182, 255);
	    m_colorWisteria = Color4B(142, 68, 173, 255);
	    m_colorClouds = Color4B(236, 240, 241, 255);
	    m_colorSilver = Color4B(189, 195, 199, 255);
	    m_colorWetAspalt = Color4B(52, 73, 94, 255);
	    m_colorMidnightBlue = Color4B(44, 62, 80, 255);
	    m_colorConcrete = Color4B(149, 165, 166, 255);
	    m_colorAsbestos = Color4B(127, 140, 141, 255);

	    if(m_color1 == 0) {
	        m_startcolor1 = m_colorPeterRiver;
	        m_endcolor1 = m_colorPeterRiver;
	    } else if(m_color1 ==1) {
	        m_startcolor1 = m_colorBelizeHole;
	        m_endcolor1 = m_colorBelizeHole;
	    } else if(m_color1 ==2) {
	        m_startcolor1 = m_colorAlizarin;
	        m_endcolor1 = m_colorAlizarin;
	    } else if(m_color1 ==3) {
	        m_startcolor1 = m_colorPomegranate;
	        m_endcolor1 = m_colorPomegranate;
	    } else if(m_color1 ==4) {
	        m_startcolor1 = m_colorEmerald;
	        m_endcolor1 = m_colorEmerald;
	    } else if(m_color1 ==5) {
	        m_startcolor1 = m_colorNephritis;
	        m_endcolor1 = m_colorNephritis;
	    } else if(m_color1 ==6) {
	        m_startcolor1 = m_colorCarrot;
	        m_endcolor1 = m_colorCarrot;
	    } else if(m_color1 ==7) {
	        m_startcolor1 = m_colorPumpkin;
	        m_endcolor1 = m_colorPumpkin;
	    } else if(m_color1 ==8) {
	        m_startcolor1 = m_colorTurqoise;
	        m_endcolor1 = m_colorTurqoise;
	    } else if(m_color1 ==9) {
	        m_startcolor1 = m_colorGreenSea;
	        m_endcolor1 = m_colorGreenSea;
	    } else if(m_color1 ==10) {
	        m_startcolor1 = m_colorSunFlower;
	        m_endcolor1 = m_colorSunFlower;
	    } else if(m_color1 ==11) {
	        m_startcolor1 = m_colorOrange;
	        m_endcolor1 = m_colorOrange;
	    } else if(m_color1 ==12) {
	        m_startcolor1 = m_colorAmethyst;
	        m_endcolor1 = m_colorAmethyst;
	    } else if(m_color1 ==13) {
	        m_startcolor1 = m_colorWisteria;
	        m_endcolor1 = m_colorWisteria;
	    } else if(m_color1 ==14) {
	        m_startcolor1 = m_colorClouds;
	        m_endcolor1 = m_colorClouds;
	    } else if(m_color1 ==15) {
	        m_startcolor1 = m_colorSilver;
	        m_endcolor1 = m_colorSilver;
	    } else if(m_color1 ==16) {
	        m_startcolor1 = m_colorWetAspalt;
	        m_endcolor1 = m_colorWetAspalt;
	    } else if(m_color1 ==17) {
	        m_startcolor1 = m_colorMidnightBlue;
	        m_endcolor1 = m_colorMidnightBlue;
	    } else if(m_color1 ==18) {
	        m_startcolor1 = m_colorConcrete;
	        m_endcolor1 = m_colorConcrete;
	    } else if(m_color1 ==19) {
	        m_startcolor1 = m_colorAsbestos;
	        m_endcolor1 = m_colorAsbestos;
	    }

	    if(m_color2 == 0) {
	        m_startcolor2 = m_colorPeterRiver;
	        m_endcolor2 = m_colorPeterRiver;
	    } else if(m_color2 ==1) {
	    	m_startcolor2 = m_colorBelizeHole;
	    	m_endcolor2 = m_colorBelizeHole;
	    } else if(m_color2 ==2) {
	    	m_startcolor2 = m_colorAlizarin;
	    	m_endcolor2 = m_colorAlizarin;
	    } else if(m_color2 ==3) {
	    	m_startcolor2 = m_colorPomegranate;
	    	m_endcolor2 = m_colorPomegranate;
	    } else if(m_color2 ==4) {
	    	m_startcolor2 = m_colorEmerald;
	    	m_endcolor2 = m_colorEmerald;
	    } else if(m_color2 ==5) {
	    	m_startcolor2 = m_colorNephritis;
	    	m_endcolor2 = m_colorNephritis;
	    } else if(m_color2 ==6) {
	    	m_startcolor2 = m_colorCarrot;
	        m_endcolor2 = m_colorCarrot;
	    } else if(m_color2 ==7) {
	    	m_startcolor2 = m_colorPumpkin;
	        m_endcolor2 = m_colorPumpkin;
	    } else if(m_color2 ==8) {
	    	m_startcolor2 = m_colorTurqoise;
	        m_endcolor2 = m_colorTurqoise;
	    } else if(m_color2 ==9) {
	    	m_startcolor2 = m_colorGreenSea;
	        m_endcolor2 = m_colorGreenSea;
	    } else if(m_color2 ==10) {
	    	m_startcolor2 = m_colorSunFlower;
	    	m_endcolor2 = m_colorSunFlower;
	    } else if(m_color2 ==11) {
	    	m_startcolor2 = m_colorOrange;
	        m_endcolor2 = m_colorOrange;
	    } else if(m_color2 ==12) {
	    	m_startcolor2 = m_colorAmethyst;
	        m_endcolor2 = m_colorAmethyst;
	    } else if(m_color2 ==13) {
	    	m_startcolor2 = m_colorWisteria;
	        m_endcolor2 = m_colorWisteria;
	    } else if(m_color2 ==14) {
	    	m_startcolor2 = m_colorClouds;
	        m_endcolor2 = m_colorClouds;
	    } else if(m_color2 ==15) {
	    	m_startcolor2 = m_colorSilver;
	        m_endcolor2 = m_colorSilver;
	    } else if(m_color2 ==16) {
	    	m_startcolor2 = m_colorWetAspalt;
	        m_endcolor2 = m_colorWetAspalt;
	    } else if(m_color2 ==17) {
	    	m_startcolor2 = m_colorMidnightBlue;
	        m_endcolor2 = m_colorMidnightBlue;
	    } else if(m_color2 ==18) {
	    	m_startcolor2 = m_colorConcrete;
	        m_endcolor2 = m_colorConcrete;
	    } else if(m_color2 ==19) {
	    	m_startcolor2 = m_colorAsbestos;
	        m_endcolor2 = m_colorAsbestos;
	    }
}

bool BaseSettingScene::checkParameter() {

    std::string batteryStatus = getBatteryStatus();
	log("batteryStatus:%s", batteryStatus.c_str());

	std::string batteryLevelStr = getBatteryLevel();
    int batteryLevel = atoi(batteryLevelStr.data());
	log("batteryLevel:%d", batteryLevel);

	if(batteryStatus != "full" && batteryStatus != "charging") {

		if(batteryLevel < 20) {
			showBatteryAlertDialog("バッテリーの残量が少なくなっています。端末をプラグに接続してプレイして下さい。");
			return false;
		}

		int fullPlayTime1 = 0;
		int fullPlayTime2 = 0;

		if(m_rule == RULE_KIREMAKE || m_rule == RULE_FISHER) {
			fullPlayTime1 = m_hour1 * 3600 + m_minite1 * 60;
			fullPlayTime2 = m_hour2 * 3600 + m_minite2 * 60;
		} else if(m_rule == RULE_BYOYOMI) {
			fullPlayTime1 = m_hour1 * 3600 + m_minite1 * 60 +
					m_byo_yomi_time1 * m_byo_yomi_kouryo_cnt1;
			fullPlayTime2 = m_hour2 * 3600 + m_minite2 * 60 +
					m_byo_yomi_time2 * m_byo_yomi_kouryo_cnt2;
		} else if(m_rule == RULE_CANADA) {
			fullPlayTime1 = m_hour1 * 3600 + m_minite1 * 60 +
					m_canada_kitei_time1 * 60;
			fullPlayTime2 = m_hour2 * 3600 + m_minite2 * 60 +
					m_canada_kitei_time2 * 60;
		} else if(m_rule == RULE_XIANQQI) {
			fullPlayTime1 = m_hour1 * 3600 + m_minite1 * 60 +
					m_xianqqi_moti_jikan2_1 * m_xianqqi_kitei_cnt1 * 60;
			fullPlayTime2 = m_hour2 * 3600 + m_minite2 * 60 +
					m_xianqqi_moti_jikan2_2 * m_xianqqi_kitei_cnt2 * 60;
		}

		if(fullPlayTime1 >= 3600 || fullPlayTime2 >= 3600) {
			showBatteryAlertDialog("プレイ時間が長いためバッテリー切れをする怖れがあります。端末をプラグに接続してプレイして下さい。");
			return false;
		}
	}

	if((m_hour1 == 0 && m_minite1 == 0) || (m_hour2 == 0 && m_minite2 == 0)) {
		MessageBox("1分以上の持ち時間を設定して下さい。", "エラー");
		return false;
	}

	if(m_rule == RULE_BYOYOMI) {
		if(m_byo_yomi_time1 == 0 || m_byo_yomi_time2 == 0) {
			MessageBox("1秒以上の秒読み時間を設定して下さい。", "エラー");
			return false;
		}

		if(m_byo_yomi_kouryo_cnt_current1 == 0 || m_byo_yomi_kouryo_cnt_current2 == 0) {
			MessageBox("1回以上の考慮回数を設定して下さい。", "エラー");
			return false;
		}
	}

	if(m_rule == RULE_FISHER) {
		if(m_fisher_additional_time1 == 0 || m_fisher_additional_time2 == 0) {
			MessageBox("1秒以上の加算時間を設定して下さい。", "エラー");
			return false;
		}
	}

	if(m_rule == RULE_CANADA) {
		if(m_canada_kitei_time1 == 0 || m_canada_kitei_time2 == 0) {
			MessageBox("1分以上の規定時間を設定して下さい。", "エラー");
			return false;
		}

		if(m_canada_kitei_tekazu1 == 0 || m_canada_kitei_tekazu2 == 0) {
			MessageBox("1手以上の規定手数を設定して下さい。", "エラー");
			return false;
		}
	}

	if(m_rule == RULE_XIANQQI) {
		if(m_xianqqi_kitei_tekazu1_1 == 0 || m_xianqqi_kitei_tekazu1_2 == 0) {
			MessageBox("1手以上の規定手数を設定して下さい。", "エラー");
			return false;
		}

		if(m_xianqqi_moti_jikan2_1 == 0 || m_xianqqi_moti_jikan2_2 == 0) {
			MessageBox("1分以上の持ち時間を設定して下さい。", "エラー");
			return false;
		}

		if(m_xianqqi_kitei_tekazu2_1 == 0 || m_xianqqi_kitei_tekazu2_2 == 0) {
			MessageBox("1手以上の規定手数を設定して下さい。", "エラー");
			return false;
		}

		if(m_xianqqi_kitei_cnt1 == 0 || m_xianqqi_kitei_cnt2 == 0) {
			MessageBox("1回以上の規定回数を設定して下さい。", "エラー");
			return false;
		}

		if(m_xianqqi_byo_yomi_time1 == 0 || m_xianqqi_byo_yomi_time2 == 0) {
			MessageBox("1秒以上の秒読み時間を設定して下さい。", "エラー");
			return false;
		}
	}

	return true;
}

void BaseSettingScene::showRuleTitle() {

	int scrollLayerHeight =  m_scrollLayer->getInnerContainerSize().height;

    // 見出し（ルール）
	m_ruleHeadLineLabel = Label::createWithSystemFont("ルール", "HiraKakuProN-W6", 55);
	m_ruleHeadLineLabel->setPosition(Vec2(50, scrollLayerHeight-80));
	m_ruleHeadLineLabel->setColor(m_subjectFontColor);
	m_ruleHeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_ruleHeadLineLabel, 150);

}

void BaseSettingScene::showRuleMenu() {

	int startPositionY = m_ruleHeadLineLabel->getPositionY()-90;

	// 設定値表示セル（ルール）
    m_ruleSettingButtonItem = MenuItemImage::create(
													 "res/setting_menu.png",
													 "res/setting_menu.png",
													 CC_CALLBACK_1(BaseSettingScene::ruleButtonDidPushed, this));
    m_ruleSettingButtonItem->setPosition(Vec2(m_winSize.width/2, startPositionY));

    // ボタン内部見出し（ルール）
	Label *ruleLabel = Label::createWithSystemFont("ルール", "HiraKakuProN-W6", 48);
	ruleLabel->setPosition(Vec2(30, m_ruleSettingButtonItem->getContentSize().height/2));
	ruleLabel->setColor(m_buttonFontColor);
	ruleLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_ruleSettingButtonItem->addChild(ruleLabel, 150);

	m_ruleSettingButtonItem->removeChild(m_ruleDispLabel, true);

    // ボタン内部設定値（ルール）
	if(m_rule == RULE_KIREMAKE) {
		m_ruleDispLabel = Label::createWithSystemFont("切れ負け", "HiraKakuProN-W6", 48);
	} else if(m_rule == RULE_BYOYOMI) {
		m_ruleDispLabel = Label::createWithSystemFont("秒読み", "HiraKakuProN-W6", 48);
	} else if(m_rule == RULE_FISHER) {
		m_ruleDispLabel = Label::createWithSystemFont("フィッシャー", "HiraKakuProN-W6", 48);
	} else if(m_rule == RULE_CANADA) {
		m_ruleDispLabel = Label::createWithSystemFont("カナダ式", "HiraKakuProN-W6", 48);
	} else if(m_rule == RULE_XIANQQI) {
		m_ruleDispLabel = Label::createWithSystemFont("シャンチー国際", "HiraKakuProN-W6", 48);
	} else {
		m_ruleDispLabel = Label::createWithSystemFont("切れ負け", "HiraKakuProN-W6", 48);
	}

	m_ruleDispLabel->setPosition(Vec2(m_winSize.width-150, m_ruleSettingButtonItem->getContentSize().height/2));
    m_ruleDispLabel->setColor(m_buttonFontColor);
    m_ruleDispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_ruleSettingButtonItem->addChild(m_ruleDispLabel, 150);

	m_ruleSettingButton = ScrollMenu::create(m_ruleSettingButtonItem, NULL);
	m_ruleSettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_ruleSettingButton, 1);

}

void BaseSettingScene::showTime1Title() {

	int startPositionY = m_ruleSettingButtonItem->getPositionY()-110;

    // 見出し（持ち時間（player1））
	m_time1HeadLineLabel = Label::createWithSystemFont("時間設定(先手)", "HiraKakuProN-W6", 55);
	m_time1HeadLineLabel->setPosition(Vec2(50, startPositionY));
	m_time1HeadLineLabel->setColor(m_subjectFontColor);
	m_time1HeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_time1HeadLineLabel, 150);

}

void BaseSettingScene::showTime1Menu() {

	int startPositionY = m_time1HeadLineLabel->getPositionY()-90;

	// 設定値表示セル（持ち時間(先手)）
	m_time1SettingButtonItem = MenuItemImage::create(
			"res/setting_menu.png",
			"res/setting_menu.png",
			CC_CALLBACK_1(BaseSettingScene::time1ButtonDidPushed, this));
	m_time1SettingButtonItem->setPosition(Vec2(m_winSize.width/2, startPositionY));

	Label *timeLabel1 = Label::createWithSystemFont("持ち時間", "HiraKakuProN-W6", 48);
	timeLabel1->setPosition(Vec2(30, m_time1SettingButtonItem->getContentSize().height/2));
	timeLabel1->setColor(m_buttonFontColor);
	timeLabel1->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_time1SettingButtonItem->addChild(timeLabel1, 150);

	char hour1Str[15];
	sprintf(hour1Str, "%d 時間", m_hour1);
	m_hour1DispLabel = Label::createWithSystemFont(hour1Str, "HiraKakuProN-W6", 48);
	m_hour1DispLabel->setPosition(Vec2(m_winSize.width-300, m_time1SettingButtonItem->getContentSize().height/2));
	m_hour1DispLabel->setColor(m_buttonFontColor);
	m_hour1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
    m_time1SettingButtonItem->addChild(m_hour1DispLabel, 150);

	char minite1Str[15];

	sprintf(minite1Str, "%d 分", m_minite1);
	m_minite1DispLabel = Label::createWithSystemFont(minite1Str, "HiraKakuProN-W6", 48);
	m_minite1DispLabel->setPosition(Vec2(m_winSize.width-130, m_time1SettingButtonItem->getContentSize().height/2));
	m_minite1DispLabel->setColor(m_buttonFontColor);
	m_minite1DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
    m_time1SettingButtonItem->addChild(m_minite1DispLabel, 150);

	m_time1SettingButton = ScrollMenu::create(m_time1SettingButtonItem, NULL);
	m_time1SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_time1SettingButton, 1);

}

void BaseSettingScene::showTime2Title() {

	int startPositionY;

	if(m_rule == RULE_KIREMAKE) {
		startPositionY = m_time1SettingButtonItem->getPositionY()-110;
	} else if(m_rule == RULE_BYOYOMI) {
		startPositionY = m_byoYomiKouryoTime1SettingButtonItem->getPositionY()-110;
	} else if(m_rule == RULE_FISHER) {
		startPositionY = m_fisherAddTime1SettingButtonItem->getPositionY()-110;
	} else if(m_rule == RULE_CANADA) {
		startPositionY = m_canadaKiteiTekazu1SettingButtonItem->getPositionY()-110;
	} else if(m_rule == RULE_XIANQQI) {
		startPositionY = m_xianqqiByoYomi1SettingButtonItem->getPositionY()-110;
	}

	// 持ち時間（player2）
	m_time2HeadLineLabel = Label::createWithSystemFont("時間設定(後手)", "HiraKakuProN-W6", 55);
	m_time2HeadLineLabel->setPosition(Vec2(50, startPositionY));
	m_time2HeadLineLabel->setColor(m_subjectFontColor);
	m_time2HeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_time2HeadLineLabel, 150);
}

void BaseSettingScene::showTime2Menu() {

	int startPositionY = m_time2HeadLineLabel->getPositionY()-90;

	m_time2SettingButtonItem = MenuItemImage::create(
													 "res/setting_menu.png",
													 "res/setting_menu.png",
													 CC_CALLBACK_1(BaseSettingScene::time2ButtonDidPushed, this));
	m_time2SettingButtonItem->setPosition( Vec2(m_winSize.width/2, startPositionY) );

	Label *timeLabel2 = Label::createWithSystemFont("持ち時間", "HiraKakuProN-W6", 48);
	timeLabel2->setPosition(Vec2(30, m_time2SettingButtonItem->getContentSize().height/2));
	timeLabel2->setColor(m_buttonFontColor);
	timeLabel2->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_time2SettingButtonItem->addChild(timeLabel2, 150);

	char hour2Str[15];
	sprintf(hour2Str, "%d 時間", m_hour2);
	m_hour2DispLabel = Label::createWithSystemFont(hour2Str, "HiraKakuProN-W6", 48);
	m_hour2DispLabel->setPosition(Vec2(m_winSize.width-300, m_time1SettingButtonItem->getContentSize().height/2));
	m_hour2DispLabel->setColor(m_buttonFontColor);
	m_hour2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
    m_time2SettingButtonItem->addChild(m_hour2DispLabel, 150);

	char minite2Str[15];
	sprintf(minite2Str, "%d 分", m_minite2);
	m_minite2DispLabel = Label::createWithSystemFont(minite2Str, "HiraKakuProN-W6", 48);
	m_minite2DispLabel->setPosition(Vec2(m_winSize.width-130, m_time2SettingButtonItem->getContentSize().height/2));
	m_minite2DispLabel->setColor(m_buttonFontColor);
	m_minite2DispLabel->setAnchorPoint(Vec2(1.0f, 0.5f));
	m_time2SettingButtonItem->addChild(m_minite2DispLabel, 150);

	m_time2SettingButton = ScrollMenu::create(m_time2SettingButtonItem, NULL);
	m_time2SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_time2SettingButton, 1);

}

void BaseSettingScene::showColorTitle() {

	int startPositionY;

	if(m_rule == RULE_KIREMAKE) {
		startPositionY = m_time2SettingButtonItem->getPositionY()-110;
	} else if(m_rule == RULE_BYOYOMI) {
		startPositionY = m_byoYomiKouryoTime2SettingButtonItem->getPositionY()-110;
	} else if(m_rule == RULE_FISHER) {
		startPositionY = m_fisherAddTime2SettingButtonItem->getPositionY()-110;
	} else if(m_rule == RULE_CANADA) {
		startPositionY = m_canadaKiteiTekazu2SettingButtonItem->getPositionY()-110;
	} else if(m_rule == RULE_XIANQQI) {
		startPositionY = m_xianqqiByoYomi2SettingButtonItem->getPositionY()-110;
	}

	log("color title startPositionY:%d", startPositionY);

	// 背景色（player1）
	m_colorHeadLineLabel = Label::createWithSystemFont("背景色設定", "HiraKakuProN-W6", 55);
	m_colorHeadLineLabel->setPosition(Vec2(50, startPositionY));
	m_colorHeadLineLabel->setColor(Color3B(255, 255, 255));
	m_colorHeadLineLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_scrollLayer->addChild(m_colorHeadLineLabel, 150);
}

void BaseSettingScene::showColor1Menu() {

	int startPositionY =  m_colorHeadLineLabel->getPositionY()-90;

	m_color1SettingButtonItem = MenuItemImage::create(
													  "res/setting_menu_top.png",
													  "res/setting_menu_top.png",
													  CC_CALLBACK_1(BaseSettingScene::color1ButtonDidPushed, this));
	m_color1SettingButtonItem->setPosition( Vec2(m_winSize.width/2, startPositionY) );

	Label *colorLabel1 = Label::createWithSystemFont("背景色(先手)", "HiraKakuProN-W6", 48);
	colorLabel1->setPosition(Vec2(30, m_time2SettingButtonItem->getContentSize().height/2));
	colorLabel1->setColor(m_buttonFontColor);
	colorLabel1->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_color1SettingButtonItem->addChild(colorLabel1, 150);

	LayerGradient *baseLayer1 = LayerGradient::create(m_colorLabelBorderColor, m_colorLabelBorderColor);
	baseLayer1->setContentSize(Size(70, 70));
	baseLayer1->setAnchorPoint(Vec2(1.0f, 0.5f));
	baseLayer1->setPosition(Vec2(m_winSize.width-200, 13));

	m_color1Layer = LayerGradient::create(m_startcolor1, m_endcolor1);
	m_color1Layer->setContentSize(Size(68, 68));
	m_color1Layer->setAnchorPoint(Vec2(0.0f, 0.0f));
	m_color1Layer->setPosition(Vec2(2, 2));
	baseLayer1->addChild(m_color1Layer, 10);

	m_color1SettingButtonItem->addChild(baseLayer1, 10);

	m_color1SettingButton = ScrollMenu::create(m_color1SettingButtonItem, NULL);
	m_color1SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_color1SettingButton, 1);
}

void BaseSettingScene::showColor2Menu() {

	int startPositionY =  m_color1SettingButtonItem->getPositionY()-95;

	// 背景色（player2）
	m_color2SettingButtonItem = MenuItemImage::create(
														"res/setting_menu_bottom.png",
														"res/setting_menu_bottom.png",
														CC_CALLBACK_1(BaseSettingScene::color2ButtonDidPushed, this));
	m_color2SettingButtonItem->setPosition( Vec2(m_winSize.width/2, startPositionY) );

	Label *colorLabel2 = Label::createWithSystemFont("背景色(後手)", "HiraKakuProN-W6", 48);
	colorLabel2->setPosition(Vec2(30, m_time2SettingButtonItem->getContentSize().height/2));
	colorLabel2->setColor(m_buttonFontColor);
	colorLabel2->setAnchorPoint(Vec2(0.0f, 0.5f));
	m_color2SettingButtonItem->addChild(colorLabel2, 150);

	LayerGradient *baseLayer2 = LayerGradient::create(m_colorLabelBorderColor, m_colorLabelBorderColor);
	baseLayer2->setContentSize(Size(70, 70));
	baseLayer2->setAnchorPoint(Vec2(1.0f, 0.5f));
	baseLayer2->setPosition(Vec2(m_winSize.width-200, 13));

	m_color2Layer = LayerGradient::create(m_startcolor2, m_endcolor2);
	m_color2Layer->setContentSize(Size(68, 68));
	m_color2Layer->setAnchorPoint(Vec2(0.0f, 0.0f));
	m_color2Layer->setPosition(Vec2(2, 2));
	baseLayer2->addChild(m_color2Layer, 10);

	m_color2SettingButtonItem->addChild(baseLayer2, 10);

	m_color2SettingButton = ScrollMenu::create(m_color2SettingButtonItem, NULL);
	m_color2SettingButton->setPosition( Point::ZERO );
	m_scrollLayer->addChild(m_color2SettingButton, 1);
}

void BaseSettingScene::showBatteryAlertDialog(const char* message) {

	m_scrollLayer->setTouchEnabled(false);
	updateActiveStateButtonUnderDialog(false);

    // グレーアウト
    m_pauseLayerbg = LayerColor::create(Color4B(0, 0, 0, 100));
    m_pauseLayerbg->setContentSize(Size(m_winSize.width, m_winSize.height));
    m_pauseLayerbg->setPosition(Point(0, 0));
    m_pauseLayerbg->setTag(120);
    this->addChild( m_pauseLayerbg , 300);

	m_batteryAlertDialogLayer = BatteryAlertDialogLayer::create();
	m_batteryAlertDialogLayer->setPosition(Point(m_winSize.width/2-300, m_winSize.height/2-175));
	m_batteryAlertDialogLayer->setContentSize( Size(600,500) );
	m_batteryAlertDialogLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_batteryAlertDialogLayer->setTag(110);
	m_batteryAlertDialogLayer->setTitle("警告", new __String("pause"));
	m_batteryAlertDialogLayer->setScale(0.5f);
    this->addChild(m_batteryAlertDialogLayer, 400);

    ActionInterval* scaleAction = ScaleTo::create(0.2f, 1.0f);
    ActionInterval* easeAction = EaseElasticOut::create(scaleAction, 1.0f);
    m_batteryAlertDialogLayer->runAction(easeAction);

    // ラベル
    Label *messageLabel = Label::createWithSystemFont(message, "Thonburi", 42, Size::ZERO, TextHAlignment::LEFT, TextVAlignment::TOP);
    messageLabel->setColor(m_fontColor);
    messageLabel->setWidth(540);
    messageLabel->setPosition( Vec2(m_batteryAlertDialogLayer->getContentSize().width/2, m_batteryAlertDialogLayer->getContentSize().height-120) );
    messageLabel->setAnchorPoint(Vec2(0.5f, 1.0f));
    m_batteryAlertDialogLayer->addChild(messageLabel, 420);

    // キャンセルボタン
    MenuItemImage *cancelButtonItem = MenuItemImage::create(
                                                               "res/dialog_cancel_button.png",
                                                               "res/dialog_cancel_button.png",
                                                               CC_CALLBACK_1(BaseSettingScene::cancelButtonDidPushed, this));
    cancelButtonItem->setPosition( Vec2(180, 80) );
    Menu* cancelButton = Menu::create(cancelButtonItem, NULL);
    cancelButton->setPosition( Point::ZERO );
    m_batteryAlertDialogLayer->addChild(cancelButton, 410);

    // スタートボタン
    MenuItemImage *startButtonItem = MenuItemImage::create(
                                                               "res/dialog_start_button.png",
                                                               "res/dialog_start_button.png",
                                                               CC_CALLBACK_1(BaseSettingScene::forceStartButtonDidPushed, this));
    startButtonItem->setPosition( Vec2(m_batteryAlertDialogLayer->getContentSize().width-180, 80) );
    Menu* startButton = Menu::create(startButtonItem, NULL);
    startButton->setPosition( Point::ZERO );
    m_batteryAlertDialogLayer->addChild(startButton, 410);
}

std::string BaseSettingScene::getBatteryStatus() {
	JniMethodInfo methodInfo;

	if (! JniHelper::getStaticMethodInfo(methodInfo, "com/rmstudio/gameClock/gameClock", "getBatteryStatus", "()Ljava/lang/String;"))
	{
		log("Failed to find static method of getBatteryStatus");
		return "";
	}
	// Javaのプログラムを呼び出す
	jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);

	// jstringをstd::stringに変換
	std::string ret = JniHelper::jstring2string((jstring)objResult);

	//log("batteryStatus:%s", ret.c_str());

	// 解放処理
	methodInfo.env->DeleteLocalRef(objResult);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);

	return ret.c_str();
}

std::string BaseSettingScene::getBatteryLevel() {
	JniMethodInfo methodInfo;

	if (! JniHelper::getStaticMethodInfo(methodInfo, "com/rmstudio/gameClock/gameClock", "getBatteryLevel", "()Ljava/lang/String;"))
	{
		log("Failed to find static method of getBatteryLevel");
		return "";
	}

	// Javaのプログラムを呼び出す
	jobject objResult = methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);

	// jstringをstd::stringに変換
	std::string ret = JniHelper::jstring2string((jstring)objResult);

	//log("batteryStatus:%s", ret.c_str());

	// 解放処理
	methodInfo.env->DeleteLocalRef(objResult);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);

	return ret.c_str();
}

void BaseSettingScene:: updateActiveStateButtonUnderDialog(bool state) {
	m_startButton->setEnabled(state);
    m_ruleSettingButton->setEnabled(state);
    m_time1SettingButton->setEnabled(state);
    m_time2SettingButton->setEnabled(state);
    m_color1SettingButton->setEnabled(state);
    m_color2SettingButton->setEnabled(state);

    if(m_rule == RULE_BYOYOMI) {
		m_byouYomiTime1SettingButton->setEnabled(state);
		m_byouYomiTime2SettingButton->setEnabled(state);
		m_byoYomiKouryoCnt1SettingButton->setEnabled(state);
		m_byoYomiKouryoCnt2SettingButton->setEnabled(state);
		m_byoYomiKouryoTime1SettingButton->setEnabled(state);
		m_byoYomiKouryoTime2SettingButton->setEnabled(state);
    } else if(m_rule == RULE_FISHER) {
		m_fisherAddTime1SettingButton->setEnabled(state);
		m_fisherAddTime2SettingButton->setEnabled(state);
    } else if(m_rule == RULE_CANADA) {
		m_canadaKiteiTime1SettingButton->setEnabled(state);
		m_canadaKiteiTime2SettingButton->setEnabled(state);
		m_canadaKiteiTekazu1SettingButton->setEnabled(state);
		m_canadaKiteiTekazu2SettingButton->setEnabled(state);
    } else if(m_rule == RULE_XIANQQI) {
		m_xianqqiKiteiTekazu1_1SettingButton->setEnabled(state);
		m_xianqqiKiteiTekazu1_2SettingButton->setEnabled(state);
		m_xianqqiKiteiTekazu2_1SettingButton->setEnabled(state);
		m_xianqqiKiteiTekazu2_2SettingButton->setEnabled(state);
		m_xianqqiTime2_1SettingButton->setEnabled(state);
		m_xianqqiTime2_2SettingButton->setEnabled(state);
		m_xianqqiKiteiCnt1SettingButton->setEnabled(state);
		m_xianqqiKiteiCnt2SettingButton->setEnabled(state);
		m_xianqqiByoYomi1SettingButton->setEnabled(state);
		m_xianqqiByoYomi2SettingButton->setEnabled(state);
    }
}
