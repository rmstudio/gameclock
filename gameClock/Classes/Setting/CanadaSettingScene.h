#ifndef __CanadaSettingScene_h__
#define __CanadaSettingScene_h__

#include "cocos2d.h"
#include "DialogLayer.h"
#include "cocos-ext.h"
#include "CCSlidingLayer.h"
#include "BaseSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class CanadaSettingScene : public BaseSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(CanadaSettingScene);
    
    float m_start_y;
    float m_end_y;

    // display
    void showRuleTitle();
    void showRuleMenu();
    void showTime1Title();
    void showTime2Title();
    void showColorTitle();
    void showColor1Menu();
    void showColor2Menu();

    void showCanadaTime1Menu();
    void showCanadaTime2Menu();

    // button action
    void canadaKiteiTime1ButtonDidPushed(Ref* pSender);
    void canadaKiteiTime2ButtonDidPushed(Ref* pSender);
    void canadaKiteiTekazu1ButtonDidPushed(Ref* pSender);
    void canadaKiteiTekazu2ButtonDidPushed(Ref* pSender);

};

#endif
