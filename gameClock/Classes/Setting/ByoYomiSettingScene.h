#ifndef __ByoYomiSettingScene_h__
#define __ByoYomiSettingScene_h__

#include "cocos2d.h"
#include "DialogLayer.h"
#include "cocos-ext.h"
#include "CCSlidingLayer.h"
#include "BaseSettingScene.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class ByoYomiSettingScene : public BaseSettingScene {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(ByoYomiSettingScene);
    
    // 設定値ボタン
    MenuItemImage *m_byoYomiTime1SettingButtonItem;			// 秒読み時間（プレイヤー１）（秒読み）
    MenuItemImage *m_byoYomiTime2SettingButtonItem;			// 秒読み時間（プレイヤー２）（秒読み）
    MenuItemImage *m_byoYomiKouryoCnt1SettingButtonItem;		// 考慮回数（プレイヤー１）（秒読み）
    MenuItemImage *m_byoYomiKouryoCnt2SettingButtonItem;		// 考慮回数（プレイヤー２）（秒読み）
    
    Menu *m_byouYomiTime1SettingButton;						// 秒読み時間（プレイヤー１）（秒読み）
    Menu *m_byouYomiTime2SettingButton;						// 秒読み時間（プレイヤー２）（秒読み）
    Menu *m_byoYomiKouryoCnt1SettingButton;					// 考慮回数（プレイヤー１）（秒読み）
    Menu *m_byoYomiKouryoCnt2SettingButton;					// 考慮回数（プレイヤー２）（秒読み）

    // display
    void showRuleTitle();
    void showRuleMenu();
    void showTime1Title();
    void showTime2Title();
    void showColorTitle();
    void showColor1Menu();
    void showColor2Menu();

    void showByoYomiTime1Menu();
    void showByoYomiTime2Menu();

    // button aciton
    void byoYomiTime1ButtonDidPushed(Ref* pSender);
    void byoYomiTime2ButtonDidPushed(Ref* pSender);
    void byoYomiKouryoCnt1ButtonDidPushed(Ref* pSender);
    void byoYomiKouryoCnt2ButtonDidPushed(Ref* pSender);
};

#endif
