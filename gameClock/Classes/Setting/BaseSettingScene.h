#ifndef __BaseSettingScene_h__
#define __BaseSettingScene_h__

#include "cocos2d.h"
#include "BatteryAlertDialogLayer.h"
#include "cocos-ext.h"
#include "CCSlidingLayer.h"
#include "ui/CocosGUI.h"
#include "ScrollMenuView.h"
#include "ScrollMenu.h"

using namespace cocos2d;
using namespace cocos2d::extension;

class BaseSettingScene : public cocos2d::LayerGradient {
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(BaseSettingScene);

	bool flicking;

	bool m_isCharging;

	Size m_winSize;
    
    // レイヤ
    BatteryAlertDialogLayer *m_batteryAlertDialogLayer;
    LayerGradient *m_headerBarLayer;
    LayerColor *m_settingLayerbg;
    LayerColor *m_pauseLayerbg;
    ScrollMenuView *m_scrollLayer;
    LayerGradient *m_footerBarLayer;
    
    // 設定画面背景色
    Color4B m_settingBgColorStart;
    Color4B m_settingBgColorEnd;

    // 設定画面ヘッダー背景色
    Color4B m_settingHeaderBgColorStart;
    Color4B m_settingHeaderBgColorEnd;

    // 設定画面フッター背景色
    Color4B m_settingFooterBgColorStart;
    Color4B m_settingFooterBgColorEnd;

    // カラーラベルの枠の色
    Color4B m_colorLabelBorderColor;

    // フォントカラー
    Color3B m_titleFontColor;
    Color3B m_subjectFontColor;
    Color3B m_buttonFontColor;

    // 設定値ボタン
    MenuItemImage *m_startButtonItem;
    MenuItemImage *m_ruleSettingButtonItem;						// ルール
    MenuItemImage *m_time1SettingButtonItem;					// 持ち時間（プレイヤー１）
    MenuItemImage *m_time2SettingButtonItem;					// 持ち時間（プレイヤー２）

    MenuItemImage *m_byoYomiTime1SettingButtonItem;				// 秒読み時間（プレイヤー１）（秒読み）
    MenuItemImage *m_byoYomiTime2SettingButtonItem;				// 秒読み時間（プレイヤー２）（秒読み）
    MenuItemImage *m_byoYomiKouryoCnt1SettingButtonItem;		// 考慮回数（プレイヤー１）（秒読み）
    MenuItemImage *m_byoYomiKouryoCnt2SettingButtonItem;		// 考慮回数（プレイヤー２）（秒読み）
    MenuItemImage *m_byoYomiKouryoTime1SettingButtonItem;		// 考慮時間（プレイヤー１）（秒読み）
    MenuItemImage *m_byoYomiKouryoTime2SettingButtonItem;		// 考慮時間（プレイヤー２）（秒読み）
    MenuItemImage *m_fisherAddTime1SettingButtonItem;           // 加算時間（プレイヤー１）（フィッシャー）
    MenuItemImage *m_fisherAddTime2SettingButtonItem;			// 加算時間（プレイヤー２）（フィッシャー）
    MenuItemImage *m_canadaKiteiTime1SettingButtonItem;			// 規定時間（プレイヤー１）（カナダ式）
    MenuItemImage *m_canadaKiteiTime2SettingButtonItem;			// 規定時間（プレイヤー２）（カナダ式）
    MenuItemImage *m_canadaKiteiTekazu1SettingButtonItem;		// 規定手数（プレイヤー１）（カナダ式）
    MenuItemImage *m_canadaKiteiTekazu2SettingButtonItem;		// 規定手数（プレイヤー２）（カナダ式）
    MenuItemImage *m_xianqqiTime2_1SettingButtonItem;			// 持ち時間（プレイヤー１）（シャンチー国際）
    MenuItemImage *m_xianqqiTime2_2SettingButtonItem;			// 持ち時間（プレイヤー２）（シャンチー国際）
    MenuItemImage *m_xianqqiKiteiTekazu1_1SettingButtonItem;	// 規定手数１（プレイヤー１）（シャンチー国際）
    MenuItemImage *m_xianqqiKiteiTekazu1_2SettingButtonItem;	// 規定手数１（プレイヤー２）（シャンチー国際）
    MenuItemImage *m_xianqqiKiteiTekazu2_1SettingButtonItem;	// 規定手数２（プレイヤー１）（シャンチー国際）
    MenuItemImage *m_xianqqiKiteiTekazu2_2SettingButtonItem;	// 規定手数２（プレイヤー２）（シャンチー国際）
    MenuItemImage *m_xianqqiKiteiCnt1SettingButtonItem;			// 規定回数（プレイヤー１）（シャンチー国際）
    MenuItemImage *m_xianqqiKiteiCnt2SettingButtonItem;			// 規定回数（プレイヤー２）（シャンチー国際）
    MenuItemImage *m_xianqqiByoYomi1SettingButtonItem;			// 秒読み時間（プレイヤー１）（シャンチー国際）
    MenuItemImage *m_xianqqiByoYomi2SettingButtonItem;			// 秒読み時間（プレイヤー２）（シャンチー国際）

    MenuItemImage *m_color1SettingButtonItem;					// 背景色１
    MenuItemImage *m_color2SettingButtonItem;					// 背景色２
    
    Menu *m_startButton;
    Menu *m_cancelButton;
    ScrollMenu *m_ruleSettingButton;								// ルール
    ScrollMenu *m_time1SettingButton;								// 持ち時間（プレイヤー１）
    ScrollMenu *m_time2SettingButton;								// 持ち時間（プレイヤー２）

    ScrollMenu *m_byouYomiTime1SettingButton;						// 秒読み時間（プレイヤー１）（秒読み）
    ScrollMenu *m_byouYomiTime2SettingButton;						// 秒読み時間（プレイヤー２）（秒読み）
    ScrollMenu *m_byoYomiKouryoCnt1SettingButton;					// 考慮回数（プレイヤー１）（秒読み）
    ScrollMenu *m_byoYomiKouryoCnt2SettingButton;					// 考慮回数（プレイヤー２）（秒読み）
    ScrollMenu *m_byoYomiKouryoTime1SettingButton;					// 考慮時間（プレイヤー１）（秒読み）
    ScrollMenu *m_byoYomiKouryoTime2SettingButton;					// 考慮時間（プレイヤー２）（秒読み）
    ScrollMenu *m_fisherAddTime1SettingButton;						// 加算時間（プレイヤー１）（フィッシャー）
    ScrollMenu *m_fisherAddTime2SettingButton;						// 加算時間（プレイヤー２）（フィッシャー）
    ScrollMenu *m_canadaKiteiTime1SettingButton;					// 規定時間（プレイヤー１）（カナダ式）
    ScrollMenu *m_canadaKiteiTime2SettingButton;					// 規定時間（プレイヤー２）（カナダ式）
    ScrollMenu *m_canadaKiteiTekazu1SettingButton;					// 規定手数（プレイヤー１）（カナダ式）
    ScrollMenu *m_canadaKiteiTekazu2SettingButton;					// 規定手数（プレイヤー２）（カナダ式）
    ScrollMenu *m_xianqqiKiteiTekazu1_1SettingButton;				// 規定手数１（プレイヤー１）（シャンチー国際）
    ScrollMenu *m_xianqqiKiteiTekazu1_2SettingButton;				// 規定手数１（プレイヤー２）（シャンチー国際）
    ScrollMenu *m_xianqqiKiteiTekazu2_1SettingButton;				// 規定手数２（プレイヤー１）（シャンチー国際）
    ScrollMenu *m_xianqqiKiteiTekazu2_2SettingButton;				// 規定手数２（プレイヤー２）（シャンチー国際）
    ScrollMenu *m_xianqqiTime2_1SettingButton;						// 持ち時間２（プレイヤー１）（シャンチー国際）
    ScrollMenu *m_xianqqiTime2_2SettingButton;						// 持ち時間２（プレイヤー２）（シャンチー国際）
    ScrollMenu *m_xianqqiKiteiCnt1SettingButton;					// 規定回数（プレイヤー１）（シャンチー国際）
    ScrollMenu *m_xianqqiKiteiCnt2SettingButton;					// 規定回数（プレイヤー２）（シャンチー国際）
    ScrollMenu *m_xianqqiByoYomi1SettingButton;						// 秒読み時間（シャンチー国際）
    ScrollMenu *m_xianqqiByoYomi2SettingButton;						// 秒読み時間（シャンチー国際）
    ScrollMenu *m_color1SettingButton;								// 背景色１
    ScrollMenu *m_color2SettingButton;								// 背景色２

    // タイトルラベル
    Label *m_ruleHeadLineLabel;
    Label *m_time1HeadLineLabel;
    Label *m_time2HeadLineLabel;
    Label *m_colorHeadLineLabel;

    // 設定値ラベル
    cocos2d::Label *m_hour1DispLabel;
    cocos2d::Label *m_hour2DispLabel;
    cocos2d::Label *m_minite1DispLabel;
    cocos2d::Label *m_minite2DispLabel;
    cocos2d::Label *m_color1DispLabel;
    cocos2d::Label *m_color2DispLabel;
    cocos2d::Label *m_ruleDispLabel;
    
    // 設定値ラベル（ダイアログ内）
    cocos2d::Label* m_hour1Label;					// 持ち時間（時間）（プレイヤー１）
    cocos2d::Label* m_hour2Label;					// 持ち時間（時間）（プレイヤー２）
    cocos2d::Label* m_minite1Label;					// 持ち時間（分）（プレイヤー１）
    cocos2d::Label* m_minite2Label;					// 持ち時間（分）（プレイヤー２）
    cocos2d::Label* m_color1Label_;					// 背景色（プレイヤー１）
    cocos2d::Label* m_color2Label_;					// 背景色（プレイヤー２）
    cocos2d::Label* m_ruleLabel_;					// ルール

    cocos2d::Label *m_explanation;					// ルール説明

    // 背景レイヤー
    LayerGradient *m_color1Layer;
    LayerGradient *m_color2Layer;

    // 背景色
    Color4B m_startcolor1;
    Color4B m_endcolor1;
    Color4B m_startcolor2;
    Color4B m_endcolor2;
    
    Color4B m_startColorRed;				// 赤
    Color4B m_endColorRed;
    Color4B m_startColorBlue;				// 青
    Color4B m_endColorBlue;
    Color4B m_startColorGreen;				// 緑
    Color4B m_endColorGreen;
    Color4B m_startColorOrange;				// オレンジ（フォント：グレー）
    Color4B m_endColorOrange;

    Color4B m_startColorDeepPink;			// ディープピンク
    Color4B m_endColorDeepPink;
    Color4B m_startColorLightSeaGreen;		// ライトシーグリーン
    Color4B m_endColorLightSeaGreen;
    Color4B m_startColorOliveGreen;			// ダークオリーブグリーン
    Color4B m_endColorOliveGreen;
    Color4B m_startColorYellow;				// 黄
    Color4B m_endColorYellow;

    Color4B m_startColorHotPink;			// ホットピンク
    Color4B m_endColorHotPink;
    Color4B m_startColorSkyBlue;			// スカイブルー（フォント：グレー）
    Color4B m_endColorSkyBlue;
    Color4B m_startColorPaleGreen;			// パールグリーン
    Color4B m_endColorPaleGreen;
    Color4B m_startColorKhaki;				// カーキ
    Color4B m_endColorKhaki;

    Color4B m_startColorMediumViolet;		// ミディアムバイオレット
    Color4B m_endColorMediumViolet;
    Color4B m_startColorPurple;				// パープル
    Color4B m_endColorPurple;
    Color4B m_startColorDarkGreen;			// 深緑
    Color4B m_endColorDarkGreen;
    Color4B m_startColorBrown;				// 茶色
    Color4B m_endColorBrown;

    Color4B m_startColorMaroon;				// マルーン
    Color4B m_endColorMaroon;
    Color4B m_startColorNavy;				// ネイビー
    Color4B m_endColorNavy;
    Color4B m_startColorSilver;				// シルバー
    Color4B m_endColorSilver;
    Color4B m_startColorBlack;				// ブラック
    Color4B m_endColorBlack;
    
    Color4B m_colorTurqoise;				// ターコイズ
    Color4B m_colorEmerald;					// エメラルド
    Color4B m_colorPeterRiver;				// ピーター・リバー
    Color4B m_colorAmethyst;				// アメジスト
    Color4B m_colorWetAspalt;				// ウェット・アスファルト
    Color4B m_colorGreenSea;				// グリーン・シー
    Color4B m_colorNephritis;				// ネフリシス
    Color4B m_colorBelizeHole;				// ベリーズホール
    Color4B m_colorWisteria;				// ウィステリア
    Color4B m_colorMidnightBlue;			// ミッドナイト・ブルー
    Color4B m_colorSunFlower;				// サンフラワー
    Color4B m_colorCarrot;					// キャロット
    Color4B m_colorAlizarin;				// アリザリン
    Color4B m_colorClouds;					// クラウド
    Color4B m_colorConcrete;				// コンクリート
    Color4B m_colorOrange;					// オレンジ
    Color4B m_colorPumpkin;					// パンプキン
    Color4B m_colorPomegranate;				// ポームグラネート
    Color4B m_colorSilver;					// シルバー
    Color4B m_colorAsbestos;				// アスベストス

    // フォントカラー
    Color3B m_fontColor;

    // 設定値
    int m_player;										// プレイヤー（0:プレイヤー１ 1:プレイヤー２）
    int m_hour1;										// 持ち時間（時間）（プレイヤー１）
    int m_hour2;										// 持ち時間（時間）（プレイヤー２）
    int m_minite1;										// 持ち時間（分）（プレイヤー１）
    int m_minite2;										// 持ち時間（分）（プレイヤー２）
    int m_color1;										// 背景色（プレイヤー１）
    int m_color2;										// 背景色（プレイヤー２）
    int m_coler_before1;								// 背景色（プレイヤー１）過去
    int m_coler_before2;								// 背景色（プレイヤー２）過去
    int m_rule;											// ルール
    int m_byo_yomi_time1;								// 秒読み時間（プレイヤー１）
    int m_byo_yomi_time2;								// 秒読み時間（プレイヤー２）
    int m_byo_yomi_kouryo_cnt_current1;					// 秒読み考慮回数（プレイヤー１）
    int m_byo_yomi_kouryo_cnt_current2;					// 秒読み考慮回数（プレイヤー２）
    int m_byo_yomi_kouryo_cnt1;							// 最大秒読み考慮回数（プレイヤー１）
    int m_byo_yomi_kouryo_cnt2;							// 最大秒読み考慮回数（プレイヤー２）
    int m_byo_yomi_kouryo_time1;						// 秒読み考慮時間（プレイヤー１）
    int m_byo_yomi_kouryo_time2;						// 秒読み考慮時間（プレイヤー２）
    int m_fisher_additional_time1;						// フィッシャー加算時間（プレイヤー１）
    int m_fisher_additional_time2;						// フィッシャー加算時間（プレイヤー２）
    int m_canada_kitei_time1;							// カナダ式規定時間（プレイヤー１）
    int m_canada_kitei_time2;							// カナダ式規定時間（プレイヤー２）
    int m_canada_kitei_tekazu1;							// カナダ式規定手数（プレイヤー１）
    int m_canada_kitei_tekazu2;							// カナダ式規定手数（プレイヤー２）
    int m_xianqqi_kitei_tekazu1_1;						// シャンチー国際 規定手数１（プレイヤー１）
    int m_xianqqi_kitei_tekazu1_2;						// シャンチー国際 規定手数１（プレイヤー２）
    int m_xianqqi_moti_jikan2_1;						// シャンチー国際 持ち時間２（プレイヤー１）
    int m_xianqqi_moti_jikan2_2;						// シャンチー国際 持ち時間２（プレイヤー２）
    int m_xianqqi_kitei_tekazu2_1;						// シャンチー国際 規定手数２（プレイヤー１）
    int m_xianqqi_kitei_tekazu2_2;						// シャンチー国際 規定手数２（プレイヤー２）
    int m_xianqqi_kitei_cnt1;							// シャンチー国際 規定回数（プレイヤー１）
    int m_xianqqi_kitei_cnt2;							// シャンチー国際 規定回数（プレイヤー２）
    int m_xianqqi_byo_yomi_time1;						// シャンチー国際 秒読み時間（プレイヤー１）
    int m_xianqqi_byo_yomi_time2;						// シャンチー国際 秒読み時間（プレイヤー２）

    int m_headerBarHeight;
    int m_footerBarHeight;

    float m_start_y;
    float m_end_y;
    unsigned long long m_touch_start_time;
    unsigned long long m_touch_end_time;

    // button action
    void startButtonDidPushed(Ref* pSender);
    void time1ButtonDidPushed(Ref* pSender);
    void time2ButtonDidPushed(Ref* pSender);
    void color1ButtonDidPushed(Ref* pSender);
    void color2ButtonDidPushed(Ref* pSender);
    void ruleButtonDidPushed(Ref* pSender);
    void forceStartButtonDidPushed(Ref* pSender);
    void cancelButtonDidPushed(Ref* pSender);

    std::string getBatteryStatus();
    std::string getBatteryLevel();
    void showAlertDialog(const char* title, const char* message, const char* positiveButton, const char* nagativeButton, const char* neutralButton);

    // display
    void loadCommonSetting();
    void loadSaveParams();
    void setBackGroundColor();
    bool checkParameter();
    virtual void showRuleTitle();
    virtual void showRuleMenu();
    virtual void showTime1Title();
    virtual void showTime1Menu();
    virtual void showTime2Title();
    virtual void showTime2Menu();
    virtual void showColorTitle();
    virtual void showColor1Menu();
    virtual void showColor2Menu();
    void showBatteryAlertDialog(const char* message);
    void updateActiveStateButtonUnderDialog(bool state);

private:

};

#endif
